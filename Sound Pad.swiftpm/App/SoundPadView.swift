import SwiftUI

struct SoundPadView: View {

    var body: some View {
        /*#-code-walkthrough(1.exploreLoop)*/Beats {
            LoopButton(beat: .CosmicBeat, color: /*#-code-walkthrough(1.changeColor)*/ .blue/*#-code-walkthrough(1.changeColor)*/)
            LoopButton(beat: .HotBeat1, color: .cyan)
            LoopButton(beat: /*#-code-walkthrough(1.changeSound)*/.DiscoBeat1/*#-code-walkthrough(1.changeSound)*/, color: .indigo)
            LoopButton(beat: .DiscoBeat1, color: .orange)
            LoopButton(beat: .FeverBeat, color: .yellow)
            LoopButton(beat: .SolarisBeat2, color: .red)
            LoopButton(beat: .GoTimeBeat2, color: .pink)
        }/*#-code-walkthrough(1.exploreLoop)*/
        //#-learning-code-snippet(3.learnBass)
        //#-learning-code-snippet(3.learnMelodyAmbient)
        //#-learning-code-snippet(4.learnSoundFX)
    }
}
