import SwiftUI
//#-learning-task(tapView)

/*#-code-walkthrough(1.tapView)*/
struct TapView: View {
    /*#-code-walkthrough(1.tapView)*/

    /*#-code-walkthrough(1.setUp)*/
    @State private var color : Color = Color.primary
    /*#-code-walkthrough(1.setUp)*/
    
    /*#-code-walkthrough(1.tapGesture)*/
    var tapGesture: some Gesture {
        /*#-code-walkthrough(1.gestureDefinition)*/
        TapGesture()
            .onEnded {
                withAnimation {
                    color = Color.random()
                }
            }
        /*#-code-walkthrough(1.gestureDefinition)*/
    }
    /*#-code-walkthrough(1.tapGesture)*/
    
    var body: some View {
        VStack {
            Text("Tap the rectangle to change its color")
            Spacer()
            /*#-code-walkthrough(1.rectangle)*/
            Rectangle()
            /*#-code-walkthrough(1.rectangle)*/
                /*#-code-walkthrough(1.foregroundColor)*/
                .foregroundColor(color)
                /*#-code-walkthrough(1.foregroundColor)*/
                .frame(width: 250, height: 450)
                /*#-code-walkthrough(1.applyingTheColor)*/
                .gesture(tapGesture)
                /*#-code-walkthrough(1.applyingTheColor)*/
            Spacer()
        }
        .navigationTitle("Tap")
        .padding()
        .toolbar {
            Button("Reset") {
                color = Color.primary
            }
        }
    }
}

struct TapView_Previews: PreviewProvider {
    static var previews: some View {
        TapView()
    }
}

extension Color {
    static func random() -> Color {
        return Color(red: Double.random(in: 0...1), green: Double.random(in: 0...1), blue: Double.random(in: 0...1))
    }
}
