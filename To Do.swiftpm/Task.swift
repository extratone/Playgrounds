import CoreData
import SwiftUI

@objc(Task)
class Task: NSManagedObject {
    @NSManaged var complete: Bool
    @NSManaged var name: String?
    @NSManaged var index: Int
}

extension Task: Identifiable {
    var id: Int {
        index
    }
}
