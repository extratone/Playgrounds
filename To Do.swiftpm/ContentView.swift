import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(sortDescriptors: [
        NSSortDescriptor(keyPath: \Task.index, ascending: true)
    ], animation: .default)
    private var items: FetchedResults<Task>
    
    var body: some View {
        List(items) { item in
            VStack {
                if !item.complete {
                    Text(item.name ?? "Unknown")
                } else {
                    Text(item.name ?? "Unknown")
                        .strikethrough()
                        .foregroundColor(.gray)
                        .opacity(0.75)
                }
            }
            .onTapGesture {
                viewContext.perform {
                    item.complete.toggle()
                    try! viewContext.save()
                }
            }
        }
    }
}
