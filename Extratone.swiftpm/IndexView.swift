import SwiftUI





/*#-code-walkthrough(IndexView.struct)*/


struct IndexView: View {
    
    
    /*#-code-walkthrough(IndexView.struct)*/
    
    
    /*#-code-walkthrough(IndexView.views)*/
    
    
    var body: some View {
        
        
        VStack {
            
            
            Text("Howdy")
            
            
            /*#-code-walkthrough(IndexView.modifiers)*/
            
            
                .font(.largeTitle)
            
            
                .fontWeight(.bold)
            
            
                .padding()
            
            
            /*#-code-walkthrough(IndexView.modifiers)*/
            
            
            /*#-code-walkthrough(IndexView.Image)*/
            
            
            Image("BigBoyPad")
            
            
            /*#-code-walkthrough(IndexView.Image)*/
            
            
            /*#-code-walkthrough(IndexView.Image.resizable)*/
            
            
                .resizable()
            
            
                .scaledToFit()
            
            
            //.clipShape(Circle())
            
            
            //.clipShape(RoundedRectangle(cornerRadius: 10))
            
            
            //.clipShape(Ellipse())
            
            
                .clipShape(Capsule())
            
            
                .overlay(
                    
                    
                    Circle()
                    
                    
                        .stroke(.blue, style: StrokeStyle(lineWidth: 7.4))
                    
                    
                )
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            /*#-code-walkthrough(IndexView.Image.resizable)*/
            
            
            /*#-code-walkthrough(IndexView.Image.modifiers)*/
            
            
            
            
            
            /*#-code-walkthrough(IndexView.Image.modifiers)*/
            
            
            /*#-code-walkthrough(omeView.Image.overlay)*/
            
            
            
            
            
            /*#-code-walkthrough(omeView.Image.overlay)*/
            
            
            /*#-code-walkthrough(IndexView.Text)*/
            
            
            Text("David Blue")
            
            
            /*#-code-walkthrough(IndexView.Text)*/
            
            
            /*#-code-walkthrough(IndexView.Text.modifiers)*/
            
            
                .font(.largeTitle)
            
            
                .font(.custom(FontNames.georgia, size: 40))
            
            
                .foregroundColor(.blue)
            
            
                .shadow(color: .blue, radius: 30)
            
            
            HStack {
                
                
                Image(systemName: "command")
                
                
                    .foregroundColor(.green)
                
                
                Text("Fuck")
                
                
                    .font(.largeTitle)
                
                
                    .foregroundColor(.cyan)
                
                
                    .padding()
                
                
                Image(systemName: "book")
                
                
                    .foregroundColor(.purple)
                
                
            }
            
            
            
            
            
            
            
            
            
            
            
            /*#-code-walkthrough(IndexView.Text.modifiers)*/
            
            
            /*#-code-walkthrough(IndexView.Text.moreModifiers)*/
            
            
            
            
            
            /*#-code-walkthrough(IndexView.Text.moreModifiers)*/
            
            
            
            
            
            /*#-code-walkthrough(IndexView.stacksOnStacks)*/
            
            
            
            
            
            /*#-code-walkthrough(IndexView.stacksOnStacks)*/
            
            
        }
        
        
        .padding()
        
        
        /*#-code-walkthrough(IndexView.Image.background)*/
        
        
        
        
        
        /*#-code-walkthrough(IndexView.Image.background)*/
        
        
        /*#-code-walkthrough(IndexView.Image.clip)*/
        
        
        
        
        
        /*#-code-walkthrough(IndexView.Image.clip)*/
        
        
        
        
        
    }
    
    
    /*#-code-walkthrough(IndexView.views)*/
    
    
    
    
    
}





struct IndexView_Previews: PreviewProvider {
    
    
    static var previews: some View {
        
        
        IndexView()
        
        
    }
    
    
}





struct FontNames {
    
    
    static var americanTypwriter = "American Typewriter"
    
    
    static var arial = "Arial"
    
    
    static var baskerville = "Baskerville"
    
    
    static var chalkduster = "Chalkduster"
    
    
    static var courier = "Courier"
    
    
    static var georgia = "Georgia"
    
    
    static var helvetica = "Helvetica"
    
    
    static var palatino = "Palatino"
    
    
    static var zapfino = "Zapfino"
    
    
}
