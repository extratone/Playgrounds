import SwiftUI

struct FormView: View {
    enum Flavor: String, CaseIterable { case strawberry, chocolate, vanilla }
    
    @State private var selectedColor: Color = .blue
    @State private var selectedFlavor: Flavor = .strawberry
    @State private var isOn = false
    
    let formattedDate = Date().formatted()
    
    var body: some View {
        Form {
            Section("First") {
                LabeledContent("Date", value: formattedDate)
                ColorPicker("Color", selection: $selectedColor)
            }
            Section("Second") {
                Picker("Flavor", selection: $selectedFlavor) {
                    ForEach(Flavor.allCases, id: \.hashValue) { Text($0.rawValue.capitalized) }
                }
                Toggle(isOn: $isOn) {
                    Text("Dark Mode")
                    Text("Turns everything dark")
                }
            }
        }
        .formStyle(.grouped)
    }
}
