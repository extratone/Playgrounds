import SwiftUI

struct TableView: View {
    struct Fruit: Identifiable {
        let id = UUID()
        let name: String
        let emoji: String
        let color: Color
    }
    
    @State private var fruits: [Fruit] = [
        Fruit(name: "Strawberry", emoji: "🍓", color: .red),
        Fruit(name: "Banana", emoji: "🍌", color: .yellow),
        Fruit(name: "Apple", emoji: "🍎", color: .red),
        Fruit(name: "Kiwi", emoji: "🥝", color: .green),
        Fruit(name: "Blueberry", emoji: "🫐", color: .blue),
        Fruit(name: "Cherry", emoji: "🍒", color: .red),
        Fruit(name: "Grape", emoji: "🍇", color: .purple),
        Fruit(name: "Orange", emoji: "🍊", color: .orange)
    ]
    @State private var sortOrder = [KeyPathComparator(\Fruit.name)]
    
    var body: some View {
        NavigationStack {
            Table(fruits, sortOrder: $sortOrder) {
                TableColumn("", value: \.emoji)
                    .width(32)
                TableColumn("Name", value: \.name)
                TableColumn("Color") { fruit in
                    Circle()
                        .frame(width: 16)
                        .foregroundColor(fruit.color)
                }
            }
            .onChange(of: sortOrder) {
                fruits.sort(using: $0)
            }
        }
    }
}
