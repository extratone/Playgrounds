import SwiftUI
import UIKit

///@Task(type: addCode, title: "Try changing the text to something new.", id: "changeText", file: IntroView.swift) {
///     @Page(id: "changeTextExpanded", title: "Try changing the text to something new.") {
///         Change the text, `"Hello, friend."` to something you want it to say.
///     }
///}


struct AssessmentImages {
    static let friend = "Friend"
    static let friendAndGem = "FriendAndGem"
    static let blu = "Blu"
    static let hopper = "Hopper"
    //Optional string to maintain API compatibility below, we'd allow "nil" as a "wildcard" if needed
    static let acceptableFriendImages: [String?] = [AssessmentImages.friend, AssessmentImages.friendAndGem]
}

func changeText() -> Bool {
    return UIElement.elements.contains(where: {$0.traits.contains(.staticText) && $0.label != "Hello, friend."})
}

func textElement() -> Bool {
    return UIElement.elements.compactMap({($0.traits.contains(.staticText)) ? $0 : nil}).count > 1
}

func addFriend() -> Bool {
    return UIElement.elements.contains(where: {
        let ratio = ($0.frame.size.width/$0.frame.size.height)
        
        return $0.traits.contains(.image) && AssessmentImages.acceptableFriendImages.contains($0.label) && (ratio < 1.4 || ratio > 1.5)
    })
}

func modifier() -> Bool {
    return UIElement.elements.contains(where: {
        if $0.traits.contains(.image) && AssessmentImages.acceptableFriendImages.contains($0.label) {
            
            let ratio = ($0.frame.size.width/$0.frame.size.height)
            
            // If the image isn't resized, the view will render at full width with the proper aspect ratio, so we should check that this is not the case.
            return ratio > 1.4 && ratio < 1.5 && $0.frame.size.width != 1720
        }
        
        return false
    })
}

//NB - this actually uses an HStack now, but I don't want to change the task identifier as it's unnecessary risk...
func addAVStack() -> Bool {
    // Returns true when the learner adds a new HStack and placed a view inside of that stack.
    return UIElement.elements.contains(where: {
        return $0.label == nil && $0.frame.size.width != 0 && $0.frame.size.height != 0 && $0.frame != CGRect.zero && // <-- ignore outer wrapper VStack
            $0.elements.count >= 1
    })
}

func addImageInHStack() -> Bool {
    // Returns true when the learner adds the Image view inside the HStack created in the previous task. There should be a text and image view in the HStack and two text views outside the HStack.
    return UIElement.elements.contains(where: {
            return $0.elements.contains(where: {
            guard $0.elements.count >= 2 else { return false }
                let frame1 = $0.elements[0].frame
                let frame2 = $0.elements[1].frame
                let containsImage = $0.elements.contains{$0.traits.contains(.image)}
                let containsText = $0.elements.contains{$0.traits.contains(.staticText)}
               
                //the frame comparison to check that they're laid out in an HStack, not VStack
                return $0.elements.count >= 2 && containsText && containsImage && frame1.maxX < frame2.minX || frame2.maxX < frame1.minX
            })
        })
}

func composeAView() -> Bool {
    // Returns true when the learner creates an HStack containing an Image View as the first subview, and a VStack as the second view.
    return UIElement.elements.contains(where: {
        return $0.label == nil && /*$0.frame.origin.x == 0.0 && $0.frame.origin.y == 0.0 &&*/
        $0.elements.count == 1 &&
        $0.elements.contains(where: { $0.traits.contains(.image) })
    })
}

func addVStackInHStack() -> Bool {
    // Returns true when the learner adds a VStack inside of the HStack created in the previous step.
    return UIElement.elements.contains(where: {
        guard $0.elements.count >= 2 else { return false }
        return $0.label == nil && /*$0.frame.origin.x == 0.0 && $0.frame.origin.y == 0.0 &&*/
        $0.elements.count == 2 &&
        $0.elements[0].frame.origin.x != $0.elements[1].frame.origin.x &&
        $0.elements.contains(where: { $0.traits.contains(.image) }) &&
        $0.elements.contains(where: { $0.label == nil /*$0.frame.size.width == 0.0 && $0.frame.size.height == 0.0*/ })
    })
}

func addTextInVStack() -> Bool {
    // Returns true when the learner adds a text view inside of the VStack created in the previous task. (Ideally, it also checks that the text font is .largeTitle.)
    let testLabel = UILabel()
    
    testLabel.text = "Friend"
    testLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
    testLabel.sizeToFit()
    
    return UIElement.elements.contains(where: {
        guard $0.elements.count >= 2 else { return false }
        return $0.label == nil &&
        $0.elements.count == 2 &&
        $0.elements[0].frame.origin.x != $0.elements[1].frame.origin.x &&
        $0.elements.contains(where: { $0.traits.contains(.image) }) &&
        $0.elements.contains(where: { $0.label == nil &&
            $0.elements.contains(where: {
                $0.traits.contains(.staticText) && $0.label == "Friend" && (abs($0.frame.size.height - testLabel.frame.size.height) < 1)
            } )
        })
    })
}

func describeFriend() -> Bool {
    // Returns true when the learner adds a second text view below the first in the VStack. (Ideally, it also checks that the text font is .caption.)
    let testLabel = UILabel()
    
    testLabel.numberOfLines = 0
    testLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
    
    return UIElement.elements.contains(where: {
        guard $0.elements.count >= 2 else { return false }
        return $0.label == nil &&
        $0.elements.count == 2 &&
        $0.elements[0].frame.origin.x != $0.elements[1].frame.origin.x &&
        $0.elements.contains(where: { $0.traits.contains(.image) }) &&
        $0.elements.contains(where: { $0.label == nil &&
            $0.elements.compactMap({ $0.traits.contains(.staticText) }).count >= 2 &&
            $0.elements.contains(where: {
                if $0.traits.contains(.staticText) &&
                    $0.label != "Friend" {
                    testLabel.text = $0.label
                    let testSize = testLabel.sizeThatFits(CGSize(width: $0.frame.size.width, height: 10000))
                    
                    return abs($0.frame.size.height - testSize.height) < 2
                } else {
                    return false
                }
             }) &&
            $0.elements[0].frame.origin.y != $0.elements[1].frame.origin.y
        })
    })
}

func createDetailView() -> Bool {
    // Returns true when the learner initializes the FriendDetailView in ExperimentView.swift.
    return assessDetailView(acceptableNames: AssessmentImages.acceptableFriendImages)
}

func createBluView() -> Bool {
    // Returns true when the learner creates a view similar to FriendDetailView, (HStack containing an image using the blu resource and a VStack, VStack containing two text elements).
    return assessDetailView(acceptableNames: [AssessmentImages.blu])
}

func createHopperView() -> Bool {
    // Returns true when the learner creates a view similar to FriendDetailView, (HStack containing an image using the hopper resource and a VStack, VStack containing two text elements).
    return assessDetailView(acceptableNames: [AssessmentImages.hopper])
}

/// Assesses whether the current structures matches the criteria given
/// - Parameter acceptableNames: a list of acceptable names for the image, passing nil in the array denotes a wildcard. i.e. assessDetailView([nil]) = we'll accept any name, but structure has to match
/// - Returns: true if we the current structures passes criteria, false if not
func assessDetailView(acceptableNames: [String?]) -> Bool {
    return UIElement.elements.contains(where: {
        guard $0.elements.count >= 2 else { return false }
        return $0.elements.count >= 2 &&
        $0.elements[0].frame.origin.x != $0.elements[1].frame.origin.x &&
        $0.elements.contains(where: { $0.traits.contains(.image) && acceptableNames.contains($0.label) }) &&
        $0.elements.contains(where: { $0.label == nil &&
            $0.elements.compactMap({ $0.traits.contains(.staticText) && $0.label != nil }).count >= 2
        })
    })
}


struct UIElement: Identifiable {
    static var elements: [UIElement] {
        get {
            var elements = [UIElement]()
            
            for windowScene in UIApplication.shared.connectedScenes.compactMap({ $0 as? UIWindowScene }) {
                for window in windowScene.windows {
                    if let rootView = window.rootViewController?.view {
                        elements.append(contentsOf: elementData(rootView))
                        
                        break
                    }
                }
            }
                
            return elements
        }
    }
    
    static func elementData(_ container: NSObject, removeRoot: Bool = false) -> [UIElement] {
        var data = [UIElement]()
        
        let elemData = UIElement(element: container)
        
        if !removeRoot {
            data.append(elemData)
        }
        
        if let elements = container.accessibilityElements {
            for element in elements.compactMap( { $0 as? NSObject } ) {
                data.append(contentsOf: elementData(element))
            }
        }
        
        return data
    }
    
    let id = UUID()
    var identifier: String?
    var label: String?
    var hint: String?
    var value: String?
    var frame = CGRect.zero
    var traits = UIAccessibilityTraits.none
    var containerType = UIAccessibilityContainerType.none
    var elements = [UIElement]()
    
    init(element: NSObject) {
        if let identifier = element.perform(#selector(getter: UIAccessibilityIdentification.accessibilityIdentifier))?.takeUnretainedValue() as? String {
            self.identifier = identifier
        }
        
        if let label = element.perform(#selector(NSObject.accessibilityLabel))?.takeUnretainedValue() as? String {
            self.label = label
        }
        
        if let hint = element.perform(#selector(NSObject.accessibilityHint))?.takeUnretainedValue() as? String {
            self.hint = hint
        }
        
        if let value = element.perform(#selector(NSObject.accessibilityValue))?.takeUnretainedValue() as? String {
            self.value = value
        }
        
        if let frame = element.value(forKey: "accessibilityFrame") as? CGRect {
            self.frame = frame
        }
        
        if let traits = element.value(forKey: "accessibilityTraits") as? UIAccessibilityTraits {
            self.traits = traits
        }
        
//        if let containerType = element.value(forKey: "containerType") as? UIAccessibilityContainerType {
//            self.containerType = containerType
//        }
        
        if let containedElements = element.value(forKey: "accessibilityElements") as? [Any] {
            for containedElement in containedElements {
                if let containedNSObject = containedElement as? NSObject {
                    self.elements.append(UIElement(element: containedNSObject))
                }
            }
        }
    }
    
    static func debugPrint() {
        print("\n####\n\(elements[0].debugDescription())\n####")
    }
    
    static var debugTabDepth = 0
    
    func debugDescription() -> String {
        var description = ""
        var tabString = ""
        
        for _ in 0..<UIElement.debugTabDepth {
            tabString += "\t"
        }
        
        description += "\(tabString)[ "
        
        if let id = identifier {
            description += "id:\(id) "
        }
        
        if let label = label {
            description += "label:'\(label)' "
        }
        
        description += "frame:(\(Int(frame.origin.x)),\(Int(frame.origin.x)))(\(Int(frame.size.width)),\(Int(frame.size.height))) "
        
        if let hint = hint {
            description += "hint:\(hint) "
        }
        
        if let value = value {
            description += "value:\(value) "
        }
        
        var traitString = ""
        
        if !traits.isEmpty {
            if traits.contains(.button)                    { traitString += ".button " }
            if traits.contains(.header)                    { traitString += ".header " }
            if traits.contains(.selected)                  { traitString += ".selected " }
            if traits.contains(.link)                      { traitString += ".link " }
            if traits.contains(.searchField)               { traitString += ".searchField " }
            if traits.contains(.image)                     { traitString += ".image " }
            if traits.contains(.playsSound)                { traitString += ".playsSound " }
            if traits.contains(.keyboardKey)               { traitString += ".keyboardKey " }
            if traits.contains(.staticText)                { traitString += ".staticText " }
            if traits.contains(.summaryElement)            { traitString += ".summaryElement " }
            if traits.contains(.updatesFrequently)         { traitString += ".updatesFrequently " }
            if traits.contains(.startsMediaSession)        { traitString += ".startsMediaSession " }
            if traits.contains(.allowsDirectInteraction)   { traitString += ".allowsDirectInteraction " }
            if traits.contains(.causesPageTurn)            { traitString += ".causesPageTurn " }
            if traits.contains(.tabBar)                    { traitString += ".tabBar " }
            
            if traitString.count > 0 {
                description += "traits:(\(traitString)) "
            }
        }
        
        if !elements.isEmpty {
            description += "{\n"
            
            UIElement.debugTabDepth += 1
            
            for element in elements {
                description += element.debugDescription()
            }
            
            UIElement.debugTabDepth -= 1
            
            description += "\(tabString)}"
        }
        
        description += "]\n"
        
        return description
    }
}
