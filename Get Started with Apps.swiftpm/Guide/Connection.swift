import Foundation

let taskFunctionByID = ["changeText": changeText,
                        "textElement": textElement,
                        "addFriend": addFriend,
                        "modifier": modifier,
                        "addAVStack": addAVStack,
                        "addImageInHStack": addImageInHStack,
                        "composeAView": composeAView,
                        "addVStackInHStack": addVStackInHStack,
                        "addTextInVStack": addTextInVStack,
                        "describeFriend": describeFriend,
                        "createDetailView": createDetailView,
                        "createBluView": createBluView,
                        "createHopperView": createHopperView]

@_cdecl("Assessment") public dynamic func Assessment(_ payload: [String: Any], _ completion: @escaping ([String: Any]?, NSError?) -> Void) -> Void {
    if let taskIDData = payload["TaskID"] as? Data,
       let taskID = String(data: taskIDData, encoding: .utf8) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            var completed = false
            
            if let taskFunction = taskFunctionByID[taskID] {
                completed = taskFunction()
            } else {
                print("Error: Assessment function for taskID '\(taskID)' not found.")
            }
            
            completion([taskID: Data(completed.description.utf8)], nil)
        }
    }
}
