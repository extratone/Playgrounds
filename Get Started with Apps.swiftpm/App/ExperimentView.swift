import SwiftUI

struct ExperimentView: View {
    var body: some View {
        VStack {
            
            FriendDetailView()
            
            HStack { 
                Image("Blu")
                    .resizable()
                    .scaledToFit()
                VStack {
                    Text("Blu")
                        .font(.largeTitle)
                    Text("First Text")
                        .font(.caption)
                }
            }
            
            }
    }
}

struct ExperimentView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ExperimentView()
        }
    }
}
