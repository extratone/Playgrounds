import SwiftUI
//#-learning-task(dataFetcherApp)

@main
struct MemeCreatorApp: App {
    /*#-code-walkthrough(1.pandaCollectionFetcher)*/
    @StateObject private var fetcher = PandaCollectionFetcher()
    /*#-code-walkthrough(1.pandaCollectionFetcher)*/
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                /*#-code-walkthrough(1.environmentObject)*/
                MemeCreator()
                    .environmentObject(fetcher)
                /*#-code-walkthrough(1.environmentObject)*/

            }
            .navigationViewStyle(.stack)
        }
    }
}
