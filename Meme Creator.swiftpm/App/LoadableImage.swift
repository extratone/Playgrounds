import SwiftUI
//#-learning-task(loadableImage)

/*#-code-walkthrough(4.loadableImage)*/
struct LoadableImage: View {
    /*#-code-walkthrough(4.loadableImage)*/
    /*#-code-walkthrough(4.imageMetaData)*/
    var imageMetadata: Panda
    /*#-code-walkthrough(4.imageMetaData)*/
    
    var body: some View {
        /*#-code-walkthrough(4.asyncImage)*/
        AsyncImage(url: imageMetadata.imageUrl) {/*#-code-walkthrough(4.phase)*/ phase in /*#-code-walkthrough(4.phase)*/
            /*#-code-walkthrough(4.asyncImage)*/
            /*#-code-walkthrough(4.phaseImage)*/
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(15)
                    .shadow(radius: 5)
                    .accessibility(hidden: false)
                    .accessibilityLabel(Text(imageMetadata.description))
                /*#-code-walkthrough(4.phaseImage)*/
            } /*#-code-walkthrough(4.phaseError)*/ else if phase.error != nil /*#-code-walkthrough(4.phaseError)*/ {
                VStack {
                    Image("pandaplaceholder")
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 300)
                    Text("The pandas were all busy.")
                        .font(.title2)
                    Text("Please try again.")
                        .font(.title3)
                }
                
            } else {
                /*#-code-walkthrough(4.progressView)*/
                ProgressView()
                /*#-code-walkthrough(4.progressView)*/
            }
        }
    }
}

struct Panda_Previews: PreviewProvider {
    static var previews: some View {
        LoadableImage(imageMetadata: Panda.defaultPanda)
    }
}
