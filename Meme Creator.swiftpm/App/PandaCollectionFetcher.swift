import SwiftUI
//#-learning-task(pandaCollectionFetcher)

/*#-code-walkthrough(3.pandaCollectionFetcher)*/
class PandaCollectionFetcher: ObservableObject {
    /*#-code-walkthrough(3.pandaCollectionFetcher)*/
    /*#-code-walkthrough(3.publishedValues)*/
    @Published var imageData = PandaCollection(sample: [Panda.defaultPanda])
    @Published var currentPanda = Panda.defaultPanda
    /*#-code-walkthrough(3.publishedValues)*/
    
    let urlString = "http://playgrounds-cdn.apple.com/assets/pandaData.json"
    
    enum FetchError: Error {
        case badRequest
        case badJSON
    }
    
    @available(iOS 15.0, *)
    /*#-code-walkthrough(3.asyncFunc)*/ func fetchData() async /*#-code-walkthrough(3.asyncFunc)*/
    /*#-code-walkthrough(3.throws)*/ throws /*#-code-walkthrough(3.throws)*/ {
        /*#-code-walkthrough(3.validURL)*/
        guard let url = URL(string: urlString) else { return }
        /*#-code-walkthrough(3.validURL)*/

        /*#-code-walkthrough(3.urlSession)*/
        let (data, response) = try await URLSession.shared.data(for: URLRequest(url: url))
        /*#-code-walkthrough(3.urlSession)*/
        /*#-code-walkthrough(3.response)*/
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { throw FetchError.badRequest }
        /*#-code-walkthrough(3.response)*/

        Task { @MainActor in
            /*#-code-walkthrough(3.decodingData)*/
            imageData = try JSONDecoder().decode(PandaCollection.self, from: data)
            /*#-code-walkthrough(3.decodingData)*/
        }
    }
    
}
