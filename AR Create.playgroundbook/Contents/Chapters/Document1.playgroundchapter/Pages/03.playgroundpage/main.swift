//#-hidden-code
//
//  main.swift
//  
//  Copyright © 2016-2020 Apple Inc. All rights reserved.
//

import PlaygroundSupport
import CoreGraphics

let viewController = ARLiveViewController.makeFromStoryboard(showDebug: true)
PlaygroundPage.current.liveView = viewController
viewController.scene = scene

//#-code-completion(everything, hide)
//#-code-completion(keyword, show, var, func, if, for)
//#-code-completion(currentmodule, show)
//#-code-completion(module, show, MyFiles)
//#-code-completion(identifier, hide, viewController)
//#-code-completion(literal, show, integer, string, array, boolean)
//#-code-completion(description, hide, "look(at: SCNVector3)", "remove(UnsafePointer<Int8>!)")
//#-code-completion(identifier, show, (x:y:z:), ., BuiltinAction, createStartScreen(with:), stop(), ImaginaryNode, Model, Point, Scene, add(_:), alarmClock, animate(), animate(_:), animate(_:repeats:completion:), appear, appear(), applyColor(scheme:), arabic, arpeggio, arpeggioLoop, arrow, basic, bounceAndPoint, box(width:height:depth:), buzz, cactus, camera, chinese, chineseHongKong, chineseTaiwan, chord, chordLoop, click, cm, cool, cube(size:), cylinder(size:height:), czech, danish, disappear, disappear(), drip, dutch, ear, englishAustralia, englishIreland, englishSouthAfrica, englishUK, englishUS, exploreWithTouch(), eye, eyeLook, fadeTo(duration:), false, fingerWiggle, finnish, flap, flee(from:), flee(from:safeDistance:), float, flyingBug, follow(_:), follow(_:at:), followTheFeet(), foot, frenchCanada, frenchFrance, frown, german, greek, hand, hebrew, hindi, hot, hungarian, indonesian, italian, japanese, juice, korean, lemon, lightbulb, lightning, location, look(at:), look(at:)), lookLeft, lookRight, loop, m, melody, melodyLoop, mm, models, moody, moon, mouth, moveBy(x:y:z:duration:), moveTo(duration:), node, none, norwegian, nose, number(), pingpong, place(_:at:), plane(width:length:), play(_:), play(_:loops:completion:), play(_:rate:loops:completion:), point, polish, portugueseBrazil, portuguesePortugal, pyramid(width:height:depth:), rain, raincloud, remove(), rhythm, rhythmLoop, ring, romanian, rotateBy(x:y:z:duration:), run(action:), run(action:completion:), run(group:), run(sequence:), russian, scaleTo(duration:), scene, setOnStartHandler, setSelectionHandler, slovak, smellTheRoses(), smile, snail, soundscape, soundscapeLoop, spanishMexico, spanishSpain, speak(text:), speak(text:withAccent:rate:pitch:completion:), sphere(size:), spines, star, sun, swedish, tacky, tear, text(_:elevation:), thai, tick, toeWiggle, torus(size:thickness:), trafficCone, true, turkish, wacky, waggle, music, when(_:isWithin:do:), x, x:y:z:, y, z)
//#-end-hidden-code
//#-editable-code
//#-localizable-zone(StartingPointPage0301)
// Create a model.
//#-end-localizable-zone
let lemon = Model.lemon
let snail = Model.snail
let eye = Model.eye
let foot = Model.foot

//#-localizable-zone(StartingPointPage0302)
// Apply a color scheme to a model.
//#-end-localizable-zone
lemon.applyColor(scheme: .basic)

//#-localizable-zone(StartingPointPage0303)
// Create a shape.
// The .m, .cm, and .mm properties make it possible for you to provide real-world units to objects in the AR scene.
//#-end-localizable-zone
let pyramid = Model.pyramid(width: 10.cm, height: 10.cm, depth: 10.cm)
let box = Model.box(width: 1.cm, height: 10.cm, depth: 5.mm)
let torus = Model.torus(size: 30.cm, thickness: 5.cm)
let cube = Model.cube(size: 5.mm)
let cylinder = Model.cylinder(size: 10.cm, height: 30.cm)
let sphere = Model.sphere(size: 30.cm)

//#-localizable-zone(StartingPointPage0304)
// Create 3D text.
//#-end-localizable-zone
let text = Model.text("/*#-localizable-zone(StartingPointPage03Textk1)*/Hello there, coder./*#-end-localizable-zone*/", elevation: 20.cm)

//#-localizable-zone(StartingPointPage0305)
// Add models to the scene.
//#-end-localizable-zone
scene.add([lemon, pyramid, text, snail, eye, foot])

//#-localizable-zone(StartingPointPage0306)
// Set the handler for when a model is selected with a tap.
//#-end-localizable-zone
foot.onSelected = {
    foot.run(action: .scaleBy(1.2, duration: 1))
}

//#-localizable-zone(StartingPointPage0307)
// The onStart handler runs after you press "Start Scene".
//#-end-localizable-zone
scene.setOnStartHandler {
//#-localizable-zone(StartingPointPage0308)
    // Animation API allows you to run animations on models.
//#-end-localizable-zone
    pyramid.animate()
    snail.animate(.waggle)
    eye.animate(.lookLeft, repeats: .loop, completion: nil)
   
//#-localizable-zone(StartingPointPage0309)
    // Play a single sound.
//#-end-localizable-zone
    lemon.play(.chord)
 
//#-localizable-zone(StartingPointPage0310)
    // Loop a sound.
//#-end-localizable-zone
    snail.play(.buzz, loops: false, completion: {
        snail.play(.frown, loops: true)
    })
}

//#-localizable-zone(StartingPointPage0311)
// Create a distance event.
//#-end-localizable-zone
scene.camera.when(snail, isWithin: 10.cm, do: {
//#-localizable-zone(StartingPointPage0312)
    // Speak text from a model, with optional parameters.
//#-end-localizable-zone
    snail.speak(text: "/*#-localizable-zone(StartingPointPage03Textk2)*/Don't talk to me like that!/*#-end-localizable-zone*/", rate: 0.2, pitch: 1.76, completion: nil)
    
//#-localizable-zone(StartingPointPage0313)
    // Run a single action on a model.
//#-end-localizable-zone
    lemon.run(action: .moveBy(x: 10.cm, y: 10.cm, z: 50.cm, duration : 5))
    
//#-localizable-zone(StartingPointPage0314)
    // Execute a completion handler after running an action.
//#-end-localizable-zone
    foot.run(action: .fadeTo(0.5, duration: 2), completion: {
        foot.applyColor(scheme: .tacky)
    })
    
//#-localizable-zone(StartingPointPage0315)
    // Apply a color scheme to all models in the scene.
    scene.applyColor(scheme: .wacky)
//#-end-localizable-zone


//#-localizable-zone(StartingPointPage0316)
    // Run mutliple actions simultaneously.
//#-end-localizable-zone
    eye.run(group: [.rotateBy(x: 0, y: 20, z: 0, duration: 5), .moveBy(x: 0, y: 20.cm, z: 0, duration: 5)])
    
//#-localizable-zone(StartingPointPage0317)
    // Run multiple actions sequentially.
//#-end-localizable-zone
    pyramid.run(sequence: [.scaleTo(0.2, duration: 4), .fadeTo(0.25, duration: 2)])
})
//#-end-editable-code
