//#-hidden-code
//
//  main.swift
//  
//  Copyright © 2016-2020 Apple Inc. All rights reserved.
//
import PlaygroundSupport
import SceneKit


let viewController = ARLiveViewController.makeFromStoryboard(showDebug: true)
PlaygroundPage.current.liveView = viewController
viewController.scene = scene

//#-code-completion(everything, hide)
//#-code-completion(keyword, show, var, func, if, for)
//#-code-completion(currentmodule, show)
//#-code-completion(module, show, MyFiles)
//#-code-completion(identifier, hide, viewController)
//#-code-completion(literal, show, integer, string, array, boolean)
//#-code-completion(description, hide, "look(at: SCNVector3)", "remove(UnsafePointer<Int8>!)")
//#-code-completion(identifier, show, (x:y:z:), ., BuiltinAction, createStartScreen(with:), stop(), ImaginaryNode, Model, Point, Scene, add(_:), alarmClock, animate(), animate(_:), animate(_:repeats:completion:), appear, appear(), applyColor(scheme:), arabic, arpeggio, arpeggioLoop, arrow, basic, bounceAndPoint, box(width:height:depth:), buzz, cactus, camera, chinese, chineseHongKong, chineseTaiwan, chord, chordLoop, click, cm, cool, cube(size:), cylinder(size:height:), czech, danish, disappear, disappear(), drip, dutch, ear, englishAustralia, englishIreland, englishSouthAfrica, englishUK, englishUS, exploreWithTouch(), eye, eyeLook, fadeTo(duration:), false, fingerWiggle, finnish, flap, flee(from:), flee(from:safeDistance:), float, flyingBug, follow(_:), follow(_:at:), followTheFeet(), foot, frenchCanada, frenchFrance, frown, german, greek, hand, hebrew, hindi, hot, hungarian, indonesian, italian, japanese, juice, korean, lemon, lightbulb, lightning, location, look(at:), look(at:)), lookLeft, lookRight, loop, m, melody, melodyLoop, mm, models, moody, moon, mouth, moveBy(x:y:z:duration:), moveTo(duration:), node, none, norwegian, nose, number(), pingpong, place(_:at:), plane(width:length:), play(_:), play(_:loops:completion:), play(_:rate:loops:completion:), point, polish, portugueseBrazil, portuguesePortugal, pyramid(width:height:depth:), rain, raincloud, remove(), rhythm, rhythmLoop, ring, romanian, rotateBy(x:y:z:duration:), run(action:), run(action:completion:), run(group:), run(sequence:), russian, scaleTo(duration:), scene, setOnStartHandler, setSelectionHandler, slovak, smellTheRoses(), smile, snail, soundscape, soundscapeLoop, spanishMexico, spanishSpain, speak(text:), speak(text:withAccent:rate:pitch:completion:), sphere(size:), spines, star, sun, swedish, tacky, tear, text(_:elevation:), thai, tick, toeWiggle, torus(size:thickness:), trafficCone, true, turkish, wacky, waggle, music, when(_:isWithin:do:), x, x:y:z:, y, z)
//#-end-hidden-code
//#-editable-code
let lemon = Model.lemon

scene.add(lemon)
//#-end-editable-code

