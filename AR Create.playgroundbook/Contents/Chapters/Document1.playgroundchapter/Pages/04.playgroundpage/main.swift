//#-hidden-code
//
//  main.swift
//  
//  Copyright © 2016-2020 Apple Inc. All rights reserved.
//
import PlaygroundSupport
import CoreGraphics

let viewController = ARLiveViewController.makeFromStoryboard(showDebug: true)
PlaygroundPage.current.liveView = viewController
viewController.scene = scene

//#-code-completion(everything, hide)
//#-code-completion(keyword, show, var, func, if, for)
//#-code-completion(currentmodule, show)
//#-code-completion(module, show, MyFiles)
//#-code-completion(identifier, hide, viewController)
//#-code-completion(literal, show, integer, string, array, boolean)
//#-code-completion(description, hide, "look(at: SCNVector3)", "remove(UnsafePointer<Int8>!)")
//#-code-completion(identifier, show, (x:y:z:), ., BuiltinAction, createStartScreen(with:), stop(), ImaginaryNode, Model, Point, Scene, add(_:), alarmClock, animate(), animate(_:), animate(_:repeats:completion:), appear, appear(), applyColor(scheme:), arabic, arpeggio, arpeggioLoop, arrow, basic, bounceAndPoint, box(width:height:depth:), buzz, cactus, camera, chinese, chineseHongKong, chineseTaiwan, chord, chordLoop, click, cm, cool, cube(size:), cylinder(size:height:), czech, danish, disappear, disappear(), drip, dutch, ear, englishAustralia, englishIreland, englishSouthAfrica, englishUK, englishUS, exploreWithTouch(), eye, eyeLook, fadeTo(duration:), false, fingerWiggle, finnish, flap, flee(from:), flee(from:safeDistance:), float, flyingBug, follow(_:), follow(_:at:), followTheFeet(), foot, frenchCanada, frenchFrance, frown, german, greek, hand, hebrew, hindi, hot, hungarian, indonesian, italian, japanese, juice, korean, lemon, lightbulb, lightning, location, look(at:), look(at:)), lookLeft, lookRight, loop, m, melody, melodyLoop, mm, models, moody, moon, mouth, moveBy(x:y:z:duration:), moveTo(duration:), node, none, norwegian, nose, number(), pingpong, place(_:at:), plane(width:length:), play(_:), play(_:loops:completion:), play(_:rate:loops:completion:), point, polish, portugueseBrazil, portuguesePortugal, pyramid(width:height:depth:), rain, raincloud, remove(), rhythm, rhythmLoop, ring, romanian, rotateBy(x:y:z:duration:), run(action:), run(action:completion:), run(group:), run(sequence:), russian, scaleTo(duration:), scene, setOnStartHandler, setSelectionHandler, slovak, smellTheRoses(), smile, snail, soundscape, soundscapeLoop, spanishMexico, spanishSpain, speak(text:), speak(text:withAccent:rate:pitch:completion:), sphere(size:), spines, star, sun, swedish, tacky, tear, text(_:elevation:), thai, tick, toeWiggle, torus(size:thickness:), trafficCone, true, turkish, wacky, waggle, music, when(_:isWithin:do:), x, x:y:z:, y, z)
//#-end-hidden-code
//#-editable-code
//#-localizable-zone(StartingPointPage0401)
// Create a banner and a butterfly.
//#-end-localizable-zone
let banner = Model.text("/*#-localizable-zone(StartingPointPage04Textk1)*/CATCH THE SPEEDY BUTTERFLY!/*#-end-localizable-zone*/", elevation: 10.cm)
let butterfly = Model.flyingBug

//#-localizable-zone(StartingPointPage0402)
// Add the models to the scene and scale down the butterfly.
//#-end-localizable-zone
scene.add([banner, butterfly])
butterfly.run(action: .scaleTo(0.1, duration: 2))

//#-localizable-zone(StartingPointPage0403)
// Create a sequence to move the butterfly. Change the duration to make it go faster or slower.
//#-end-localizable-zone
let duration: Float = 1
let sequence: [BuiltinAction] = [
    .moveBy(x: 35.cm, y: 20.cm, z: -5.cm, duration: duration),
    .moveBy(x: -20.cm, y: 5.cm, z: -20.cm, duration: duration),
    .moveBy(x: -25.cm, y: -10.cm, z: 10.cm, duration: duration),
    .moveBy(x: 15.cm, y: 20.cm, z: 30.cm, duration: duration),
    .moveBy(x: -5.cm, y: -35.cm, z: -15.cm, duration: duration)
]

//#-localizable-zone(StartingPointPage0404)
// When the scene starts, the butterfly should flap around.
//#-end-localizable-zone
scene.setOnStartHandler {
    butterfly.animate(.flap, repeats: .loop)
    butterfly.repeat(.pingpong, sequence: sequence, completion: nil)
    butterfly.play(.melodyLoop, loops: true, completion: nil)
}

//#-localizable-zone(StartingPointPage0405)
// When the camera comes within 2cm of the butterfly, create a surprise!
//#-end-localizable-zone
scene.camera.when(butterfly, isWithin: 2.cm) {
    let ear = Model.ear
    let eye = Model.eye
    let foot = Model.foot
    
    scene.place(ear, at: butterfly.location)
    scene.place(eye, at: butterfly.location)
    scene.place(foot, at: butterfly.location)
    
    ear.run(action: .moveBy(x: Float.random(in: -20.cm...20.cm), y: Float.random(in: -20.cm...20.cm), z: Float.random(in: -20.cm...20.cm), duration: 1))
    eye.run(action: .moveBy(x: Float.random(in: -20.cm...20.cm), y: Float.random(in: -20.cm...20.cm), z: Float.random(in: -20.cm...20.cm), duration: 1))
    foot.run(action: .moveBy(x: Float.random(in: -20.cm...20.cm), y: Float.random(in: -20.cm...20.cm), z: Float.random(in: -20.cm...20.cm), duration: 1))
}
//#-end-editable-code
