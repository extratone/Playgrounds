//
//  lighting.metal
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

#pragma body
//note : this Lighting modifier can be deleted if you want to use the scene ambient rather than a custom property for Ambience
half nl = max(0.0, dot(_surface.normal, _light.direction));
float3 diff = nl * _light.intensity.rgb;

_lightingContribution.diffuse = diff;

