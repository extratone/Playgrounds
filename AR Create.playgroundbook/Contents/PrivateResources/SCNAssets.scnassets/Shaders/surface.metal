//
//  surface.metal
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

#pragma arguments
float4 color0;
float4 color1;
float4 color2;
float4 color3;

float4 ColorLit0;
float4 ColorLit1;
float4 ColorLit2;
float4 ColorLit3;

#pragma varyings
float4 worldPosition;
float4 litColor;

#pragma body
//world position as in the original shader
//in.worldPosition = scn_frame.inverseViewTransform * float4(_surface.position,1);

//model position, which will not work with scale
//but will produce a fixed noise if the objects moves
float4 modelPosition = scn_node.inverseModelViewTransform * float4(_surface.position, 1);
in.worldPosition = modelPosition;

float4 red   = _surface.diffuse.r * color0;
float4 green = _surface.diffuse.g * color1;
float4 blue  = _surface.diffuse.b * color2;
float4 alpha = (1 - _surface.diffuse.a) * color3;

float4 rLit = _surface.diffuse.r * ColorLit0;
float4 gLit = _surface.diffuse.g * ColorLit1;
float4 bLit = _surface.diffuse.b * ColorLit2;
float4 aLit = (1 - _surface.diffuse.a) * ColorLit3;

in.litColor = rLit + gLit + bLit + aLit;

_surface.diffuse = red + green + blue + alpha;
