//
//  fragment.metal
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

#pragma body
float3 color;

color = mix(_surface.diffuse.rgb, in.litColor.rgb, _lightingContribution.diffuse.r);
_output.color = float4(color, 1.0);
