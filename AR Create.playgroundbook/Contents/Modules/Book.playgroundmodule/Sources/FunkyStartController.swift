//
//  FunkyStartController.swift
//
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

@objc(FunkyStartController)
public class FunkyStartController: UIViewController {
    public static let SegueIdentifier = "com.apple.spc.funkystart"

    @IBOutlet weak var messageBackground: UIView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var funkyScreenStartButton: UIButton!
    
    public var onStartHandler : (() -> Void)?

    override public func viewDidLoad() {
        super.viewDidLoad()

        messageBackground.layer.borderWidth = 6.0
        messageBackground.layer.borderColor = UIColor.white.cgColor
        messageBackground.layer.cornerRadius = 16.0
        
        funkyScreenStartButton.accessibilityLabel = NSLocalizedString("Amazing! Let's start the scene.", comment: "AX description of the start button on the funky start screen.")
    }

    @IBAction func start(_ sender: Any) {
        onStartHandler?()
    }

}
