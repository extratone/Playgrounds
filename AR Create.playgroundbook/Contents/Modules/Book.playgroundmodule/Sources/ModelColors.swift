//
//  ModelColors.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import UIKit

public enum ColorScheme: String, CaseIterable {
    case basic
    case wacky
    case tacky
    case cool
    case moody
    case hot
    case custom
}

public struct Palette {
    public static var Color1 = #colorLiteral(red: 0, green: 1, blue: 0.7647058824, alpha: 1)
    public static var Color2 = #colorLiteral(red: 0.2862745098, green: 0.06666666667, blue: 0.9960784314, alpha: 1)
    public static var Color3 = #colorLiteral(red: 1, green: 0.1882352941, blue: 0, alpha: 1)
    public static var Color4 = #colorLiteral(red: 1, green: 0.9411764706, blue: 0, alpha: 1)

    public static var Lit1 = #colorLiteral(red: 0.8352941176, green: 1, blue: 0, alpha: 0)
    public static var Lit2 = #colorLiteral(red: 0.8352941176, green: 1, blue: 0, alpha: 0)
    public static var Lit3 = #colorLiteral(red: 0.8352941176, green: 1, blue: 0, alpha: 0)
    public static var Lit4 = #colorLiteral(red: 0.8352941176, green: 1, blue: 0, alpha: 0)

    public static var First: ColorDefinition  = .custom(base: Color1, lit: Lit1)
    public static var Second: ColorDefinition = .custom(base: Color2, lit: Lit2)
    public static var Third: ColorDefinition  = .custom(base: Color3, lit: Lit3)
    public static var Fourth: ColorDefinition = .custom(base: Color4, lit: Lit4)
}

public enum ColorDefinition {
    case pinkLime, aquaLime, aquaWhite, limePink, blue, black, limeWhite, limeAqua, pinkWhite, red, yellow, pink, white, clear, aqua, lime, yellowWhite, whiteYellow, blueAqua, whiteAqua, yellowRed, redYellow, pinkAqua, bluePink, blackBlack
    case custom(base: UIColor, lit: UIColor)

    func asColor() -> UIColor {
        switch self {
            // Pink
        case .pinkLime, .pinkWhite, .red, .pinkAqua:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(50.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Aqua
        case .aquaLime, .blue, .aquaWhite:
            return UIColor(red: CGFloat(0.percent), green: CGFloat(100.percent), blue: CGFloat(78.04.percent), alpha: CGFloat(100.percent))
            // Lime
        case .limeWhite, .limeAqua, .limePink:
            return UIColor(red: CGFloat(84.percent), green: CGFloat(100.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // Red
        case .pink, .redYellow:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(26.27.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // Black
        case .white, .blackBlack:
            return UIColor(red: CGFloat(0.percent), green: CGFloat(0.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // Blue
        case .aqua, .blueAqua, .bluePink:
            return UIColor(red: CGFloat(28.24.percent), green: CGFloat(0.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Yellow
        case .yellowWhite, .yellowRed:
            return UIColor(red: CGFloat(94.9.percent), green: CGFloat(100.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // White
        case .whiteYellow, .whiteAqua:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(100.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Grey
        case .black:
            return UIColor(red: CGFloat(26.percent), green: CGFloat(26.percent), blue: CGFloat(26.percent), alpha: CGFloat(100.percent))
            // Mustard
        case .lime, .yellow:
            return UIColor(red: CGFloat(90.percent), green: CGFloat(80.percent), blue: CGFloat(14.percent), alpha: CGFloat(100.percent))
            // Clear
        case .clear:
            return UIColor.clear

        case .custom(let color, _):
            return color
        }
    }
}

public protocol SupportsColoring {
    var scheme: ColorScheme { get set }

    func colorArray(for scheme: ColorScheme) -> [ColorDefinition]
    func applyColor(scheme: ColorScheme)
    func litColor(for base: ColorDefinition) -> UIColor
    func particleColor(for scheme: ColorScheme) -> ColorDefinition
}

extension SupportsColoring {
    public func litColor(for base: ColorDefinition) -> UIColor {
        switch base {
            // Lime
        case .pinkLime, .aquaLime, .lime:
            return UIColor(red: CGFloat(84.percent), green: CGFloat(100.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // White
        case .limeWhite, .pinkWhite, .white, .aquaWhite, .yellowWhite:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(100.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Blue
        case .blue:
            return UIColor(red: CGFloat(28.24.percent), green: CGFloat(0.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Black
        case .black, .blackBlack:
            return UIColor(red: CGFloat(0.percent), green: CGFloat(0.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // Red
        case .red, .yellowRed:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(26.27.percent), blue: CGFloat(0.percent), alpha:CGFloat(100.percent))
            // Yellow
        case .yellow, .whiteYellow, .redYellow:
            return UIColor(red: CGFloat(94.9.percent), green: CGFloat(100.percent), blue: CGFloat(0.percent), alpha: CGFloat(100.percent))
            // Pink
        case .pink, .limePink, .bluePink:
            return UIColor(red: CGFloat(100.percent), green: CGFloat(50.percent), blue: CGFloat(100.percent), alpha: CGFloat(100.percent))
            // Aqua
        case .limeAqua, .aqua, .blueAqua, .whiteAqua, .pinkAqua:
            return UIColor(red: CGFloat(0.percent), green: CGFloat(100.percent), blue: CGFloat(78.04.percent), alpha: CGFloat(100.percent))
            // Clear
        case .clear:
            return UIColor.clear

        case .custom(_, let lit):
            return lit
        }
    }

    public func particleColor(for scheme: ColorScheme) -> ColorDefinition {
        return .clear
    }

    public var defaultScheme: ColorScheme {
        return .basic
    }
}
