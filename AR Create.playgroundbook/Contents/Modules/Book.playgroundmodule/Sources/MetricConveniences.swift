//
//  MetricConveniences.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public extension Double {
    // Distance
    var  m: Float { return Float(self) }
    var dm: Float { return Float(self / 10.0) }
    var cm: Float { return Float(self / 100.0) }
    var mm: Float { return Float(self / 1000.0) }

    var percent: Float { return Float(self / 100.0) }

    // Time
    var s: Float { return Float(self) }
    var ms: Float { return Float(self / 1000.0) }
    var µs: Float { return Float(self / 1000000.0) }
}

public extension Int {
    // Distance
    var  m: Float { return Float(self) }
    var dm: Float { return Float(self) / 10.0 }
    var cm: Float { return Float(self) / 100.0 }
    var mm: Float { return Float(self) / 1000.0 }

    var percent: Float { return Float(self) / 100.0 }

    // Time
    var s: Float { return Float(self) }
    var ms: Float { return Float(self) / 1000.0 }
    var µs: Float { return Float(self) / 1000000.0 }
}

public extension Float {
    // Distance
    var  m: Float { return self }
    var dm: Float { return self / 10.0 }
    var cm: Float { return self / 100.0 }
    var mm: Float { return self / 1000.0 }

    var percent: Float { return self / 100.0 }

    // Time
    var s: Float  { return self }
    var ms: Float { return self / 1000.0 }
    var µs: Float { return self / 1000000.0 }
}

public extension CGFloat {
    // Distance
    var  m: Float { return Float(self) }
    var dm: Float { return Float(self) / 10.0 }
    var cm: Float { return Float(self) / 100.0 }
    var mm: Float { return Float(self) / 1000.0 }
    
    var percent: Float { return Float(self) / 100.0 }
    
    // Time
    var s: Float { return Float(self) }
    var ms: Float { return Float(self) / 1000.0 }
    var µs: Float { return Float(self) / 1000000.0 }
}
