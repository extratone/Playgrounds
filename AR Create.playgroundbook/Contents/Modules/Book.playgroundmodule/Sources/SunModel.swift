//
//  SunModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class SunModel : _InternalModel, ImaginaryModel {
    public static var Identifier = "Sun"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .lime]

        case .basic:
            return [.yellow, .yellowRed]

        case .wacky:
            return [.pink, .aqua]
            
        case .cool:
            return [.blueAqua, .limeAqua]
            
        case .moody:
            return [.black, .white]
            
        case .hot:
            return [.yellow, .black]

        case .custom:
            return [Palette.First, Palette.Second]
        }
    }

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: SunModel.Identifier, verticalOffset: .offPlane(CGFloat(8.cm)))
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Sun", comment: "AX title for the sun model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored sun with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored sun.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored sun with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored sun.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored sun with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored sun.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored sun with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored sun.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored sun with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored sun.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored sun with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored sun.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored sun with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored sun.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A bright yellow sun with orange flares.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A not-so-bright black sun with white colored flares.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A bright pink sun with lime colored flares.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A bright aqua blue sun with blue flares.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A bright yellow sun with black colored flares.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A bright pink sun with aqua blue flares.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A bright uniquely colored sun with flares.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate sun.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of sun.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between sun", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and sun", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
