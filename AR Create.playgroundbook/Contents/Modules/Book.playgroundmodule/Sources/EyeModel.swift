//
//  EyeModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import ARKit
import CoreGraphics
import SceneKit

public final class EyeModel : _InternalModel, ImaginaryModel, CustomAnimatable, ParticleProtocol {
    public var particleSize: Float = 0.018
    public var particleColor: ColorDefinition = .clear
    
    public static var Identifier = "Eye"
    
    public var emitterOffset : (Float, Float) = (0, -1.cm)
    
    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limeWhite, .limePink, .pinkLime, .black]

        case .basic:
            return [.limeWhite, .red, .pink, .black]

        case .wacky:
            return [.lime, .blue, .black, .aqua]
            
        case .cool:
            return [.aquaWhite, .limeAqua, .aqua, .black]
            
        case .moody:
            return [.black, .white, .black, .white]
            
        case .hot:
            return [.yellowWhite, .yellowRed, .yellowRed, .black]

        case .custom:
            return [Palette.Fourth, Palette.First, Palette.Second, Palette.Third]
        }
    }

    public func particleColor(for scheme: ColorScheme) -> ColorDefinition {
        switch scheme {
        case .tacky:
            return .black

        case .basic:
            return .aqua

        case .wacky:
            return .pink

        case .cool:
            return .blue

        case .moody:
            return .red

        case .hot:
            return .yellow

        case .custom:
            return Palette.Third
        }
    }
    
    public enum Animation : String, CustomAnimation {
        case lookLeft = "LookLeft"
        case lookRight = "LookRight"
        case tear = "Cry"

        public func shouldClipAnimation() -> Bool {
            switch self {
            case .lookLeft, .lookRight:
                return true

            case .tear:
                return false
            }
        }

        public func customDuration() -> TimeInterval {
            switch self {
            case .lookRight, .lookLeft:
                return 1.25

            case .tear:
                return 0.0
            }
        }
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    public var boundaries: [ModelBoundaryDescription] = []

    public init?() {
        super.init(named: EyeModel.Identifier, rigged: true, verticalOffset: .offPlane(CGFloat(4.cm)))
        self.name = EyeModel.Identifier
        applyColor(scheme: defaultScheme)
        
        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
     Runs the provided animation.
     
     - Parameter animation: The animation to run.
     - Parameter repeats: The RepeatingType, which determines how many times the animation runs.
     - Parameter completion: The handler to run on completion of the animation running. There is no completion handler by default.
     
     - localizationKey: EyeModel.animate(_:repeats:completion:)
     */
    public func animate(_ animation: EyeModel.Animation, repeats: RepeatingType = .none, completion: (() -> (Void))? = {}) {
        switch animation {
        case .lookLeft, .lookRight:
            loadAnimation(named: animation, for: EyeModel.Identifier, repeating: repeats, handler: completion)
            switch repeats {
            case .none:
                self.play(.eyeLookLoopSingle, loops: false, completion: nil)
            case .number(let x):
                var count: Int = x

                func playSoundXTimes() {
                    guard count > 0 else { return }
                    self.play(.eyeLookLoopSingle, loops: false, completion: playSoundXTimes)
                    count -= 1
                }
                playSoundXTimes()
            case .loop:
                self.play(.eyeLookLoopSingle, loops: true, completion: nil)
            case .pingpong:
                self.play(.eyeLook, loops: true, completion: nil)
                
            }

        case .tear:
            self.play(.tear, loops: true, completion: nil)
            cry()
        }
    }
    
    fileprivate func cry() {
        if let emitter = SCNParticleSystem(named: "EyeCry", inDirectory: nil), let explode = SCNParticleSystem(named: "ParticleRemoveSlow", inDirectory: nil) {
            emitter.particleSize = emitter.particleSize * CGFloat(scale.x)
            emitter.particleDiesOnCollision = true
            emitter.systemSpawnedOnDying = explode
            emitter.colliderNodes = Scene.colliderNodesForParticles(from: self)

            emitter.particleColor = particleColor.asColor()
            explode.particleColor = particleColor.asColor()

            let eyeNode = SCNNode()
            eyeNode.name = EyeModel.Identifier
            self.addChildNode(eyeNode)
            Scene.particleSystemNodes.append(eyeNode)
            eyeNode.position = SCNVector3(0, _InternalModel.VerticalOffset, 0)
            eyeNode.addParticleSystem(emitter)
        }
    }
    
    public func animate() {
        pop()
    }

    public var onSelected: (() -> Void) = { }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Eye", comment: "AX title for the eye model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored eye with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored eye.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored eye with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored eye.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored eye with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored eye.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored eye with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored eye.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored eye with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored eye.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored eye with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored eye.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored eye with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored eye.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A lid-less eye with a white cornea and pink iris.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A lid-less eye with a black cornea and white iris.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A lid-less eye with a white cornea and lime colored iris.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A lid-less eye with a white cornea and aqua blue iris.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A lid-less eye with a yellow cornea and orange iris.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A lid-less eye with a lime colored cornea and aqua blue iris.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A lid-less eye with a uniquely colored cornea and iris.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate eye.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of eye.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between eye", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and eye", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
