//
//  Point.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit
import simd

/// Specifies a point in the scene with x, y, and z coordinates.
/// - localizationKey: Point
public struct Point: Codable {
    
    /// The x coordinate for the point.
    /// - localizationKey: Point.x
    public var x: Float
    
    /// The y coordinate for the point.
    /// - localizationKey: Point.y
    public var y: Float
    
    /// The z coordinate for the point.
    /// - localizationKey: Point.z
    public var z: Float
    
    /// Creates a point with x, y, and z coordinates.
    /// - localizationKey: Point(x{Float}:y{Float}:z{Float}:)
    public init(x: Float = 0.0, y: Float = 0.0, z: Float = 0.0) {
        self.x = x
        self.y = y
        self.z = z
    }
    
    /// Creates a point with x and z coordinates.
    /// - localizationKey: Point(x{Float}:z{Float}:)
    public init(x: Float = 0.0, z: Float = 0.0) {
        self.x = x
        self.y = 0
        self.z = z
    }

    public init(xyz vector: float3) {
        self.x = vector.x
        self.y = vector.y
        self.z = vector.z
    }

    var simdPoint: float3 {
        return float3(x, y, z)
    }

    /// Returns a point at the center of the coordinate system.
    /// - localizationKey: Point.center
    public static let center = Point(x: 0.0, y: 0.0, z: 0.0)
    
    public var vector3: SCNVector3 { return SCNVector3(x, y, z) }
    
    /// Returns a description of the point, used for accessibility.
    public var description: String {
        return String.localizedStringWithFormat(NSLocalizedString("Point with coordinates x:%@, y:%@, z:%@", comment: "Point description, used for accessibility."), x.description, y.description, z.description)
    }
}
