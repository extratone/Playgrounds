//
//  BoxModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class BoxModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "Box"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    init(width: CGFloat, height: CGFloat, depth: CGFloat) {
        super.init(requiresAdjustmentToSitOnPlane: true)

        let box = SCNBox(width: width, height: height, length: depth, chamferRadius: 0.0)
        geometry = box
        _createPhysicsBody(for: box)
        applyColor(scheme: defaultScheme)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .limePink, .limePink, .lime, .lime, .lime]

        case .basic:
            return [.aqua, .aqua, .aqua, .pink, .pink, .pink]

        case .wacky:
            return [.aqua, .lime, .pink, .blue, .red, .yellow]
            
        case .cool:
            return [.blueAqua, .blueAqua, .blueAqua, .blue, .blue]
            
        case .moody:
            return [.black, .black, .black, .white]
            
        case .hot:
            return [.redYellow, .redYellow, .redYellow, .yellowRed, .yellowRed, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second, Palette.Fourth, Palette.First, Palette.Second]
        }
    }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Box", comment: "AX title for the box model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored box with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored box.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored box with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored box.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored box with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored box.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored box with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored box.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored box with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored box.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored box with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored box.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored box with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored box.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A box with blue and pink sides.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A box with black and white sides.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A box with pink and lime colored sides.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A box with blue and aqua blue sides.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A box with yellow and red sides.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A box with aqua blue, pink, blue, red, yellow, and lime colored sides.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A box with unique colors on the sides.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }


    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate box.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of box.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between box", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and box", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }
}
