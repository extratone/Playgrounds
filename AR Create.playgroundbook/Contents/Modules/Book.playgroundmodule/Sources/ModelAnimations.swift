//
//  ModelAnimations.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit

public protocol CustomAnimatable {
    associatedtype Animation: CustomAnimation

    func animate(_ animation: Animation, repeats: RepeatingType, completion: (()->(Void))?)
}

public protocol CustomAnimation {
    func shouldClipAnimation() -> Bool
    func customDuration() -> TimeInterval
}

extension CustomAnimation {
    public func shouldClipAnimation() -> Bool {
        return false
    }

    public func customDuration() -> TimeInterval {
        return 0.0
    }
}

public enum RepeatingType {
    case number(Int)
    case loop
    case pingpong
    case none

    func repeatingAction(for action: SCNAction) -> SCNAction {
        switch self {
        case .number(let times):
            return SCNAction.repeat(action, count: times)

        case .pingpong:
            return SCNAction.repeatForever(SCNAction.sequence([action, action.reversed()]))

        case .loop:
            return SCNAction.repeatForever(action)

        case .none:
            return action
        }
    }
}



extension CustomAnimatable where Self : _InternalModel,
                           Self : ModelProtocol,
                           Animation : RawRepresentable,
                           Animation.RawValue == String {

    /**
     Runs the provided animation.
     
     - Parameter animation: The animation to run.
     - Parameter repeats: The RepeatingType, which determines how many times the animation runs.
     - Parameter completion: The handler to run on completion of the animation running. There is no completion handler by default.
     
     - localizationKey: ModelProtocol.animate(_:repeats:completion:)
     */
    public func animate(_ animation: Animation, repeats: RepeatingType = .none, completion: (() -> (Void))? = {}) {
        loadAnimation(named: animation, for: Self.Identifier, repeating: repeats, handler: completion)
    }
    
    func loadAnimation(named: Animation, for identifier: String, repeating: RepeatingType, handler: (() -> (Void))?) {
        let selected = self._load(animation: named.rawValue, for: identifier)
        
        selected.spc_reset(for: identifier, name: named.rawValue)
        selected.spc_configure(repeating: repeating, handler: handler)

        // set custom clipping
        // rdar://problem/48522430
        if named.shouldClipAnimation() {
            selected.duration = named.customDuration()
        }
        

        childNodes[0].addAnimation(selected, forKey: selected.keyPath)
        
        let soundNameKey = named.rawValue
        
        let keyToSoundIdentifier: [String : String] = [
            "PointBounce" : "arrow_bounceandpoint",
            "Flapping" : "flyingbug_flap",
            "Spikes" : "cactus_spines",
            "AlarmRing" : "alarmclock_ring",
            "HandMove" : "alarmclock_tick",
            "Wiggle" : "foot_toewiggle",
            "FingerWiggle" : "hand_fingerwiggle",
            "Point" : "hand_point",
            "Frown" : "mouth_frown",
            "Smile" : "mouth_smile",
            "BlinkWiggle" : "snail_waggle"
        ]
    
        guard let soundIdentifier = keyToSoundIdentifier[soundNameKey] else { return }
        var modelSounds: [String] = []
        for sound in Sound.allCases {
            if sound.rawValue.contains(soundIdentifier) {
                modelSounds.append(sound.rawValue)
            }
        }
        
        let single = modelSounds.filter { $0.contains("single") }
        let loop = modelSounds.filter { $0.contains("loop") }
        let reverse = modelSounds.filter { $0.contains("reverse") }
        let pingPong = modelSounds.filter { $0.contains("pingpong") }
        
        guard !single.isEmpty && !loop.isEmpty && !reverse.isEmpty && !pingPong.isEmpty else { return }
        
        guard let singleSound = Sound(rawValue: single[0]), let loopSound = Sound(rawValue: loop[0]), let reverseSound = Sound(rawValue: reverse[0]), let pingPongSound = Sound(rawValue: pingPong[0]) else { return }
        switch repeating {
        case .none:
            self.play(singleSound, rate: 1, loops: false, completion: nil)
        case .number(let x):
            var count: Int = x
            
            func playSoundXTimes() {
                guard count > 0 else { return }
                self.play(singleSound, rate: 1, loops: false, completion: playSoundXTimes)
                count -= 1
            }
            playSoundXTimes()
        case .loop:
            self.play(loopSound, rate: 1, loops: true, completion: nil)
        case .pingpong:
            self.play(pingPongSound, rate: 1, loops: true, completion: nil)
        }
        
        
    }
}

public extension SCNAnimation {
    func spc_reset(for identifier: String, name: String) {
        // normalize animations
        // rdar://problem/48522430
        repeatCount = 0
        autoreverses = false
        fillsBackward = false
        fillsForward = false

        isAppliedOnCompletion = true
        keyPath = "\(identifier)@\(name)"
    }

    func spc_configure(repeating: RepeatingType, handler: (() -> (Void))?) {
        if let block = handler {
            animationDidStop = { (_,_,_) in
                block()
            }
        }

        switch repeating {
        case .none:
            break // Do nothing
        case .loop:
            repeatCount = .infinity
            fillsForward = true
        case .number(let x):
            repeatCount = CGFloat(x)
        case .pingpong:
            autoreverses = true
            repeatCount = .infinity
            fillsForward = true
        }
    }


}
