//
//  ModelActions.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit

public protocol Actionable {
    associatedtype ActionType : ActionDescriptionProtocol

    func run(action: ActionType, completion: (() -> Void)?)
    func `repeat`(_ type: RepeatingType, action: ActionType, completion: (() -> Void)?)

    // Sequences
    func run(sequence: [ActionType], completion: (() -> Void)?)
    func `repeat`(_ type: RepeatingType, sequence: [ActionType], completion: (() -> Void)?)

    // Groups
    func run(group: [ActionType], completion: (() -> Void)?)
    func `repeat`(_ type: RepeatingType, group: [ActionType], completion: (() -> Void)?)
}

extension Actionable where Self : SCNNode, ActionType == BuiltinAction {

    /**
     Runs a single Action on a model.
     
     - Parameter desc: The Action to run.
     - Parameter completion: The handler to run on completion of the Action running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.run(action:completion:)
     */
    public func run(action desc: ActionType, completion: (() -> Void)? = nil) {
        self.repeat(.none, action: desc, completion: completion)
    }

    /**
     Runs a single action on a model a certain number of times.
     
     - Parameter type: The RepeatingType, which determines how many times the Action runs.
     - Parameter action: The Action to run.
     - Parameter completion: The handler to run on completion of the Action running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.repeat(_:action:completion:)
     */
    public func `repeat`(_ type: RepeatingType, action: ActionType, completion: (() -> Void)?) {
        guard let point = pointToLook(for: action) else {
            return runAction(type.repeatingAction(for: action.buildAction(for: self)), completionHandler: completion)
        }

        self.runAction(type.repeatingAction(for: action.buildAction(for: self)), lookingAt: point, completionHandler: completion)
    }


    // MARK: - Sequences

    /**
     Runs a list of Actions sequentially (one after another) on a model.
     
     - Parameter sequence: The sequence of Actions to run.
     - Parameter completion: The handler to run on completion of the Action sequence running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.run(sequence:completion:)
     */
    public func run(sequence: [ActionType], completion: (() -> Void)? = nil) {
        self.repeat(.none, sequence: sequence, completion: completion)
    }

    /**
     Runs a list of Actions sequentially (one after another) on a model. The entire sequence runs a certain number of times.
     
     - Parameter type: The RepeatingType, which determines how many times the Action sequence runs.
     - Parameter sequence: The sequence of Actions to run.
     - Parameter completion: The handler to run on completion of the Action sequence (or sequences) running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.repeat(_:sequence:completion:)
     */
    public func `repeat`(_ type: RepeatingType, sequence: [ActionType], completion: (() -> Void)?) {
        let actions = sequence.map { action in
            return action.buildAction(for: self)
        }

        let pointsToLook = sequence.compactMap { action in
            return pointToLook(for: action)
        }

        guard pointsToLook.count > 0 else {
            runAction(type.repeatingAction(for: SCNAction.sequence(actions)), completionHandler: completion)
            return
        }

        self.runAction(type.repeatingAction(for: SCNAction.sequence(actions)), lookingAt: pointsToLook[0], completionHandler: completion)
    }


    // MARK: - Groups
    /**
     Runs a list of Actions simultaneously (at the same time) on a model.
     
     - Parameter group: The group of Actions to run.
     - Parameter completion: The handler to run on completion of the Action group running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.run(group:completion:)
     */
    public func run(group: [ActionType], completion: (() -> Void)? = nil) {
        self.repeat(.none, group: group, completion: completion)
    }

    /**
     Runs a list of Actions simultaneously (at the same time) on a model. The entire group runs a certain number of times.
     
     - Parameter type: The RepeatingType, which determines how many times the Action group runs.
     - Parameter group: The group of Actions to run.
     - Parameter completion: The handler to run on completion of the Action group (or groups) running. This value is `nil` by default.
     
     - localizationKey: SCNActionable.repeat(_:group:completion:)
     */
    public func `repeat`(_ type: RepeatingType, group: [ActionType], completion: (() -> Void)?) {
        let actions = group.map { action in
            return action.buildAction(for: self)
        }

        let pointsToLook = group.compactMap { action in
            return pointToLook(for: action)
        }

        guard pointsToLook.count > 0 else {
            runAction(type.repeatingAction(for: SCNAction.group(actions)), completionHandler: completion)
            return
        }

        self.runAction(type.repeatingAction(for: SCNAction.sequence(actions)), lookingAt: pointsToLook[0], completionHandler: completion)
    }
}


public protocol ActionDescriptionProtocol {
    func buildAction(for item: SCNActionable) -> SCNAction
}

/**
 A simple, reusable animation that changes the attributes of any model you attach it to.
 
 - localizationKey: BuiltinAction
 */
public enum BuiltinAction : ActionDescriptionProtocol {
    /**
     Moves a model to the specified point over the provided duration.
     
    - duration: The amount of time it takes for the model to move to the destination, in seconds.
     
     - localizationKey: BuiltinAction.moveTo(_:duration:)
     */
    case moveTo(Point, duration: Float)
    /**
     Moves a model by an `x`, `y`, and `z` value over the provided duration. The default unit for distance is meters. Use `.cm` or `.mm` to specify a different unit.
     
     - x: The `x` value moves a model either left (negative values) or right (positive values).
    - y: The `y` value moves a model either down (negative values) or up (positive values).
    - z: The `z` value moves a model either back (negative values) or forward (positive values).
    - duration: The amount of time it takes for the model to move, in seconds.
     
     - localizationKey: BuiltinAction.moveBy(x:y:z:duration:)
     */
    case moveBy(x: Float, y: Float, z: Float, duration: Float)
    /**
     Rotates a model by a value, in radians, about the `x`, `y`, and `z` axis over the provided duration.
     
    - x: The `x` value rotates a model around the x-axis, which runs left and right.
    - y: The `y` value rotates a model around the y-axis, which runs up and down.
    - z: The `z` value rotates a model around the z-axis, which runs backward and forward.
    - duration: The amount of time it takes for the model to rotate, in seconds.
     
     - localizationKey: BuiltinAction.rotateBy(x:y:z:duration:)
     */
    case rotateBy(x: Float, y: Float, z: Float, duration: Float)
    /**
     Fades a model to a certain alpha value over the provided duration. 1 is fully visible, 0 is invisible.
     
     - duration: The amount of time the model takes to fade, in seconds.
     
     - localizationKey: BuiltinAction.fadeTo(_:duration:)
     */
    case fadeTo(Float, duration: Float)
    /**
     Fades a model by a certain alpha value over the provided duration.
     
     - duration: The amount of time the model takes to fade, in seconds.
     
     - localizationKey: BuiltinAction.fadeBy(_:duration:)
     */
    case fadeBy(Float, duration: Float)
    /**
     Scales a model to a certain value over the provided duration.
     
    - duration: The amount of time the model takes to scale, in seconds.
     
     - localizationKey: BuiltinAction.scaleTo(_:duration:)
     */
    case scaleTo(Float, duration: Float)
    /**
     Scales a model by a certain value over the provided duration.
     
    - duration: The amount of time the model takes to scale, in seconds.
     
     - localizationKey: BuiltinAction.scaleBy(_:duration:)
     */
    case scaleBy(Float, duration: Float)
    /**
     Waits a certain amount of time.
     
    - duration: The amount of time the model waits before performing the next Action, in seconds.
     
     - localizationKey: BuiltinAction.wait(duration:)
     */
    case wait(duration: Float)

    public func buildAction(for item: SCNActionable) -> SCNAction {
        switch self {
        case .moveTo(let point, let duration):
            var loc = point
            if let model = item as? _InternalModel {
                loc = Point(xyz: point.simdPoint + model.simdOffset)
            }
            return SCNAction.move(to: loc.vector3, duration: TimeInterval(duration))

        case .moveBy(let x, let y, let z, let duration):
            return SCNAction.moveBy(x: CGFloat(x), y: CGFloat(y), z: CGFloat(z), duration: TimeInterval(duration))

        case .rotateBy(let x, let y, let z, let duration):
            return SCNAction.rotateBy(x: CGFloat(x), y: CGFloat(y), z: CGFloat(z), duration: TimeInterval(duration))

        case .fadeBy(let value, let duration):
            return SCNAction.fadeOpacity(by: CGFloat(value), duration: TimeInterval(duration))

        case .fadeTo(let value, let duration):
            return SCNAction.fadeOpacity(to: CGFloat(value), duration: TimeInterval(duration))

        case .scaleBy(let value, let duration):
            if let model = item as? ParticleProtocol {
                model.scaleParticles(by: value)
            }
            return SCNAction.scale(by: CGFloat(value), duration: TimeInterval(duration))

        case .scaleTo(let value, let duration):
            if let model = item as? ParticleProtocol {
                model.scaleParticles(to: value)
            }
            return SCNAction.scale(to: CGFloat(value), duration: TimeInterval(duration))

        case .wait(let duration):
            return SCNAction.wait(duration: TimeInterval(duration))
        }

    }
}


extension SCNActionable where Self : SCNNode {
    func runAction(_ action: SCNAction, lookingAt point: Point, completionHandler: (() -> Void)?) {
        self.look(at: point)

        let updatedCompletion = {
            self.constraints = []
            completionHandler?()
        }
        self.runAction(action, completionHandler: updatedCompletion)
    }

    func pointToLook(for action: BuiltinAction) -> Point? {
        switch action {
        case .moveBy(let x, let y, let z, _):

            // Since the look constraint doesn't permit rotation
            // about the x and z axis, a completely vertical move
            // produces an undefined behavior.
            guard !(x == 0 && z == 0) else {
                // vertical moveBy actions should not "look"
                return nil
            }

            return Point(xyz: self.simdPosition + float3(x, y, z))

        case .moveTo(let point, _):
            return point

        default:
            return nil
        }
    }
}
