//
//  PyramidModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class PyramidModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "Pyramid"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    init(width: CGFloat, height: CGFloat, depth: CGFloat) {
        super.init()

        let pyramid = SCNPyramid(width: width, height: height, length: depth)
        geometry = pyramid
        _createPhysicsBody(for: pyramid)
        applyColor(scheme: defaultScheme)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .lime, .limePink, .limePink, .black, .limePink]

        case .basic:
            return [.aqua, .red, .pink, .lime]

        case .wacky:
            return [.black, .aqua, .pink, .lime]
            
        case .cool:
            return [.blue, .blue, .blueAqua, .whiteAqua, .blueAqua]
            
        case .moody:
            return [.black, .black, .white, .white]
            
        case .hot:
            return [.yellowRed, .yellowRed, .redYellow, .yellowWhite, .redYellow]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second, Palette.First, Palette.Second]
        }
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Pyramid", comment: "AX title for the pyramid model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored pyramid with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored pyramid.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored pyramid with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored pyramid.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored pyramid with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored pyramid.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored pyramid with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored pyramid.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored pyramid with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored pyramid.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored pyramid with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored pyramid.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored pyramid with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored pyramid.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("An aqua blue and lime colored pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black and white pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A mostly lime colored pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A mostly blue pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A red and yellow pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A black and lime colored pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored pyramid with a very pointy top. Don’t step on it!", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }
    
    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate pyramid.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of pyramid.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between pyramid", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and pyramid", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
