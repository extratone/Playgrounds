//
//  CylinderModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class CylinderModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "Cylinder"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    init(diameter: CGFloat, height: CGFloat) {
        super.init(requiresAdjustmentToSitOnPlane: true)

        let cylinder = SCNCylinder(radius: diameter / 2.0, height: height)
        geometry = cylinder
        _createPhysicsBody(for: cylinder)
        applyColor(scheme: defaultScheme)
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .limePink, .limePink]

        case .basic:
            return [.aqua, .blue, .blue]

        case .wacky:
            return [.pink, .aqua, .aqua]
            
        case .cool:
            return [.blue, .blueAqua, .blueAqua]
            
        case .moody:
            return [.white, .black, .black]
            
        case .hot:
            return [.yellowRed, .redYellow, .redYellow]

        case .custom:
            return [Palette.First, Palette.Second, Palette.Third]
        }
    }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Cylinder", comment: "AX title for the cylinder model")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored cylinder with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored cylinder.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored cylinder with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored cylinder.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored cylinder with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored cylinder.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored cylinder with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored cylinder.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored cylinder with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored cylinder.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored cylinder with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored cylinder.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored cylinder with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored cylinder.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A blue cylinder, standing on its flat end", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A white colored cylinder, standing on its flat end", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A lime colored cylinder, standing on its flat end", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A blue cylinder, standing on its flat end", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange colored cylinder, standing on its flat end", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink cylinder, standing on its flat end", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored cylinder, standing on its flat end", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate cylinder.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of cylinder.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between cylinder", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and cylinder", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
