//
//  FlyingBugModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

public final class FlyingBugModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Butterfly"
    
    public var emitterOffset : (Float, Float) = (0, 0)
    
    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .lime, .pinkWhite, .black]

        case .basic:
            return [.black, .white, .aqua, .white]

        case .wacky:
            return [.pinkAqua, .bluePink, .lime, .pinkWhite]
            
        case .cool:
            return [.whiteAqua, .blue, .black, .aquaWhite]
            
        case .moody:
            return [.black, .white, .white, .white]
            
        case .hot:
            return [.yellow, .yellowRed, .black, .yellowWhite]

        case .custom:
            return [Palette.First, Palette.Second, Palette.Third]
        }
    }
    
    public enum Animation: String, CustomAnimation {
        case flap = "Flapping"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 5.8
        }
    }

    public var boundaries: [ModelBoundaryDescription] = []

    public var onSelected: (() -> Void) = { }

    public init?() {
        super.init(named: FlyingBugModel.Identifier, rigged: true, verticalOffset: .offPlane(CGFloat(5.cm)))
        applyColor(scheme: defaultScheme)
        
        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)
    }
    
    public func flee(from object: ImaginaryNode, safeDistance: Float) {
        _internalFlee(from: object, safeDistance: CGFloat(safeDistance), block: {
            self.animate(.flap, repeats: .loop, completion: nil)
        })
    }
    
    public func follow(_ object: ImaginaryNode, at distance: Float) {
        _internalFollow(object, at: CGFloat(distance)) {
            self.animate(.flap, repeats: .loop, completion: nil)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Flying bug", comment: "AX title for the flying bug model")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored flying bug with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored flying bug.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored flying bug with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored flying bug.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored flying bug with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored flying bug.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored flying bug with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored flying bug.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored flying bug with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored flying bug.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored flying bug with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored flying bug.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored flying bug with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored flying bug.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A beautiful flying bug, with a black body and white, blue-spotted wings.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A beautiful flying bug, with a black body and white wings.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A beautiful flying bug, with a pink body and yellow, white-spotted wings.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A beautiful flying bug, with a aqua blue body and blue, black-spotted wings.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A beautiful flying bug, with a yellow body and orange, black-spotted wings.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A beautiful flying bug, with a aqua blue body and pink, yellow-spotted wings.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A beautiful flying bug, with uniquely colored body and wings.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate flying bug.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of flying bug.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between flying bug", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and flying bug", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
