//
//  FootModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import ARKit
import UIKit

public final class FootModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static let Identifier = "Foot"
    
    public var emitterOffset : (Float, Float) = (2.cm, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .pink, .black]
            
        case .basic:
            return [.aqua, .red, .black]

        case .wacky:
            return [.pink, .aqua, .black]
            
        case .cool:
            return [.blue, .aquaWhite, .black]
            
        case .moody:
            return [.white, .black, .black]
            
        case .hot:
            return [.yellowRed, .black, .black]

        case .custom:
            return [Palette.Second, Palette.First, Palette.Third]
        }
    }
    
    
    public enum Animation : String, CustomAnimation {
        case toeWiggle = "Wiggle"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 1.0
        }
    }
    
    public var boundaries: [ModelBoundaryDescription] = []
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }
    
    public func animate() {
        pop()
    }

    public init?() {
        super.init(named: FootModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)

        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)

        // <rdar://problem/48716761>
        pivot = SCNMatrix4MakeRotation(Float.pi / 2.0, 0, 1.0, 0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var onSelected: (() -> Void) = { }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Foot", comment: "AX title for the foot model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored foot with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored foot.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored foot with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored foot.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored foot with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored foot.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored foot with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored foot.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored foot with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored foot.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored foot with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored foot.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored foot with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored foot.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A blue foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A white colored foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A yellow foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A blue foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink foot with five toes, each wearing black nail polish.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored foot with five toes, each wearing nail polish.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate foot.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of foot.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between foot", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and foot", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
