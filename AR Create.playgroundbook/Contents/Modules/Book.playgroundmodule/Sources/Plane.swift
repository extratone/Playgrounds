//
//  Plane.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import ARKit

public final class Plane : _InternalPlane, Selectable {
    var width: Float { return anchor.extent.x }

    var height: Float { return anchor.extent.z }

    public var onSelected: (() -> Void) = { }
}
