//
//  Assessments.swift
//
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import os
import PlaygroundSupport
import AVFoundation

public class AssessmentManager {
    
    static let learningTrails = LearningTrailsProxy()
    
    static let page = PlaygroundPage.current
    
    static let checker = ContentsChecker(contents: page.text)
    
    public static func callAssessment() {
        if checker.contents.contains("01.playgroundpage") {
            AssessmentManager.page01assessmentPoint()
        } else if checker.contents.contains("02.playgroundpage") {
            AssessmentManager.page02assessmentPoint()
        } else if checker.contents.contains("03.playgroundpage") {
            AssessmentManager.page03assessmentPoint()
        } else if checker.contents.contains("04.playgroundpage") {
            AssessmentManager.page04assessmentPoint()
        } else if checker.contents.contains("06.playgroundpage") {
            AssessmentManager.page06assessmentPoint()
        } else if checker.contents.contains("07.playgroundpage") {
            AssessmentManager.page07assessmentPoint()
        } else {
            // Do nothing for AR Create pages.
        }
    }
    
    public static func page01assessmentPoint(success: Bool = true) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        
        if learningTrails.currentStep == "cameraPermission" {
            if status == .denied {
                learningTrails.sendMessageOnce("cameraPermission-hint")
            } else {
                if success {
                    learningTrails.setAssessment("cameraPermission", passed: true)
                    learningTrails.sendMessageOnce("cameraPermission-success")
                }
            }
        }
        
        if learningTrails.currentStep == "changedFunction" {
            if checker.functionCallCount(forName: "doSomethingMysterious") == 1 || checker.functionCallCount(forName: "doSomethingFriendly") == 1  {
                if success {
                    learningTrails.setAssessment("changedFunction", passed: true)
                    learningTrails.sendMessageOnce("changedFunction-success")
                }
            } else {
                learningTrails.sendMessageOnce("changedFunction-hint")
            }
        }
    }
    
    public static func page02assessmentPoint(success: Bool = true) {
                    
        if learningTrails.currentStep == "addModel" {
            if checker.functionCallCount(forName: "scene.add") >= 1 {
                if success {
                    learningTrails.setAssessment("addModel", passed: true)
                    learningTrails.sendMessageOnce("addModel-success")
                }
            } else {
                learningTrails.sendMessageOnce("addModel-hint")
                
            }
        }
        
        if learningTrails.currentStep == "animateModel" {
            if checker.functionCallCount(containing: "animate") > 0 {
                if success {
                    learningTrails.setAssessment("animateModel", passed: true)
                    learningTrails.sendMessageOnce("animateModel-success")
                }
            } else {
                learningTrails.sendMessageOnce("animateModel-hint")
                
            }
        }
        
        if learningTrails.currentStep == "changeModel" {
            if checker.variableAccessCount(containing: "snail") < 1 && checker.functionCallCount(containing: "animate") > 0 && checker.functionCallCount(containing: "add") > 0 {
                if success {
                    learningTrails.setAssessment("changeModel", passed: true)
                    learningTrails.sendMessageOnce("changeModel-success")
                }
            } else {
                learningTrails.sendMessageOnce("changeModel-hint")
                
            }
        }
        
    }
    
    public static func page03assessmentPoint(success: Bool = true) {
        var rotateModel = checker.passedArguments.filter { $0.contains("rotateBy")}.count > 0
        var scaleModel = checker.passedArguments.filter { $0.contains("scaleTo")}.count > 0
        var moveModelBy = checker.passedArguments.filter { $0.contains("moveBy")}.count > 0
        var moveModelTo = checker.passedArguments.filter { $0.contains("moveTo")}.count > 0

        var experimentPassed = rotateModel || scaleModel || moveModelBy || moveModelTo
        
        if success && learningTrails.currentStep == "experimentActions" {
            if rotateModel {
                learningTrails.setTask("rotate", completed: true)
            }
            
            if scaleModel {
                learningTrails.setTask("scale", completed: true)
            }
            
            if moveModelBy {
                learningTrails.setTask("moveBy", completed: true)
            }
            
            if moveModelTo {
                learningTrails.setTask("moveTo", completed: true)
            }
            
            if experimentPassed {
                learningTrails.setAssessment("experimentActions", passed: true)
                learningTrails.sendMessageOnce("experiment-success")
            }
        }
        
        if learningTrails.currentStep == "moveRainCloud" {
            let rainCloudArgs = checker.passedArguments(forCall: "rainCloud.run")
            if rainCloudArgs.filter { $0.contains("moveTo")}.count > 0  && rainCloudArgs.filter { $0.contains("snail.location")}.count > 0 {
                if success {
                    learningTrails.setAssessment("moveRainCloud", passed: true)
                    learningTrails.sendMessageOnce("moveRainCloud-success")
                }
            } else {
                if learningTrails.hasSentMessage("moveRainCloud-hint") {
                    learningTrails.sendMessageOnce("moveRainCloud-solution")
                }
                learningTrails.sendMessageOnce("moveRainCloud-hint")
            }
        }
    }
    
    public static func page04assessmentPoint(success: Bool = true) {
        if learningTrails.currentStep == "distanceEvent" {
            if checker.functionCallCount(forName: "scene.camera.when") >= 1 {
                if success {
                    learningTrails.setAssessment("distanceEvent", passed: true)
                    learningTrails.sendMessageOnce("distanceEvent-success")
                }
            } else {
                learningTrails.sendMessageOnce("distanceEvent-hint")
            }
        }
        
        if learningTrails.currentStep == "completionEvent" {
            if checker.passedArguments(forCallContaining: "animate").filter { $0.contains("spines") }.count > 0 {
                if success {
                    learningTrails.setAssessment("completionEvent", passed: true)
                    learningTrails.sendMessageOnce("completionEvent-success")
                }
            } else {
                learningTrails.sendMessageOnce("completionEvent-hint")
            }
        }
        
        var animateModel = checker.functionCallCount(containing: "animate") > 2
        var disappearModel = checker.functionCallCount(containing: "disappear") > 0
        var applyColors = checker.functionCallCount(containing: "applyColor") > 0
        var playSound = checker.functionCallCount(containing: "play") > 0
        var speakModel = checker.functionCallCount(containing: "speak") > 2
        
        var experimentPassed = animateModel || disappearModel || applyColors || playSound || speakModel
        
        if success {
            if animateModel {
                learningTrails.setTask("animate", completed: true)
            }
            
            if disappearModel {
                learningTrails.setTask("disappear", completed: true)
            }
            
            if applyColors {
                learningTrails.setTask("applyColors", completed: true)
            }
            
            if playSound {
                learningTrails.setTask("playSound", completed: true)
            }
            
            if speakModel {
                learningTrails.setTask("speak", completed: true)
            }
        }
        
        
        if learningTrails.currentStep == "usingEventsExperiment" {
            if experimentPassed {
                if success {
                    learningTrails.setAssessment("usingEventsExperiment", passed: true)
                    learningTrails.sendMessageOnce("usingEventsExperiment-success")
                }
            } else {
                learningTrails.sendMessageOnce("usingEventsExperiment-hint")
            }
        }
    
    }
    
    public static func page06assessmentPoint(success: Bool = true) {
        if learningTrails.currentStep == "follow" {
            if checker.functionCallCount(containing: "follow") >= 1 {
                if success {
                    learningTrails.setAssessment("follow", passed: true)
                    learningTrails.sendMessageOnce("follow-success")
                }
            } else {
                learningTrails.sendMessageOnce("fleeFollow-hint")
            }
        }
        
        if learningTrails.currentStep == "flee" {
            if checker.functionCallCount(containing: "flee") >= 1 && checker.functionCallCount(containing: "when") >= 1 {
                if success {
                    learningTrails.setAssessment("flee", passed: true)
                    learningTrails.sendMessageOnce("flee-success")
                }
            } else {
                learningTrails.sendMessageOnce("fleeFollow-hint")
            }
        }
        
    }

    public static func page07assessmentPoint(success: Bool = true) {
        
        if learningTrails.currentStep == "ARExperiences" {
            if checker.calledFunctions.contains("followTheFeet") || checker.calledFunctions.contains("exploreWithTouch") || checker.calledFunctions.contains("smellTheRoses()") {
                if success {
                    learningTrails.setAssessment("ARExperiences", passed: true)
                    learningTrails.sendMessageOnce("ARExperiences-success")
                }
            } else {
                learningTrails.sendMessageOnce("ARExperiences-hint")
            }
        }
        
        var addedModels = false
        var playedSound = false
        var triggeredCodeOnStart = false
        var animatedModel = false
        var appliedColors = false
        var appliedEffects = false
        var ranActions = false
        var ranDistanceEvent = false
        var fleeOrFollow = false
        
        if success {
            if checker.calledFunctions.contains("scene.add") {
                addedModels = true
                learningTrails.setTask("createModel", completed: true)
            }
            
            if checker.functionCallCount(containing: "play") > 0 || checker.functionCallCount(containing: "speak") > 0 {
                playedSound = true
                learningTrails.setTask("toolkitSound", completed: true)
            }
            
            if checker.calledFunctions.contains("scene.setOnStartHandler") {
                triggeredCodeOnStart = true
                learningTrails.setTask("triggerCode", completed: true)
            }
            
            if checker.functionCallCount(containing: "animate") > 0 {
                animatedModel = true
                learningTrails.setTask("toolkitAnimate", completed: true)
            }
            
            if checker.functionCallCount(containing: "apply") > 0 {
                appliedColors = true
                learningTrails.setTask("toolkitColors", completed: true)
            }
            
            if checker.functionCallCount(containing: "run") > 0 {
                appliedEffects = true
                learningTrails.setTask("toolkitActions", completed: true)
            }
            
            if checker.passedArguments.filter { $0.contains("group") }.count > 0 || checker.passedArguments.filter { $0.contains("sequence") }.count > 0 {
                ranActions = true
                learningTrails.setTask("runGroups", completed: true)
            }
            
            if checker.calledFunctions.contains("scene.camera.when") {
                ranDistanceEvent = true
                learningTrails.setTask("distanceEvents", completed: true)
            }
            
            if checker.functionCallCount(containing: "flee") > 0 || checker.functionCallCount(containing: "follow") > 0 {
                fleeOrFollow = true
                learningTrails.setTask("fleeFollow", completed: true)
            }
            
            if learningTrails.currentStep == "ARCreate" {
                if addedModels || playedSound || triggeredCodeOnStart || animatedModel || appliedColors || appliedEffects || ranActions || ranDistanceEvent || fleeOrFollow {
                    learningTrails.sendMessageOnce("ARCreate")
                    learningTrails.setAssessment("ARCreate", passed: true)
                }
            }
        }
    }
}


public class LearningTrailsProxy {
    
    private static let currentTrailKey = "LearningTrails.currentTrail"
    private static let currentStepKey = "LearningTrails.currentStep"
    
    /// The key under which the names of sent messages are saved in the key-value store (per page/trail).
    private var sentMessagesKey: String? {
        guard !currentTrailIdentifier.isEmpty else {
            os_log("LearningTrailsProxy: failed to create sentMessagesKey: missing currentTrailIdentifier.", log: OSLog.default, type: .error)
            return nil
        }
        return "LearningTrailsProxy.SentMessages.\(currentTrailIdentifier)"
    }
    
    public init() { }
    
    /// A persistent array of the messages that have been sent.
    var sentMessages: [String] {
        get {
            guard
                let sentMessagesKey = sentMessagesKey,
                case let .array(playgroundValues)? = PlaygroundKeyValueStore.current[sentMessagesKey]
            else { return [] }
            var messageNames = [String]()
            messageNames = playgroundValues.compactMap { playgroundValue in
                guard case let .string(messageName) = playgroundValue else { return nil }
                return messageName
            }
            return messageNames
        }
        set {
            guard let sentMessagesKey = sentMessagesKey else { return }
            PlaygroundKeyValueStore.current[sentMessagesKey] = .array(newValue.map { PlaygroundValue.string($0) } )
        }
    }
    
    /// Returns a dictionary of key-value pairs for the specified key.
    func getKeyValueStoreInfoFor(key: String) -> [String : String]? {
        guard case let .dictionary(valueDict)? = PlaygroundKeyValueStore.current[key] else { return nil }
        var info = [String : String]()
        if let value = valueDict["Identifier"], case let .string(identifier) = value {
            info["Identifier"] = identifier
        }
        if let value = valueDict["Name"], case let .string(name) = value {
            info["Name"] = name
        }
        return info
    }
    
    /// The name of the current learning trail (as defined in LearningTrail.xml).
    public var currentTrail: String {
        guard
            let trailInfo = getKeyValueStoreInfoFor(key: Self.currentTrailKey),
            let trailName = trailInfo["Name"]
        else { return "" }
        return trailName
    }
    
    /// The identifier of the current trail.
    public var currentTrailIdentifier: String {
        guard
            let trailInfo = getKeyValueStoreInfoFor(key: Self.currentTrailKey),
            let trailIdentifier = trailInfo["Identifier"]
        else { return "" }
        return trailIdentifier
    }
    
    /// The name of the current step (as defined in LearningTrail.xml).
    public var currentStep: String {
        guard
            let stepInfo = getKeyValueStoreInfoFor(key: Self.currentStepKey),
            let stepName = stepInfo["Name"]
        else { return "" }
        return stepName
    }
    
    /// The identifier of the current step.
    public var currentStepIdentifier: String {
        guard
            let stepInfo = getKeyValueStoreInfoFor(key: Self.currentStepKey),
            let stepIdentifier = stepInfo["Identifier"]
        else { return "" }
        return stepIdentifier
    }
    
    /// Sends actions to the learning trail.
    /// - parameter actions: The actions to be sent.
    public func sendActions(_ actions: [String]) {
        PlaygroundPage.current.assessmentStatus = .fail(hints: actions, solution: nil)
    }
    
    /// Sends a message to be displayed in the learning trail.
    /// To send a message defined in LearningTrail.xml just specify its `name`.
    /// To send an ad hoc message specify `name`, `sender`, `content`, and optionally `scope`.
    /// - parameter name: The name of the message to be sent.
    /// - parameter sender: The character the message is to be sent from: byte, blue, hopper or expert.
    /// - parameter scope: The scope of the message: trail, step (the default), or ephemeral.
    /// - parameter content: The content of the message.
    public func sendMessage(_ name: String, sender: String? = nil, scope: String? = nil, content: String? = nil) {
        var action = "learningtrails://sendChatMessage?name=\(name)"
        if let sender = sender, let content = content, let encodedContent = content.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            action += "&sender=\(sender)"
            if let scope = scope {
                action += "&scope=\(scope)"
            }
            action += "&content=\(encodedContent)"
        }
        sendActions([action])
        sentMessages.append(name)
    }
    
    /// Sends a message to be displayed in the learning trail, but will only send the message one time.
    /// To send a message defined in LearningTrail.xml just specify its `name`.
    /// To send an ad hoc message specify `name`, `sender`, `content`, and optionally `scope`.
    /// - parameter name: The name of the message to be sent.
    /// - parameter sender: The character the message is to be sent from: byte, blue, hopper or expert.
    /// - parameter scope: The scope of the message: trail, step (the default), or ephemeral.
    /// - parameter content: The content of the message.
    public func sendMessageOnce(_ name: String, sender: String? = nil, scope: String? = nil, content: String? = nil) {
        guard !hasSentMessage(name) else { return }
        sendMessage(name, sender: sender, content: content)
      }
    
    /// Returns `true` if the specified message has been sent.
    /// - parameter messageName: The name of the message.
    public func hasSentMessage(_ messageName: String) -> Bool {
        return sentMessages.contains(messageName)
    }
    
    /// Sets the assessment status.
    /// - parameter assessmentName: The name of the assessment (as defined in LearningTrail.xml).
    /// - parameter passed: The status of the assessment to be set.
    public func setAssessment(_ assessmentName: String, passed: Bool) {
        let action = "assessment://assessmentPassed?name=\(assessmentName)&passed=\(passed)"
        sendActions([action])
    }
    
    /// Marks a task as completed.
    /// - parameter taskName: The name of the task (as defined in LearningTrail.xml).
    /// - parameter completed: The status of the task to be set.
    public func setTask(_ taskName: String, completed: Bool) {
        let action = "learningtrails://setTaskCompleted?name=\(taskName)&completed=\(completed)"
        sendActions([action])
    }
}
