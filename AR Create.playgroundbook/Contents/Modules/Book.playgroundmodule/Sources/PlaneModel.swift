//
//  PlaneModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class PlaneModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "PlaneModel"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    init(width: CGFloat, length: CGFloat) {
        super.init()

        let plane = SCNPlane(width: width, height: length)
        geometry = plane
        _createPhysicsBody(for: plane)
        applyColor(scheme: defaultScheme)
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink]

        case .basic:
            return [.limeAqua]

        case .wacky:
            return [.yellow]
            
        case .cool:
            return [.bluePink]
            
        case .moody:
            return [.black]
            
        case .hot:
            return [.redYellow]

        case .custom:
            return [Palette.First]
        }
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Plane", comment: "AX title for the plane model. 'plane' describes a flat surface in the AR scene.")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored plane with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored plane.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored plane with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored plane.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored plane with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored plane.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored plane with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored plane.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored plane with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored plane.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored plane with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored plane.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored plane with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored plane.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A flat, lime colored surface, just hanging out.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A flat, black colored surface, just hanging out.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A flat, pink surface, just hanging out.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A flat, blue surface, just hanging out.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A flat, red surface, just hanging out.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A flat, yellow surface, just hanging out.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A flat, uniquely colored surface, just hanging out.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate plane.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of plane.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between surface", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and surface", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
