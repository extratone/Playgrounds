//
//  NoseModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit
import UIKit

public final class NoseModel : _InternalModel, ImaginaryModel, CustomAnimatable, ParticleProtocol {
    public var particleSize: Float = 0.008
    public var particleColor: ColorDefinition = .clear
    
    public static var Identifier = "Nose"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .black, .pink]

        case .basic:
            return [.pink, .blue, .aqua]

        case .wacky:
            return [.aqua, .blue, .lime]
            
        case .cool:
            return [.blue, .whiteAqua, .blue]
            
        case .moody:
            return [.white, .blackBlack, .blackBlack]
            
        case .hot:
            return [.yellowRed, .blackBlack, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second]
        }
    }

    public func particleColor(for scheme: ColorScheme) -> ColorDefinition {
        switch scheme {
        case .tacky:
            return .lime

        case .basic:
            return .yellow

        case .wacky:
            return .pink

        case .cool:
            return .aqua

        case .moody:
            return .black

        case .hot:
            return .red

        case .custom:
            return Palette.First
        }
    }
    

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: NoseModel.Identifier, verticalOffset: .offPlane(CGFloat(5.cm)))
        self.name = NoseModel.Identifier
        applyColor(scheme: defaultScheme)
        
        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)
    }

    public enum Animation : CustomAnimation {
        case drip
    }

    /**
     Runs the provided animation.
     
     - Parameter animation: The animation to run.
     - Parameter repeats: The RepeatingType, which determines how many times the animation runs.
     - Parameter completion: The handler to run on completion of the animation running. There is no completion handler by default.
     
     - localizationKey: NoseModel.animate(_:repeats:completion:)
     */
    public func animate(_ animation: NoseModel.Animation, repeats: RepeatingType = .none, completion: (() -> (Void))? = {}) {
        switch animation {
        case .drip:
            self.play(.drip, loops: true, completion: nil)
            drip()
        }
    }
    
    public func scaleParticles(by value: Float) {
        let emitterNodes = Scene.particleSystemNodes.filter { $0.name == NoseModel.Identifier }
        for node in emitterNodes {
            if let particleSystem = node.particleSystems {
                for system in particleSystem {
                    // Need to scale the size of the drips at a lesser rate than 1:1 with the model. Subtract 1 from value then divide by 2 to decrease the rate of increase or decrease by half.
                    let scaleFactor: Float = ((value - 1) / 2) + 1
                    system.particleSize = system.particleSize * CGFloat(scaleFactor)
                }
            }
        }
    }
    
    public func scaleParticles(to value: Float) {
        let emitterNodes = Scene.particleSystemNodes.filter { $0.name == NoseModel.Identifier }
        for node in emitterNodes {
            if let particleSystem = node.particleSystems {
                for system in particleSystem {
                    let scaleFactor: Float = ((value - 1) / 2) + 1
                    system.particleSize = CGFloat(self.particleSize * scaleFactor)
                }
            }
        }
    }

    fileprivate func drip() {
        if let emitter = SCNParticleSystem(named: "NoseDrip.scnp", inDirectory: nil), let explode = SCNParticleSystem(named: "ParticleRemoveNose", inDirectory: nil) {
            emitter.particleSize = emitter.particleSize * CGFloat(scale.x)
            emitter.particleDiesOnCollision = true
            emitter.systemSpawnedOnDying = explode
            emitter.colliderNodes = Scene.colliderNodesForParticles(from: self)
           
            let verticalOffset = 1.5.cm
            let horizontalOffset = 0.5.cm
            let zOffset = 0.5.cm

            emitter.particleColor = particleColor.asColor()
            explode.particleColor = particleColor.asColor()
            
            let leftNostril = SCNNode()
            leftNostril.name = NoseModel.Identifier
            self.addChildNode(leftNostril)
            Scene.particleSystemNodes.append(leftNostril)
            leftNostril.position = SCNVector3(-horizontalOffset - 0.3.cm, verticalOffset, zOffset) * self.scale
            leftNostril.addParticleSystem(emitter)
            
            Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
                let rightNostril = SCNNode()
                rightNostril.name = NoseModel.Identifier
                self.addChildNode(rightNostril)
                Scene.particleSystemNodes.append(rightNostril)
                rightNostril.position = SCNVector3(horizontalOffset, verticalOffset, zOffset) * self.scale
                rightNostril.addParticleSystem(emitter)
                timer.invalidate()
            }
            

        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Nose", comment: "AX title for the nose model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored nose with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored nose.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored nose with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored nose.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored nose with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored nose.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored nose with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored nose.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored nose with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored nose.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored nose with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored nose.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored nose with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored nose.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A triangular, pink nose.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A triangular, white colored nose.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A triangular, yellow nose.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A triangular, blue nose.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A triangular, orange nose.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A triangular, aqua blue nose.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A triangular, uniquely colored nose.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate nose.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of nose.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between nose", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and nose", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}

