//
//  ModelBoundaries.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit

public typealias ModelBoundaryDescription = (object: ImaginaryNode,
                                             threshold: Float,
                                             lastThrown: Date?,
                                             handler: (() -> Void))

public protocol ProjectsBoundary: class {
    var boundaries: [ModelBoundaryDescription]  { get set }
    var boundaryOwners: [ImaginaryNode] { get set }

    func when(_ object: ImaginaryNode, isWithin: Float, do handler: @escaping (() -> Void))
}


public extension ProjectsBoundary where Self : ImaginaryNode {
    func when(_ object: ImaginaryNode, isWithin: Float, do handler: @escaping (() -> Void)) {
        // 1. Add Boundary Physics body to self
        let sphere = SCNSphere(radius: CGFloat(isWithin))
        let barrierNode = SCNNode()
        addChildNode(barrierNode)

        let physicsShape = SCNPhysicsShape(geometry: sphere, options: nil)
        let physicsBody = SCNPhysicsBody(type: .kinematic, shape: physicsShape)
        physicsBody.isAffectedByGravity = false
        physicsBody.contactTestBitMask = Int(SCNPhysicsCollisionCategory.default.rawValue)
        barrierNode.physicsBody = physicsBody

        // 2. Store description
        let desc = ModelBoundaryDescription(object: object, threshold: isWithin, handler: handler, lastThrown: nil)
        boundaries.append(desc)
        
        // 3. Bookkeeping to allow the object to know who's holding the other end of the event
        object.boundaryOwners.append(self)
    }
}

extension ARLiveViewController : SCNPhysicsContactDelegate {
    public func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        // 0. If neither project a boundary, then move on
        guard contact.nodeA.parent is ProjectsBoundary || contact.nodeB.parent is ProjectsBoundary else { return }

        if let test1 = contact.nodeA.parent as? ProjectsBoundary,
           let test2 = contact.nodeB as? ImaginaryNode {
            testContact(projector: test1, node: test2)
            return
        }

        if let test1 = contact.nodeA as? ImaginaryNode,
           let test2 = contact.nodeB.parent as? ProjectsBoundary {
            testContact(projector: test2, node: test1)
            return
        }
    }

    func testContact(projector: ProjectsBoundary, node: ImaginaryNode) {
        for (index, boundary) in projector.boundaries.enumerated() {
            projector.boundaries[index] = handleContact(for: boundary, from: node)
        }
    }
    
    func handleContact(for boundary: ModelBoundaryDescription, from: ImaginaryNode) -> ModelBoundaryDescription {
        guard boundary.object == from else { return boundary }
        guard let timestamp = boundary.lastThrown else {
            DispatchQueue.main.async {
                boundary.handler()
            }
            var updated = boundary
            updated.lastThrown = Date()
            return updated
        }

        if abs(timestamp.timeIntervalSinceNow) > TimeInterval(5.0) {
            DispatchQueue.main.async {
                boundary.handler()
            }
            var updated = boundary
            updated.lastThrown = Date()
            return updated
        }

        return boundary
    }
}
