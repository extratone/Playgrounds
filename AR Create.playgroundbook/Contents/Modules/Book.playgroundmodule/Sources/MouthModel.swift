//
//  MouthModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class MouthModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Mouth"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.pink, .lime, .black]
            
        case .basic:
            return [.red, .lime, .aqua]

        case .wacky:
            return [.lime, .blue, .pink]
            
        case .cool:
            return [.black, .aqua, .blue]
            
        case .moody:
            return [.white, .black, .white]
            
        case .hot:
            return [.yellow, .yellowRed, .black]

        case .custom:
            return [Palette.Third, Palette.First, Palette.Second]
        }
    }
    
    
    public enum Animation : String, CustomAnimation {
        case frown = "Frown"
        case smile = "Smile"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 2.0
        }
    }

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public func animate() {
        pop()
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }
    
    public init?() {
        super.init(named: MouthModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Mouth", comment: "AX title for the mouth model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored mouth with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored mouth.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored mouth with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored mouth.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored mouth with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored mouth.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored mouth with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored mouth.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored mouth with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored mouth.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored mouth with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored mouth.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored mouth with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored mouth.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A flat lime colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A flat black colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A flat lime colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A flat aqua blue colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A flat orange colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A flat blue colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A flat uniquely colored mouth with no hint of expression. You should try to make it smile.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate mouth.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of mouth.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between mouth", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and mouth", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
