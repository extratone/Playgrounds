//
//  ArrowModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class ArrowModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public var emitterOffset : (Float, Float) = (0, 0)
    
    public enum Animation : String, CustomAnimation {
        case bounceAndPoint = "PointBounce"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 4.0
        }
    }

    public var scheme: ColorScheme = .basic

    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink]

        case .basic:
            return [.pink]

        case .wacky:
            return [.aquaLime]
            
        case .cool:
            return [.blueAqua]
            
        case .moody:
            return [.black]
            
        case .hot:
            return [.yellowRed]

        case .custom:
            return [Palette.First]
        }
    }
    
    public func animate() {
        pop()
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: 0)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: 0, completion: {
            self.reset(includeActions: false)
        })
    }
    
    public static var Identifier = "Arrow"

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: ArrowModel.Identifier, rigged: true, verticalOffset: .offPlane(CGFloat(2.cm)))
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Arrow", comment: "AX title for the arrow model")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored arrow with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored arrow.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored arrow with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored arrow.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored arrow with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored arrow.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored arrow with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored arrow.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored arrow with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored arrow.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored arrow with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored arrow.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored arrow with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored arrow.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A pink, marshmallow looking arrow.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black colored, marshmallow looking arrow.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A pink and lime colored, marshmallow looking arrow.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An aqua blue, marshmallow looking arrow.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange colored, marshmallow looking arrow.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A lime colored, marshmallow looking arrow.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored, marshmallow looking arrow.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate arrow.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of arrow.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between arrow", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and arrow", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
