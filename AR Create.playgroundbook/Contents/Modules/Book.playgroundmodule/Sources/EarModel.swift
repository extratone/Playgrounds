//
//  EarModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class EarModel : _InternalModel, ImaginaryModel {
    public var emitterOffset : (Float, Float) = (-2.cm, 0)

    public static var Identifier = "Ear"

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .black, .pink]

        case .basic:
            return [.pink, .red, .aqua]

        case .wacky:
            return [.blue, .lime, .red]
            
        case .cool:
            return [.aqua, .lime, .blue]
            
        case .moody:
            return [.white, .blackBlack, .blackBlack]
            
        case .hot:
            return [.whiteYellow, .yellowRed, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Second, Palette.Third]
        }
    }
    
    public func animate() {
        pop()
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    public var boundaries: [ModelBoundaryDescription] = []

    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: EarModel.Identifier)
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Ear", comment: "AX title for the ear model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored ear with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored ear.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored ear with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored ear.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored ear with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored ear.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored ear with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored ear.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored ear with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored ear.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored ear with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored ear.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored ear with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored ear.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A pink and red ear, looking for a head to call home.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black and white colored ear, looking for a head to call home.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A yellow and black colored ear, looking for a head to call home.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An aqua blue and lime colored ear, looking for a head to call home.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A yellow and orange ear, looking for a head to call home.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A blue and lime colored ear, looking for a head to call home.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored ear, looking for a head to call home.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate ear.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of ear.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between ear", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and ear", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
