//
//  ModelInteractions.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit
import ARKit


public protocol Selectable : class {
    var onSelected: (() -> Void) { get set }
    func setSelectionHandler(_ handler: @escaping (() -> Void))
}

public extension Selectable {
    func setSelectionHandler(_ handler: @escaping (() -> Void)) {
        self.onSelected = handler
    }
}

public protocol PlaceableContainer : class {
    func add(_ object: ImaginaryNode)

    func add(_ objects: [ImaginaryNode])

    func place(_ object: ImaginaryNode, at point: Point)
}

public protocol DecoratableProtocol {
    func attach(_ decoration: DecoratableProtocol)
}

public protocol CanFollowProtocol {
    func _internalFollow(_ object: ImaginaryNode, at distance: CGFloat, block: (() -> Void))
    func follow(_ object: ImaginaryNode, at distance: Float)
}

public extension CanFollowProtocol where Self : ImaginaryNode {
    func _internalFollow(_ object: ImaginaryNode, at distance: CGFloat, block: (() -> Void)) {
        let constraint = SCNDistanceConstraint(target: object)
        constraint.maximumDistance = distance
        constraint.minimumDistance = distance
        constraint.isIncremental = true
        constraint.influenceFactor = 0.003

        let look = self._internalLookConstraint(for: object)

        guard !Model.shouldFly(self) else {
            self.constraints = [look[0], look[1], constraint]
            block()
            return
        }

        // Model should not leave the ground as a result
        // of a follow, e.g., Snail, Cactus, Cone, etc.

        let nofly = SCNTransformConstraint.positionConstraint(inWorldSpace: false) { node, proposed in
            return SCNVector3(proposed.x, node.position.y, proposed.z)
        }
        nofly.isIncremental = false

        self.constraints = [look[0], look[1], constraint, nofly]
        block()
    }
    
    func follow(_ object: ImaginaryNode, at distance: Float) {
        _internalFollow(object, at: CGFloat(distance), block: { })
    }
}


public protocol CanFleeProtocol {
    func _internalFlee(from object: ImaginaryNode, safeDistance: CGFloat, block: (() -> Void))
    func flee(from object: ImaginaryNode, safeDistance: Float)
}

public extension CanFleeProtocol where Self : ImaginaryNode {
    func _internalFlee(from object: ImaginaryNode, safeDistance: CGFloat, block: (() -> Void)) {
        let constraint = SCNDistanceConstraint(target: object)
        constraint.maximumDistance = CGFloat.greatestFiniteMagnitude
        constraint.minimumDistance = safeDistance
        constraint.isIncremental = true
        constraint.influenceFactor = 0.003
        
        let look = self._internalLookConstraint(for: object, away: true)

        // No model should not burrow under the ground or float off
        // as a result of a flee.

        let nofly = SCNTransformConstraint.positionConstraint(inWorldSpace: false) { node, proposed in
            return SCNVector3(proposed.x, node.position.y, proposed.z)
        }
        nofly.isIncremental = false
        
        self.constraints = [look[0], look[1], constraint, nofly]
        
        block()
    }
    func flee(from object: ImaginaryNode, safeDistance: Float) {
        _internalFlee(from: object, safeDistance: CGFloat(safeDistance), block: { })
    }
}
