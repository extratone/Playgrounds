//
//  LightBulbModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class LightBulbModel : _InternalModel, ImaginaryModel {
    public static var Identifier = "LightBulb"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .black, .limePink]
            
        case .basic:
            return [.pink, .black, .lime]

        case .wacky:
            return [.aqua, .black, .bluePink]
            
        case .cool:
            return [.whiteAqua, .black, .blue]
            
        case .moody:
            return [.white, .black, .black]
            
        case .hot:
            return [.white, .black, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second, Palette.First]
        }
    }

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: LightBulbModel.Identifier)
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Light bulb", comment: "AX title for the light bulb model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored light bulb with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored light bulb.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored light bulb with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored light bulb.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored light bulb with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored light bulb.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored light bulb with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored light bulb.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored light bulb with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored light bulb.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored light bulb with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored light bulb.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored light bulb with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored light bulb.", comment: "AX description of a custom colored model")
        }
    }

    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("An incandescent light bulb emitting lime colored light with alternating pink and black bands on the socket.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("An incandescent light bulb emitting black colored light with alternating white and black bands on the socket.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("An incandescent light bulb emitting pink colored light with alternating yellow and black bands on the socket.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An incandescent light bulb emitting blue colored light with alternating aqua blue and black bands on the socket.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An incandescent light bulb emitting orange colored light with alternating white and black bands on the socket.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("An incandescent light bulb emitting pink colored light with alternating aqua blue and black bands on the socket.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("An incandescent light bulb emitting a uniquely colored light with alternating uniquely colored bands on the socket.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate light bulb.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of light bulb.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between light bulb", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and light bulb", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
