//
//  PageManager.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import PlaygroundSupport

public class PageManager {
    public static func setsFullScreenLiveViewIfPossible() {
        let executionMode = PlaygroundPage.current.executionMode

        if executionMode == .step || executionMode == .stepSlowly {
            PlaygroundPage.current.wantsFullScreenLiveView = false
        } else {
            PlaygroundPage.current.wantsFullScreenLiveView = true
        }
    }
}
