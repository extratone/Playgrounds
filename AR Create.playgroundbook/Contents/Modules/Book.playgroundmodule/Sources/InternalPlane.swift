//
//  InternalPlane.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import UIKit
import ARKit

public extension UIColor {
    static var featurePointsColor = UIColor(red: 88.0/255, green: 197.0/255, blue: 248.0/255, alpha: 1.0)
    static var detectedPlaneColor = UIColor(red: 88.0/255, green: 197.0/255, blue: 248.0/255, alpha: 0.25)
}

public class _InternalPlane: SCNNode {
    public let meshNode: SCNNode
    public let planeNode: SCNNode
    public let scene: Scene

    internal let anchor: ARPlaneAnchor

    public init(anchor: ARPlaneAnchor, in view: ARSCNView, parent: Scene, showDebug: Bool = false) {
        guard let meshGeometry = ARSCNPlaneGeometry(device: view.device!) else {
            fatalError("Can't create plane geometry")
        }
//        showDebugInfo = showDebug

        meshGeometry.update(from: anchor.geometry)
        meshNode = SCNNode(geometry: meshGeometry)
        meshNode.castsShadow = false

        let planeGeo = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
        planeNode = SCNNode(geometry: planeGeo)
        planeNode.simdPosition = anchor.center

        // `SCNPlane` is vertically oriented in its local coordinate space, so
        // rotate it to match the orientation of `ARPlaneAnchor`.
        planeNode.eulerAngles.x = -.pi / 2


        self.anchor = anchor
        self.scene = parent

        super.init()

        addChildNode(meshNode)
        addChildNode(planeNode)

        planeNode.physicsBody = SCNPhysicsBody.static()
        planeNode.physicsBody?.categoryBitMask = Int(SCNPhysicsCollisionCategory.default.rawValue)

        planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.clear

        makePlanesInivisible(false)


    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func makePlanesInivisible(_ invisible: Bool) {
        guard let material = meshNode.geometry?.firstMaterial else {
            print("ARSCNPlaneGeometry always has one material")
            return
        }

        material.isLitPerPixel = false
        material.lightingModel = .constant

        switch invisible {
        case true:
            meshNode.opacity = 1.0
            material.diffuse.contents = UIColor.white
            material.colorBufferWriteMask = SCNColorMask(rawValue: 0)
            material.blendMode = .alpha
        case false:
            meshNode.opacity = 0.8
            material.diffuse.contents = UIColor.detectedPlaneColor
            material.colorBufferWriteMask = .all
            material.blendMode = .screen
        }
    }

    internal func relativeTo(_ position: SCNVector3) -> SCNVector3 {
        return SCNVector3Make(anchor.center.x + position.x, 0.0 + position.y, anchor.center.z + position.z)
    }
}
