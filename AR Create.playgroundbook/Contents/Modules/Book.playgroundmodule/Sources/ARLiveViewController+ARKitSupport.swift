//
//  ARLiveViewController+ARKitSupport.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import ARKit

public extension ARLiveViewController {
    func displayUnsupportedMessage() {
        // This device does not support AR world tracking.
        let title = NSLocalizedString("This book is not supported on this device.", comment: "Title for unsupported iPad alert.")
        let message = NSLocalizedString("Augmented Reality requires an iPad Pro, iPad Air, iPad mini (5th generation), or iPad (5th or 6th generation) running iOS 11.3.", comment: "Message for augmented reality not supported.")
        let buttonTitle = NSLocalizedString("OK", comment: "OK button title for unsupported iPad alert.")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        blurBackground()
        alertController.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
            self.unblurBackground()
        }))
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: Background Blur

    private func blurBackground() {
        let blurEffectView = UIVisualEffectView()
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)

        UIView.animate(withDuration: 0.5) {
            blurEffectView.effect = UIBlurEffect(style: .light)
        }

        blurView = blurEffectView
    }

    private func unblurBackground() {
        guard let blurEffectView = blurView else { return }

        UIView.animate(withDuration: 0.5, animations: {
            blurEffectView.effect = nil
        }, completion: { (complete: Bool) in
            blurEffectView.removeFromSuperview()
            self.blurView = nil
        })
    }
}
