//
//  LightningBoltModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class LightningBoltModel : _InternalModel, ImaginaryModel {
    public static var Identifier = "Lightning_Bolt"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.pinkLime]

        case .basic:
            return [.yellow]

        case .wacky:
            return [.pink]
            
        case .cool:
            return [.blueAqua]
            
        case .moody:
            return [.white]
            
        case .hot:
            return [.redYellow]

        case .custom:
            return [Palette.First]
        }
    }
    

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: LightningBoltModel.Identifier, verticalOffset: .offPlane(CGFloat(2.cm)))
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Lightning", comment: "AX title for the lighting bolt model.")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored lightning bolt with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored lightning bolt.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored lightning bolt with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored lightning bolt.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored lightning bolt with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored lightning bolt.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored lightning bolt with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored lightning bolt.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored lightning bolt with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored lightning bolt.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored lightning bolt with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored lightning bolt.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored lightning bolt with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored lightning bolt.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A jagged, yellow lightning bolt.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A jagged, white colored lightning bolt.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A jagged, lime colored lightning bolt.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A jagged, aqua blue colored lightning bolt.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A jagged, yellow and orange colored lightning bolt.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A jagged, pink lightning bolt.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A jagged, uniquely colored lightning bolt.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate lightning bolt.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of lightning bolt.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between lightning bolt", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and lightning bolt", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
