//
//  InternalCamera.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import ARKit

public let DistanceEventEmitterNodeName: String = "DistanceEventEmitterNode"

public class _InternalCamera {
    public var node = _InternalCameraNode()

    func addDistanceEventDecoration(for object: ImaginaryNode) {
        let exp1 = SCNParticleSystem(named: "DistanceExplode", inDirectory: nil)!
        let exp2 = SCNParticleSystem(named: "DistanceLines", inDirectory: nil)!

        let emitterNode = SCNNode()
        emitterNode.name = DistanceEventEmitterNodeName
        object.addChildNode(emitterNode)
        // Add 3cm to the vertical offset of models due to their floor positioning
        emitterNode.position = SCNVector3(object.emitterOffset.0, object.emitterOffset.1 + _InternalModel.VerticalOffset, 0) * object.scale
        emitterNode.addParticleSystem(exp1)
        emitterNode.addParticleSystem(exp2)
    }
}

public final class _InternalCameraNode : ImaginaryNode {
    public static var Identifier = "CameraNode"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var voice: Voice = Voice()

    public func setPosition(_ position: Point) {
        simdPosition = position.simdPoint
    }

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []
    public var boundaryOwners: [ImaginaryNode] = []

    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        // Camera should be invisible
        return [.clear]
    }

    public var scheme: ColorScheme = .basic
    public func applyColor(scheme: ColorScheme) {
        // Do nothing
    }

    public func currentState() -> ModelState {
        return (transform: float4x4(0.0), scale: 0.0, alpha: 0.0, nil)
    }

    public func setState(_ state: ModelState) {
        // Do nothing
    }

    public var hasDistanceEvents: Bool {
        return false
    }
    
    public var titleDescription: String {
        return NSLocalizedString("Camera", comment: "AX title for the camera node")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between camera", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and camera", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

    // None of these should ever fire.
    public var shortDescription: String { return titleDescription }    
    public var detailedDescription: String { return titleDescription }
    public var locateModelActionTitle: String { return "" }
    public var describeModelActionTitle: String { return "" }

}
