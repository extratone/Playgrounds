//
//  Action.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit


//public class ActionGroup: Actionable {
//
//    public var group: [Action]
//
//    public enum ExecutionOrder {
//        case sequential
//        case simultaneous
//    }
//
//    public var order: ExecutionOrder
//    public var repeatingType: RepeatingType
//
//    public static func +(_ lhs: ActionGroup, _ rhs: ActionGroup) -> [ActionGroup] {
//        return [lhs, rhs]
//    }
//
//    public init(_ actions: [Action]) {
//        self.group = actions
//        self.order = .sequential
//        self.repeatingType = .none
//    }
//
//    public func run(on node: ImaginaryNode, completion: (() -> Void)? = nil) {
//        var action: Action
//
//        switch order {
//        case .sequential:
//            action = SCNAction.sequence(group)
//        case .simultaneous:
//            action = SCNAction.group(group)
//
//        }
//
//        switch repeatingType {
//        case .loop:
//            node.runAction(Action.repeatForever(action), completionHandler: completion)
//        case.none:
//            node.runAction(action, completionHandler: completion)
//        case .number(let x):
//            node.runAction(Action.repeat(action, count: x), completionHandler: completion)
//        case.reverse:
//            node.runAction(action.reversed(), completionHandler: completion)
//        }
//    }
//}
//
//extension Array where Element == ActionGroup {
//    public func run(on node: ImaginaryNode, completion: (() -> Void)? = nil) {
//        var array = self
//        guard array.count > 0 else {
//            completion?()
//            return
//        }
//
//        let g = array[0]
//        array = Array(array.dropFirst())
//
//        g.run(on: node, completion: {
//            array.run(on: node, completion: completion)
//        })
//    }
//}
