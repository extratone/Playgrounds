//
//  ARLiveViewController+Accessibility.swift
//
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import UIKit
import SceneKit.SceneKitTypes
import AVFoundation

internal protocol AccessibleElementTracking {
    static var activeAXElements: [ARAccessibilityElement] { get }
    func addAccessibleElement(for node: ImaginaryNode, controller: ARLiveViewController)
    func removeAccessibleElement(for node: ImaginaryNode)
    func removeAllAccessibleElements()
    func accessibleElement(for node: ImaginaryNode) -> ARAccessibilityElement?
    func accessibleItemsInScene() -> [ImaginaryNode]
}

public func isAnyAXRunning() -> Bool {
    return UIAccessibility.isVoiceOverRunning || UIAccessibility.isSwitchControlRunning
}

public func postAXNote(_ announcement: String, queued: Bool = false) {
    guard UIAccessibility.isVoiceOverRunning else { return }

    let attributedAnnouncement = NSAttributedString(string: announcement,
                                                    attributes: queued ? [.accessibilitySpeechQueueAnnouncement: true] : nil)
    UIAccessibility.post(notification: .announcement, argument: attributedAnnouncement)
}


public extension ARLiveViewController {
    func toggleAXUpdateTimer() {
        if isAnyAXRunning() {
            // First time turning on VO or SC, focus the scene view
            if axUpdateTimer == nil {
                UIAccessibility.post(notification: .layoutChanged, argument: sceneViewContainer)
            }

            if UIAccessibility.isVoiceOverRunning {
                // Disable the standard tap gesture recognizer, interferes with two-finger double-tap.
                installTapGesture(false)
            }
            
            axUpdateTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: true) { timer in 
                self._timerUpdate()
            }
        }
        else {
            axUpdateTimer?.invalidate()
            axUpdateTimer = nil

            // Enable the standard tap gesture recognizer
            installTapGesture(true)
        }
    }
    
    private func _timerUpdate() {
        if axSceneLastUpdated == nil || Date().timeIntervalSince(axSceneLastUpdated!) > 1.0 {
            axSceneLastUpdated = Date()
            
            _updateTrackedItemsAndAnnounce()
        }
    }
    
    // This handles not only the announcements in VoiceOver of the comings and goings of items, but updates the list of accessibility elements the scene tracks.
    private func _updateTrackedItemsAndAnnounce() {
        guard let localScene = scene, localScene.state == .started, let localTrackedItems = trackedItems else { return }

        let newItems = localScene.accessibleItemsInScene()
        var deltaAdded = [ImaginaryNode]()
        var deltaRemoved = [ImaginaryNode]()
        for newItem in newItems {
            var found = false
            for trackedItem in localTrackedItems {
                if trackedItem === newItem { found = true; break }
            }
            if !found {
                deltaAdded.append(newItem)
            }
        }
        for trackedItem in localTrackedItems {
            var found = false
            for newItem in newItems {
                if trackedItem === newItem { found = true; break }
            }
            if !found {
                deltaRemoved.append(trackedItem)
            }
        }
        
        var announcement:String? = nil
        if deltaAdded.count == 1 {
            announcement = String(format: NSLocalizedString("appeared: %@", comment: "AX announcement that an item has appeared. Placeholder is the item's titleDescription (e.g., Snail)"), deltaAdded.first!.titleDescription)
        }
        else if deltaAdded.count > 1 {
            announcement = String(format: NSLocalizedString("%d items appeared: %@", comment: "AX announcement that several items have appeared. Placeholder will be a comma-separated list of item titleDescriptions (e.g., Snail, Nose.)"), deltaAdded.count, deltaAdded.compactMap({$0.titleDescription}).joined(separator: ",")
            )
        }
        
        for addedNode in deltaAdded {
            localScene.addAccessibleElement(for: addedNode, controller: self)
        }
        
        if let note = announcement {
            postAXNote(note, queued: true)
        }
        
        announcement = nil
        if deltaRemoved.count == 1 {
            announcement = String(format: NSLocalizedString("disappeared: %@", comment: "AX announcement that an item has disappeared. Placeholder is the item's titleDescription (e.g., Snail)"), deltaRemoved.first!.titleDescription)
        }
        else if deltaRemoved.count > 1 {
            announcement = String(format: NSLocalizedString("%d items disappeared: %@", comment: "AX announcement that several items have disappeared. Placeholder will be a comma-separated list of item titleDescriptions (e.g., Snail, Nose.)"), deltaRemoved.count, deltaRemoved.compactMap({$0.titleDescription}).joined(separator: ",")
            )
        }
        
        for removedNode in deltaRemoved {
            localScene.removeAccessibleElement(for: removedNode)
        }
        
        if let note = announcement {
            postAXNote(note, queued: true)
        }
        
        if UIAccessibility.isVoiceOverRunning && (deltaAdded.count > 0 || deltaRemoved.count > 0) {
            let axElementsLeft = sceneViewContainer.accessibilityElements?.count ?? 0

            // AX APIs do not allow you to coordinate announcements. When we go to zero AX elements in the scene, focusing on the sceneViewController will truncate the last "item disappeared" announcement. We can't know how long that announcement will take, so delay one second before focusing on the thing we *do* know is in the scene: the reset button.
            if axElementsLeft == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { 
                    if let resetButton = self.resetSceneControl?.subviews.last as? UIButton {
                        UIAccessibility.post(notification: .layoutChanged, argument: resetButton)
                    }
                }
            }
            else {
                UIAccessibility.post(notification: .layoutChanged, argument: sceneViewContainer)
            }
        }
        
        trackedItems = newItems
    }
        
    private func findItemsVisibleOnScreen() -> [ImaginaryNode] {
        var foundNodes = [ImaginaryNode]()

        if let pov = sceneView.pointOfView {
            let allNodes = sceneView.nodesInsideFrustum(of: pov)
            for n in allNodes {
                if let castableNode = n as? ImaginaryNode {
                    foundNodes.append(castableNode)
                }
                else if n.name != nil {
                    if let match = _InternalScene.models.first(where: { m in
                        // The nodes that are hit-tested by nodesInsideFrustrum appear to be the deepest nodes in the model.
                        if let realParent = Model.exisitingModelContainingNode(n) { return m == realParent }
                        else { return false }
                    } ) {
                        foundNodes.append(match)
                    }
                }
            }
        }

        return foundNodes
    }

    override var accessibilityCustomActions: [UIAccessibilityCustomAction]? {
        set {}
        get {
            guard UIAccessibility.isVoiceOverRunning else { return nil }
            guard let localScene = scene, localScene.state == .preparingToStart || localScene.state == .started else { return nil }

            let descVisibleScene = UIAccessibilityCustomAction(name: NSLocalizedString("Describe visible scene", comment: "AX action to describe what is on screen at the moment"), target: self, selector: #selector(describeVisibleScene))

            return [descVisibleScene]
        }
    }
    
    // Magic Tap is "two-finger double tap".
    override func accessibilityPerformMagicTap() -> Bool {
        guard let localScene = scene else { return false }
        switch localScene.state {
        case .detectingPlanes:
            // Do nothing
            return false
        default:
            let centerScreen = CGPoint(x: sceneView.frame.midX, y: sceneView.frame.midY)
            _performTap(at: centerScreen)
        }
        return true
    }
    
    @objc
    func describeVisibleScene() -> Bool {
        var description:String = ""

        let visibleItems = findItemsVisibleOnScreen()
        if visibleItems.count == 0 {
            description.append(NSLocalizedString("No items visible on screen.", comment: "AX announcement for number of items on screen."))            
        }
        else if visibleItems.count == 1 {
            description.append(NSLocalizedString("One item visible on screen.", comment: "AX announcement for number of items on screen."))
        }
        else {
            description.append(String(format:NSLocalizedString("%d items visible on screen.", comment: "AX announcement for number of items on screen."), visibleItems.count))
        }
        for item in visibleItems {
            // No need to localize this string, the strings returned will already be localized.
            let itemDesc = String(format:"%@, %@, %@.", item.shortDescription, nodeHeadingFromCamera(from: item), descriptionForDistance(nodeDistanceToCamera(from: item)))
            description.append(itemDesc)
        }
        
        postAXNote(description, queued: true)
        return true
    }

    func playSoundEffect(_ effect: Sound) {
        guard UIAccessibility.isVoiceOverRunning else { return }

        if effectsPlayer == nil {
            effectsPlayer = AVPlayer()
        }

        if let effectURL = Bundle.main.url(forResource: effect.rawValue, withExtension: "m4a") {
            effectsPlayer?.replaceCurrentItem(with: AVPlayerItem(url: effectURL))
            effectsPlayer?.play()
        }
    }

    func accessibilityFrame(for element: ARAccessibilityElement) -> CGRect {
        guard let node = element.backingNode else { return .zero }

        let pNode = node.presentation // Must use the presentation node, otherwise all your calculations are going to be waaaay off.
        let (center, _) = pNode.boundingSphere // center of the sphere is more accurate than position, which sits lower than the model
        let distance = nodeDistanceToCamera(from: pNode.presentation)
        let worldCenter = sceneView.projectPoint(pNode.convertPosition(center, to: nil))

        // The closer we are, the larger the frame should be. 44px at 1m or more, 88px at 20cm or less.
        let side = CGFloat(100.0 - (max(0.20, min(distance, 1.0)) * 56.0))
        let origin = CGPoint(x: CGFloat(worldCenter.x) - side / 2.0, y: CGFloat(worldCenter.y) - side / 2.0)
        let size = CGSize(width: side, height: side)
        let rect = CGRect(origin: origin, size: size)

        return rect
    }
    
    fileprivate func descriptionForDistance(_ distance: Float) -> String {
        let distanceString: String
        if distance < 1.0 {
            distanceString = String(format:NSLocalizedString("Distance: %.0f centimeters.", comment:"AX announcement of how far away it item is from the iPad"), distance * 100)
        }
        else {
            distanceString = String(format:NSLocalizedString("Distance: %.1f meters.", comment:"AX announcement of how far away it item is from the iPad"), distance)
        }
        return distanceString
    }
    
    // This function is called at frame rate, so make it snappy
    func announcePlacementDistance() {
        guard UIAccessibility.isVoiceOverRunning else { return }
        guard let asset = selectedAsset else { return } // Not placing anything

        
        if lastPlacedNode == nil {
            // First time model placement, don't announce.
            lastDistanceAnnouncedDate = Date()
            lastPlacedNode = asset
            return
        }
        else { 
            if lastPlacedNode!.model !== asset.model {
                lastDistanceAnnouncedDate = Date()
                lastPlacedNode = asset
                announcementThreshold = 4.0
                return
            }
        }
        
        if let lastDate = lastDistanceAnnouncedDate {
            // announcementThreshold is longer on initial scene load, just too much to preflight the user on to overwhelm them with distance events.
            if Date().timeIntervalSince(lastDate) > announcementThreshold {
                let distanceString = descriptionForDistance(nodeDistanceToCamera(from: placementPuck))
                postAXNote(distanceString, queued: true)
                lastDistanceAnnouncedDate = Date()
                announcementThreshold = 4.0
            }
        }
    }
    
    func distanceBetweenNodes(_ a: SCNNode, _ b: SCNNode) -> Float {
        return SCNVector3.distanceBetween(a.position, to: b.position)
    }
    
    func nodeDistanceToCamera(from node: SCNNode) -> Float {
        guard let localScene = scene else { return 0.0 }
        return distanceBetweenNodes(localScene.camera.node, node)
    }

    private func headingTo(node: SCNNode) -> Float? {
        guard let pov = sceneView.pointOfView?.presentation else { return nil }
        
        // object rotation
        let rotO = atan2(node.presentation.worldPosition.x - pov.position.x, node.presentation.worldPosition.z - pov.position.z)
        
        // camera heading
        let mat = pov.transform
        let rotP = atan2f(-mat.m31, -mat.m33)
        
        // relative rotation with respect to heading
        var rotR = rotP - rotO
        rotR += (rotR > .pi ) ? -2 * .pi : (rotR < -.pi) ? 2 * .pi : 0
        
        return rotR
    }
    
    // Returns the degress and clock face position of a node relative to the camera
    private func clockPosition(for node: SCNNode) -> (Float, Int)? {
        guard let headingInRadians = headingTo(node: node) else { return nil }
        
        func _rad2deg(_ r: Float) -> Float {
            return r * 180.0 / .pi
        }

        let heading = _rad2deg(headingInRadians)
        
        var clockPos:Int
        switch heading {
        case -15 ..< 15:
            clockPos = 12
            
        case 15 ..< 45:
            clockPos = 1
        case 45 ..< 75:
            clockPos = 2
        case 75 ..< 105:
            clockPos = 3
        case 105 ..< 135:
            clockPos = 4
        case 135 ..< 165:
            clockPos = 5
            
        case 165 ..< 180:
            fallthrough
        case -180 ..< -165:
            clockPos = 6
            
        case -165 ..< -135:
            clockPos = 7
        case -135 ..< -105:
            clockPos = 8
        case -105 ..< -75:
            clockPos = 9
        case -75 ..< -45:
            clockPos = 10
        case -45 ..< -15:
            clockPos = 11
            
        default:
            clockPos = 14
        }
        
        if clockPos < 14 {
            return (heading, clockPos)
        }
        else {
            return nil
        }
        
    }

    fileprivate func nodeHeadingFromCamera(from node: SCNNode) -> String {
        var heading: String
        if let (_, clockPos) = clockPosition(for: node) {
            let distance = nodeDistanceToCamera(from: node)
            
            if distance < 0.15 { // Closer than 15cm, we're effectively on top/underneath of the model
                heading = String(format:NSLocalizedString("Heading: %d o'clock, directly above or below you", comment: "AX description of the relative position of a node to the user when they are pretty much right on top of it." ), clockPos)
            }
            else {
                heading = String(format:NSLocalizedString("Heading: %d o'clock", comment: "AX description of the relative position of a node to the user." ), clockPos)
            }
        }
        else {
            heading = NSLocalizedString("Unknown heading", comment: "AX description when we can't discern a heading for an element")
        }
        
        return heading
    }
    
    func workaround_preventDictationAlert() {
        guard UIAccessibility.isVoiceOverRunning else { return }

        // So this is unfortunate. As we are relying on two-finger double-tap pretty heavily, we can find ourselves in a situation where there's an active editing session in the editor while the user is attempting to interact with the content. This gesture, when the firstResponder is on a text editor, defaults to enabling dication. The normal mitigations for this do not work as we are in an extension and VoiceOver will not have the opportunity to find us before finding the text editor. So we need to forcfully remove firstResponder status from the editor to allow Magic Tap to work.
        // Enhancement request: <rdar://problem/53567822>
        let button = AXWorkaroundButton(frame: CGRect(x: -500, y: -500, width: 10, height: 10))
        view.addSubview(button)
        if button.becomeFirstResponder() {
            button.resignFirstResponder()
        }
        button.removeFromSuperview()
    }
}

// Please see workaround_preventDictationAlert for why this is needed <rdar://problem/53567822>
private class AXWorkaroundButton: UIButton {
    override var canBecomeFirstResponder: Bool { return true }
}

// MARK: - ARAccessibilityElement
public class ARAccessibilityElement: UIAccessibilityElement {
    weak var backingNode: ImaginaryNode? = nil
    weak var controller: ARLiveViewController? = nil
    init(node: ImaginaryNode, controller: ARLiveViewController) {
        self.backingNode = node
        self.controller = controller
        super.init(accessibilityContainer: controller.sceneViewContainer!)
    }
    
    public override var accessibilityFrame: CGRect {
        set {}
        get {
            return controller?.accessibilityFrame(for: self) ?? .zero
        }
    }
    
    public override var accessibilityLabel: String? {
        set {}
        get {
            return backingNode?.shortDescription
        }
    }
    
    public override func accessibilityPerformMagicTap() -> Bool {
        return controller?.accessibilityPerformMagicTap() ?? false
    }

    public override func accessibilityActivate() -> Bool {
        // Don't do anything if we're not in a started state
        guard let axCon = controller, axCon.scene?.state == .started else { return false }
        
        // No node? No activate.
        guard let node = backingNode else { return false }
        
        let center = node.presentation.boundingSphere.center
        let worldCenter = axCon.sceneView.projectPoint(node.presentation.convertPosition(center, to: nil))
        axCon._performTap(at: CGPoint(x: CGFloat(worldCenter.x), y: CGFloat(worldCenter.y)))
        
        return true
    }

    public override var accessibilityCustomActions: [UIAccessibilityCustomAction]? {
        set {}
        get {
            guard let node = backingNode else { return nil }
            
            var allActions = [UIAccessibilityCustomAction]()

            if UIAccessibility.isVoiceOverRunning {
                // Actions that are always available
                let findModelAction = UIAccessibilityCustomAction(name: node.locateModelActionTitle, target: self, selector: #selector(findModel))
                let describeModelAction = UIAccessibilityCustomAction(name: node.describeModelActionTitle, target: self, selector: #selector(detailedDescription))

                allActions.append(findModelAction)
                allActions.append(describeModelAction)
            }

            // Actions available only when the scene is running
            if let localScene = controller?.scene, localScene.state == .started {
                if node.hasDistanceEvents {
                    // Yes, this is a gross O(n^2) hack, but adding Hashable support is a more invasive fix.
                    var uniqueOwners = [ImaginaryNode]()
                    for owner in node.boundaryOwners {
                        if uniqueOwners.first(where: { owner === $0 }) == nil {
                            uniqueOwners.append(owner)
                        }
                    }
                    
                    var actions = [ARBoundaryAccessibilityCustomAction]()
                    for owner in uniqueOwners {
                        let boundariesWithBackingNode = owner.boundaries.filter { $0.object === backingNode! }
                        for boundary in boundariesWithBackingNode {
                            let betweenFragment = owner.distanceTriggerFragments.between
                            let andFragment = boundary.object.distanceTriggerFragments.and
                            let title = String(format:NSLocalizedString("Trigger distance event %@ %@.", comment: "AX action title for triggering a distance event. Placeholders are string fragments with preposition and conjunction for each object. (e.g., 'Trigger distance event <between camera> <and snail>')"), betweenFragment, andFragment)
                            let action = ARBoundaryAccessibilityCustomAction(name: title, 
                                                                             target: self, 
                                                                             selector: #selector(triggerDistanceEvent))
                            action.boundary = boundary
                            action.boundaryOwner = owner
                            
                            actions.append(action)
                        }
                    }
                    allActions.insert(contentsOf: actions, at: 0)
                }
            }
            return allActions
        }
    }

    @objc
    fileprivate func triggerDistanceEvent(action: ARBoundaryAccessibilityCustomAction) -> Bool {
        guard let boundaryOwner = action.boundaryOwner, let node = action.boundary?.object else { return false }
        controller?.testContact(projector: boundaryOwner, node: node)
        return true
    }
    
    @objc
    fileprivate func findModel() -> Bool {
        guard let node = backingNode, let axCon = controller else { return false }
        let distance = axCon.descriptionForDistance(axCon.nodeDistanceToCamera(from: node))
        let heading = axCon.nodeHeadingFromCamera(from: node)
        // No need to localize this string
        let location = String(format:"%@, %@.", heading, distance)
        postAXNote(location, queued: true)
        
        return true
    }
    
    @objc
    fileprivate func detailedDescription() -> Bool {
        guard let node = backingNode else { return false }
        postAXNote(node.detailedDescription, queued: true)
        return true
    }
}

// MARK: - SceneViewContainer
// The container itself will be accessible during the detectingPlanes and placingModels states. Once all models are placed, it'll transision into an accessibility container and will vend elements representing the nodes in the scene.
@objc(SceneViewContainer)
public class SceneViewContainer : UIView {
    public override var accessibilityLabel: String? {
        set {}
        get {
            return NSLocalizedString("Main scene.", comment: "AX label for the overall scene description")
        }
    }

    public override var accessibilityHint: String? {
        set {}
        get {
            return NSLocalizedString("Move the iPad around to find planes on which to place items.", comment: "AX hint for the overall scene")
        }
    }
    
    public override func accessibilityElementCount() -> Int {
        return _InternalScene.activeAXElements.count
    }
    
    public override func accessibilityElement(at index: Int) -> Any? {
        guard index < _InternalScene.activeAXElements.count else { return nil }
        return _InternalScene.activeAXElements[index]
    }
    
    public override func index(ofAccessibilityElement element: Any) -> Int {
        guard let axElement = element as? ARAccessibilityElement else { return NSNotFound }
        if let idx = _InternalScene.activeAXElements.firstIndex(of: axElement) {
            return idx
        }
        return NSNotFound
    }
    
    public override var accessibilityElements: [Any]? {
        set {}
        get {
            return _InternalScene.activeAXElements
        }
    }
}

private class ARBoundaryAccessibilityCustomAction: UIAccessibilityCustomAction {
    var boundary: ModelBoundaryDescription? = nil
    var boundaryOwner: ImaginaryNode? = nil
}
