//
//  Camera.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class Camera: _InternalCamera, ModelProtocol, ProjectsBoundary {
    public func pop() {
        // Do nothing
    }
    
    public func appearAnimation(xOffset: Float, yOffset: Float) {
        // Do nothing
    }
    
    public func disappearAnimation(xOffset: Float, yOffset: Float, completion: (() -> Void)?) {
        // Do nothing
    }
    
    public func remove() {
        // Do nothing
    }
    
    public func animate() {
        // Do nothing
    }

    public func disappear() {
        // Do nothing
    }
    
    public func appear() {
        // Do nothing
    }

    public func stop() {
        // Do nothing
    }
    
    public var location: Point {
        return Point(xyz: node.simdPosition)
    }
    
    public static var Identifier = "Camera"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public func setPosition(_ position: Point) {
        node.setPosition(position)
    }
    
    public func look(at object: ImaginaryNode) {
        // Do Nothing
    }

    public var voice: Voice {
        return node.voice
    }

    public func speak(text: String, withAccent: SpeechAccent?, rate: Float?, pitch: Float?, completion: (() -> Void)?) {
        node.speak(text: text, withAccent: withAccent, rate: rate, pitch: pitch, completion: completion)
    }
    
    public func play(_ sound: Sound, rate: Float, loops: Bool, completion: (() -> Void)?) {
        node.play(sound, loops: loops, completion: completion)
    }

    public func currentState() -> ModelState {
        return node.currentState()
    }

    public func setState(_ state: ModelState) {
        node.setState(state)
    }

    public var boundaries: [ModelBoundaryDescription] {
        get { return node.boundaries }
        set {
            // RDR: Needed hack, to satisfy ProjectsBoundary
            //      which expects a mutable boundaries property

            // Do Nothing
        }
    }
    public var boundaryOwners: [ImaginaryNode] = []

    /**
     Creates a distance event that triggers when the camera comes within a certain distance of the provided model.
     
     - Parameter object: The model with which the camera interacts.
     - Parameter isWithin: The distance at which the event triggers.
     - Parameter handler: The handler to run when the distance event triggers.
     
     - localizationKey: Camera.when(_:isWithin:do)
     */
    public func when(_ object: ImaginaryNode, isWithin: Float, do handler: @escaping (() -> Void)) {
        node.when(object, isWithin: isWithin, do: handler)

        // Add an emitter to show object has a distance event
        addDistanceEventDecoration(for: object)
    }

    public var hasDistanceEvents: Bool {
        return false
    }
}
