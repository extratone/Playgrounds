//
//  Model.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public typealias ImaginaryModel = ModelProtocol & Selectable & ProjectsBoundary & SupportsColoring & CanFleeProtocol & CanFollowProtocol & Accessibility

/**
 An AR model that has a specific set of abilities. An ImaginaryNode can be placed into the scene, follow another model, flee from model, respond to tap selection, appear or disappear, animate, play sounds, and speak.
 
 - localizationKey: ImaginaryNode
 */
public typealias ImaginaryNode = SCNNode & ImaginaryModel

/**
A 3D object that can be placed into the AR scene.
 
 ```
 let snail = Model.snail
 ```
 
 - localizationKey: Model
 */
public class Model {

    /**
     A model of an arrow.
     
     - localizationKey: Model.arrow
     */
    public static var arrow: ArrowModel {
        guard let arrow = ArrowModel() else {
            fatalError("DANGER: Can’t find the Arrow Collada file.")
        }
        return arrow
    }
    
    /**
     A model of a flying bug.
     
     - localizationKey: Model.flyingBug
     */
    public static var flyingBug: FlyingBugModel {
        guard let bf = FlyingBugModel() else {
            fatalError("DANGER: Can’t find the Butterfly Collada file.")
        }
        return bf
    }
    
    /**
     A model of a cactus.
     
     - localizationKey: Model.cactus
     */
    public static var cactus: CactusModel {
        guard let cactus = CactusModel() else {
            fatalError("DANGER: Can’t find the Cactus Collada file.")
        }
        return cactus
    }
    
    /**
     A model of an alarm clock.
     
     - localizationKey: Model.alarmClock
     */
    public static var alarmClock: ClockModel {
        guard let clock = ClockModel() else {
            fatalError("DANGER: Can’t find the Clock Collada file.")
        }
        return clock
    }
    
    /**
     A model of an ear.
     
     - localizationKey: Model.ear
     */
    public static var ear: EarModel {
        guard let ear = EarModel() else {
            fatalError("DANGER: Can’t find the Ear Collada file.")
        }
        return ear
    }
    
    /**
     A model of an eye.
     
     - localizationKey: Model.eye
     */
    public static var eye: EyeModel {
        guard let eye = EyeModel() else {
            fatalError("DANGER: Can’t find the Eye Collada file.")
        }
        return eye
    }
    
    /**
     A model of a foot.
     
     - localizationKey: Model.foot
     */
    public static var foot: FootModel {
        guard let foot = FootModel() else {
            fatalError("DANGER: Can’t find the Foot Collada file.")
        }
        return foot
    }

    /**
     A model of a hand.
     
     - localizationKey: Model.hand
     */
    public static var hand: HandModel {
        guard let hand = HandModel() else {
            fatalError("DANGER: Can’t find the Hand Collada file.")
        }
        return hand
    }
    
    /**
     A model of a lemon.
     
     - localizationKey: Model.lemon
     */
    public static var lemon: LemonModel {
        guard let lemon = LemonModel() else {
            fatalError("DANGER: Can’t find the Lemon Collada file.")
        }
        return lemon
    }
    
    /**
     A model of a mouth.
     
     - localizationKey: Model.mouth
     */
    public static var mouth: MouthModel {
        guard let mouth = MouthModel() else {
            fatalError("DANGER: Can’t find the Mouth Collada file.")
        }
        return mouth
    }
    
    /**
     A model of a nose.
     
     - localizationKey: Model.nose
     */
    public static var nose: NoseModel {
        guard let nose = NoseModel() else {
            fatalError("DANGER: Can’t find the Nose Collada file.")
        }
        return nose
    }
    
    /**
     A model of a snail.
     
     - localizationKey: Model.snail
     */
    public static var snail: SnailModel {
        guard let snail = SnailModel() else {
            fatalError("DANGER: Can’t find the Snail Collada file.")
        }
        return snail
    }
    
    /**
     A model of a sun.
     
     - localizationKey: Model.sun
     */
    public static var sun: SunModel {
        guard let sun = SunModel() else {
            fatalError("DANGER: Can’t find the Sun Collada file.")
        }
        return sun
    }
    
    /**
     A model of a traffic cone.
     
     - localizationKey: Model.trafficCone
     */
    public static var trafficCone: ConeModel {
        guard let cone = ConeModel() else {
            fatalError("DANGER: Can’t find the Cone Collada file.")
        }
        return cone
    }
    
    /**
     A model of a lightbulb.
     
     - localizationKey: Model.lightbulb
     */
    public static var lightbulb: LightBulbModel {
        guard let lb = LightBulbModel() else {
            fatalError("DANGER: Can’t find the LightBulb Collada file.")
        }
        return lb
    }

    /**
     A model of a lightning bolt.
     
     - localizationKey: Model.lightning
     */
    public static var lightning: LightningBoltModel {
        guard let lbm = LightningBoltModel() else {
            fatalError("DANGER: Can’t find the Lightning Bolt Collada file.")
        }
        return lbm
    }
    
    /**
     A model of a moon.
     
     - localizationKey: Model.moon
     */
    public static var moon: MoonModel {
        guard let moon = MoonModel() else {
            fatalError("DANGER: Can’t find the Moon Collada file.")
        }
        return moon
    }
    
    /**
     A model of a rain cloud.
     
     - localizationKey: Model.raincloud
     */
    public static var raincloud: RainCloudModel {
        guard let rc = RainCloudModel() else {
            fatalError("DANGER: Can’t find the Rain Cloud Collada file.")
        }
        return rc
    }

    /**
     A model of a star.
     
     - localizationKey: Model.star
     */
    public static var star: StarModel {
        guard let star = StarModel() else {
            fatalError("DANGER: Can’t find the Star Collada file.")
        }
        return star
    }


    // MARK: - Shapes & Text

    /**
     A model of a sphere.
     
     - Parameter size: The diameter of the sphere.
     
     - localizationKey: Model.sphere
     */
    public static func sphere(size: Float) -> SphereModel {
        return SphereModel(diameter: CGFloat(size))
    }

    /**
     A model of a cube.
     
     - Parameter size: The length of one side of the cube.
     
     - localizationKey: Model.cube
     */
    public static func cube(size: Float) -> BoxModel {
        return BoxModel(width: CGFloat(size), height: CGFloat(size), depth: CGFloat(size))
    }

    /**
     A model of a box.
     
     - Parameter width: The width of the box.
     - Parameter height: The height of the box.
     - Parameter depth: The depth of the box.
     
     - localizationKey: Model.box
     */
    public static func box(width: Float, height: Float, depth: Float) -> BoxModel {
        return BoxModel(width: CGFloat(width), height: CGFloat(height), depth: CGFloat(depth))
    }

    /**
     A model of a plane.
     
     - Parameter width: The width of the plane.
     - Parameter length: The length of the plane.
     
     - localizationKey: Model.plane
     */
    public static func plane(width: Float, length: Float) -> PlaneModel {
        return PlaneModel(width: CGFloat(width), length: CGFloat(length))
    }

    /**
     A model of a pyramid.
     
     - Parameter width: The width of the base of the pyramid.
     - Parameter height: The height from the base to the top of the pyramid.
     - Parameter depth: The depth of the base of the pyramid.
     
     - localizationKey: Model.pyramid
     */
    public static func pyramid(width: Float, height: Float, depth: Float) -> PyramidModel {
        return PyramidModel(width: CGFloat(width), height: CGFloat(height), depth: CGFloat(depth))
    }

    /**
     A model of a cylinder.
     
     - Parameter size: The diameter of the cylinder.
     - Parameter height: The height from the base to the top of the cylinder.
     
     - localizationKey: Model.cylinder
     */
    public static func cylinder(size: Float, height: Float) -> CylinderModel {
        return CylinderModel(diameter: CGFloat(size), height: CGFloat(height))
    }

    /**
     A model of a torus.
     
     - Parameter size: The diameter of the torus
     - Parameter thickness: The width from the inside edge to the outside edge of the torus.
     
     - localizationKey: Model.torus
     */
    public static func torus(size: Float, thickness: Float) -> TorusModel {
        return TorusModel(diameter: CGFloat(size), thickness: CGFloat(thickness))
    }

    /**
     A model of text.
     
     - Parameter text: The text to be represented in the model.
     - Parameter elevation: The height at which the text displays.
     
     - localizationKey: Model.text
     */
    public static func text(_ text: String, elevation: Float) -> TextModel {
        return TextModel(string: text, planeOffset: elevation)
    }


    public static func exisitingModelContainingNode(_ node: SCNNode) -> ImaginaryNode? {
        if let modelRoot = node as? ImaginaryNode {
            return modelRoot
        }

        guard let parent = node.parent else { return nil }

        // Recurse up to check if the parent is a `ModelProtocol`.
        return exisitingModelContainingNode(parent)
    }

    public static func shouldFly(_ node: ImaginaryNode) -> Bool {
        guard node is CactusModel || node is ClockModel || node is FootModel ||
              node is HandModel || node is SnailModel || node is ConeModel else {
            return true
        }

        return false
    }

}
