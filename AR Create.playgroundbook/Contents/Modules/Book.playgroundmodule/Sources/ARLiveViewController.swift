//
//  ARLiveViewController.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import UIKit
import ARKit
import PlaygroundSupport
import Dispatch

@objc(ARLiveViewController)
public class ARLiveViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate, SceneTransitionDelegate {


    public static func makeFromStoryboard(showDebug: Bool) -> ARLiveViewController {
        let bundle = Bundle(for: ARLiveViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        let vc = storyboard.instantiateInitialViewController() as! ARLiveViewController
        vc.showDebugInfo = showDebug
        return vc
    }

    // When you tell an SCNView that it's an accessible element, there's some automatic functionality that gets enabled.
    // We don't want that, so we put the scene in a container and make that view the accessible element.
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var sceneViewContainer: SceneViewContainer!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var funkyStartButton: UIButton!
    @IBOutlet weak var funkyStartScreen: UIView!

    var sceneStartControl: OverlayView?
    var resetSceneControl: OverlayView?

    var funkyStart: FunkyStartController?
    var funkySoundEmitter: AVAudioPlayer?   //: InvisibleModel = InvisibleModel()
    var showFunkyStart: Bool = false
    
    public var scene: Scene? {
        didSet {
            scene?.connect(to: sceneView)
            scene?.transitionDelegate = self
            sceneView.scene.rootNode.addChildNode(Scene.featurePoints)
            setupLighting()
        }
    }
    var blurView: UIVisualEffectView?

    var initialStates = [SCNNode : ModelState]()

    var anchorPositionCache = [ UUID : float4x4 ]()

    var showDebugInfo = false
    
    // Accessibility items
    var axUpdateTimer: Timer? = nil
    var axSceneLastUpdated: Date? = nil
    var voObserver: NSObjectProtocol? = nil
    var scObserver: NSObjectProtocol? = nil
    var effectsPlayer: AVPlayer? = nil
    var itemsAdded: [ImaginaryNode]? = nil
    var trackedItems: [ImaginaryNode]? = nil
    var placingModelsFirstTime: Bool = true
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        sceneView.audioEnvironmentNode.renderingAlgorithm = .soundField
        let dap = sceneView.audioEnvironmentNode.distanceAttenuationParameters
        dap.maximumDistance = 2.5
        dap.referenceDistance = 0.3
        dap.distanceAttenuationModel = .linear

        sceneView.accessibilityIgnoresInvertColors = true

        informationLabel.layer.shadowColor = UIColor.black.cgColor
        informationLabel.layer.shadowRadius = 12.0
        informationLabel.layer.shadowOpacity = 1.0
        informationLabel.layer.shadowOffset = CGSize.zero
        
        // Disabled the label, we manually announce what it says
        informationLabel.isAccessibilityElement = false
        sceneViewContainer.isAccessibilityElement = true
        sceneView.isAccessibilityElement = false

        setupSceneControlButtons()
        
        funkyStartButton.accessibilityLabel = NSLocalizedString("Launch start screen", comment: "AX button that starts a 'funky' start screen")
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        guard ARWorldTrackingConfiguration.isSupported else {
            displayUnsupportedMessage()
            return
        }

        sceneView.session.delegate = self
        sceneView.autoenablesDefaultLighting = false

        // iOS 12.0 required for automatic lighting
        sceneView.automaticallyUpdatesLighting = false

        let config = ARWorldTrackingConfiguration()
        config.planeDetection = [.horizontal]
        config.worldAlignment = .gravity

        // iOS 12.0 required for automatic lighting
//        config.environmentTexturing = .automatic

        sceneView.session.run(config)

        if showDebugInfo {
            sceneView.showsStatistics = false
            sceneView.debugOptions = []
        }
        
        voObserver = NotificationCenter.default.addObserver(forName: UIAccessibility.voiceOverStatusDidChangeNotification,
                                                            object: nil,
                                                            queue: OperationQueue.main) { _ in 
            self.toggleAXUpdateTimer()

            // Need to reset the scene back to placingModels so all the accessibility element tracking logic triggers correctly. This is an edge case as most VoiceOver users will have VO enabled all the time.
            if let localScene = self.scene, localScene.state == .preparingToStart || localScene.state == .started {
                localScene.state = .placingModels
            }
        }
        
        scObserver = NotificationCenter.default.addObserver(forName: UIAccessibility.switchControlStatusDidChangeNotification,
                                                            object: nil,
                                                            queue: OperationQueue.main) { _ in 
            self.toggleAXUpdateTimer()

            // Need to reset the scene back to placingModels so all the accessibility element tracking logic triggers correctly. This is an edge case as most Switch Control users will have SC enabled all the time.
            if let localScene = self.scene, localScene.state == .preparingToStart || localScene.state == .started {
                localScene.state = .placingModels
            }
        }

        if isAnyAXRunning() {
            toggleAXUpdateTimer()

            if UIAccessibility.isVoiceOverRunning {
                workaround_preventDictationAlert()
            }
        }
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        sceneView.session.pause()
    }

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == FunkyStartController.SegueIdentifier,
           let vc = segue.destination as? FunkyStartController {
            vc.onStartHandler = {
                self.scene?.play(.buttonClick)
                self.funkySoundEmitter?.setVolume(0.0, fadeDuration: 1.0)

                self.startPlaying()

                self.funkyStartScreen.alpha = 1.0
                UIView.animate(withDuration: 1.0, animations: {
                    self.funkyStartScreen.alpha = 0.0
                }, completion: { _ in
                    self.funkyStartScreen.isHidden = true
                })
            }
            funkyStart = vc
        }
    }

    func setupLighting() {
        let light = SCNLight()
        light.type = .directional
        light.intensity = 1000.0
        light.castsShadow = true
        light.shadowMode = .deferred
        light.maximumShadowDistance = 2.0
        light.zFar = 2.0
        light.zNear = CGFloat(9.cm)
        light.orthographicScale = 2.0
        light.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        light.shadowSampleCount = 2
        scene?.camera.node.light = light
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - ARSCNViewDelegate

    public func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let localScene = scene else { return }

        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        self.anchorPositionCache[planeAnchor.identifier] = planeAnchor.transform
        let plane = Plane(anchor: planeAnchor, in: self.sceneView, parent: localScene)
        Scene.planes[planeAnchor] = plane
        node.addChildNode(plane)
        localScene.onPlaneDetected(plane)
        if localScene.state == .detectingPlanes {
            // Switch to Placement UI after the first
            // plane is detected
            localScene.state = .placingModels

            sceneView.session.setWorldOrigin(relativeTransform: plane.simdWorldTransform)
        }
    }

    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        let lastMeasurement = self.anchorPositionCache[planeAnchor.identifier] ?? float4x4(0.0)
        let offset = lastMeasurement.columns.3.y - planeAnchor.transform.columns.3.y

        self.anchorPositionCache[planeAnchor.identifier] = planeAnchor.transform
        if let plane = node.childNodes.first as? Plane {
            if let meshGeometry = plane.meshNode.geometry as? ARSCNPlaneGeometry {
                meshGeometry.update(from: planeAnchor.geometry)
            }

            if let extentGeometry = plane.planeNode.geometry as? SCNPlane {
                extentGeometry.width = CGFloat(planeAnchor.extent.x)
                extentGeometry.height = CGFloat(planeAnchor.extent.z)
                plane.planeNode.simdPosition = planeAnchor.center
                plane.planeNode.physicsBody?.resetTransform()
            }
        }

        for model in Scene.models {
            model.adjust(onto: planeAnchor, using: offset)
        }

        for (node, point) in self.initialStates {
            var adjusted = point
            adjusted.transform.columns.3.y -= offset
            self.initialStates[node] = adjusted
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - ARSessionDelegate
    public func session(_ session: ARSession, didUpdate frame: ARFrame) {
        guard let localScene = scene else { return }

        // 0. Update our camera node wrapper
        localScene.camera.node.simdTransform = frame.camera.transform

        if let fp = frame.rawFeaturePoints /*, localScene.state == .detectingPlanes*/ {
            Scene.featurePoints.update(for: fp)
        }

        // 1. Update placement preview of the item currently picked up
        guard localScene.state == .placingModels else { return }

        let centerScreen = CGPoint(x: sceneView.frame.midX, y: sceneView.frame.midY)
        guard let result = detectedPlane(at: centerScreen) else {
            updatePlacementInterface(planeFound: false)
            return
        }

        // a. see if it's a plane we're tracking
        guard result.anchor is ARPlaneAnchor else { return }

        // b. capture point, for placement
        reticulePosition = Point(xyz: result.worldTransform.translation)

        // c. update placement UI
        updatePlacementInterface(planeFound: true)
    }

    public func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        DispatchQueue.main.async {
            switch camera.trackingState {
            case .limited(.excessiveMotion):
                self.informationLabel.text = NSLocalizedString("Try slowing down your movement, or run your code again.", comment: "Slow down movement string")
                postAXNote(self.informationLabel.text!)

                if self.informationLabel.isHidden == true {
                    self.informationLabel.controlBuildIn()
                }

            case .limited(.insufficientFeatures):
                self.informationLabel.text = NSLocalizedString("Move the iPad back and forth, while pointing towards a flat surface", comment: "Flat surface string")
                postAXNote(self.informationLabel.text!)

                if self.informationLabel.isHidden == true {
                    self.informationLabel.controlBuildIn()
                }

            case .limited(.initializing):
                break

            case .notAvailable:
                self.informationLabel.text = NSLocalizedString("Camera is not availabe. Please run your code again, and ensure Playgrounds is authorized to use the camera.", comment: "Camera is not available text")
                postAXNote(self.informationLabel.text!)

                if self.informationLabel.isHidden == true {
                    self.informationLabel.controlBuildIn()
                }

            case .limited(.relocalizing):
                self.informationLabel.text = NSLocalizedString("Resuming world tracking…", comment: "Restarted tracking text")
                postAXNote(self.informationLabel.text!)
                
                if self.informationLabel.isHidden == true {
                    self.informationLabel.controlBuildIn()
                }

            default:
                self.informationLabel.controlBuildOut()
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - SceneTransitionDelegate

    public func didSet(optionalStartMessage message: String) {
        showFunkyStart = true
        funkyStart?.message.text = message
    }

    public func didTransition(to state: SceneState, from oldState: SceneState) {
        switch state {
        case .placingModels:
            DispatchQueue.main.async {
                self.funkyStartButton?.isHidden = true
                if UIAccessibility.isVoiceOverRunning {
                    let labelText = NSLocalizedString("Two-finger double-tap anywhere to place.", comment: "AX placement gesture instructions")
                    self.informationLabel.text = labelText
                }
                else {
                    self.informationLabel.text = NSLocalizedString("Tap anywhere to place.", comment: "Placement gesture instructions")
                }
                self.informationLabel.controlBuildIn()


                if oldState == .detectingPlanes {
                    self.resetSceneControl?.isHidden = true
                    self.sceneStartControl?.isHidden = true

                    if !UIAccessibility.isVoiceOverRunning {
                        self.installTapGesture(true)
                    }

                    self.placingModelsFirstTime = true
                }

                else {
                    // re-placing a model
                    self.sceneStartControl?.controlBuildOut()
                    self.resetSceneControl?.isHidden = true
                    
                    if isAnyAXRunning() {
                        self.sceneViewContainer.isAccessibilityElement = true
                        // reset items, we rebuild this list when we move into prepareToStart
                        self.trackedItems = nil
                        self.scene?.removeAllAccessibleElements()
                    }
                }

                self.selectNextAsset()
            }

        case .preparingToStart:
            selectedAsset = nil

            DispatchQueue.main.async {
                if oldState == .started {
                    self.resetSceneControl?.controlBuildOut()
                    
                    if isAnyAXRunning() {
                        // Must clear out all elements, we might have added new ones during execution that are not valid at initial preparingToStart
                        self.scene?.removeAllAccessibleElements()
                        
                        let note = NSLocalizedString("Scene reset to initial setup.", comment: "AX description of what happens when the scene has been reset.")
                        postAXNote(note, queued: true)
                    }

                }
                else {
                    self.resetSceneControl?.isHidden = true
                }

                self.sceneStartControl?.controlBuildIn()

                if self.showFunkyStart {
                    self.funkyStartButton.funkyBuildIn()
                }

                if isAnyAXRunning() {
                    // Must disable the sceneViewContainer here, otherwise it'll get in the way of finding elements.
                    self.sceneViewContainer.isAccessibilityElement = false
                    
                    if let placedItems = self.scene?.accessibleItemsInScene() {
                        for item in placedItems {
                            self.scene?.addAccessibleElement(for: item, controller: self)
                        }
                        self.trackedItems = placedItems
                    }
                }
                
                if UIAccessibility.isVoiceOverRunning {
                    let labelText = NSLocalizedString("Tap the \"start scene\" button to run the scene. Two-finger double-tap any object to reposition it.", comment: "Instructions for how to re-place objects")
                    self.informationLabel.text = labelText 
                    postAXNote(labelText, queued: true)
                    
                    // Refocus VO on the button so that it can get unfocused off sceneViewContainer. If it remains, swiping left/right won't find any of the elements tracking the AR models.
                    if let startButton = self.sceneStartControl?.subviews.last as? UIButton {
                        UIAccessibility.post(notification: .layoutChanged, argument: startButton)
                    }
                    
                    // Clear distance announcement variables
                    self.lastDistanceAnnouncedDate = nil
                    self.lastPlacedNode = nil
                }
                else {
                    self.informationLabel.text = NSLocalizedString("Tap any object to reposition it.", comment: "Instructions for how to re-place objects")
                }
                
                self.informationLabel.controlBuildIn(with: 0.8)
            }


        case .started:
            DispatchQueue.main.async {
                self.sceneStartControl?.controlBuildOut()
                self.informationLabel.isHidden = true
                self.resetSceneControl?.controlBuildIn()

                if self.showFunkyStart {
                    self.funkyStartButton?.funkyBuildOut(with: 0.8)
                }
                else {
                    self.funkyStartButton?.isHidden = true
                }
            }

            scene?.showPlanes = false
            scene?.showFeaturePoints = false

            Scene.models.forEach { model in
                initialStates[model] = model.currentState()
            }

            sceneView.scene.physicsWorld.contactDelegate = self

            scene?.onStart()
            AssessmentManager.callAssessment()
            
        default:
            return
        }
    }
    
    private var sceneViewTapGesture: UITapGestureRecognizer? = nil
    func installTapGesture(_ install: Bool) {
        if install {
            guard sceneViewTapGesture == nil else { return } 
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
            sceneView.addGestureRecognizer(gesture)
            sceneViewTapGesture = gesture
        }
        else {
            if let existingTapGesture = sceneViewTapGesture {
                sceneView.removeGestureRecognizer(existingTapGesture)
            }
        }
    }
    
    @IBAction func showFunkyStart(_ sender: Any) {
        sceneStartControl?.isHidden = true
        funkyStartButton?.isHidden = true
        informationLabel.isHidden = true

        funkyStartScreen.alpha = 0.0
        funkyStartScreen.isHidden = false

        do {
            guard let url = Bundle.main.url(forResource: Sound.music.rawValue, withExtension: "m4a") else {
                fatalError("Failed to find Music Resource at: '\(Sound.music.rawValue).m4a'.")
            }
            funkySoundEmitter = try AVAudioPlayer(contentsOf: url)
            funkySoundEmitter?.numberOfLoops = -1
            funkySoundEmitter?.volume = 0.0
        } catch {
            print("Problems playing the intro music file")
        }

        funkySoundEmitter?.play()
        funkySoundEmitter?.setVolume(0.7, fadeDuration: 1.0)
        UIView.animate(withDuration: 1.0, animations: { 
            self.funkyStartScreen.alpha = 1.0
        }) { (didComplete) in
            if UIAccessibility.isVoiceOverRunning {
                UIAccessibility.post(notification: .layoutChanged, argument: self.funkyStart?.message)
            }
        }
    }

    @objc
    public func startPlaying() {
        guard let localScene = scene else { return }
        localScene.state = .started
    }

    @objc
    public func resetScene() {
        guard let localScene = scene else { return }

        let removedAfterStart = initialStates.keys.compactMap {  node in
            return Scene.models.contains(where: { return $0 == node }) ? nil : node
        }
        removedAfterStart.forEach { node in
            guard let model = node as? ImaginaryNode else { return }
            sceneView.scene.rootNode.addChildNode(model)
            Scene.models.append(model)
            model.appear()
        }

        Scene.models.forEach { model in
            model.reset(includeActions: true)

            guard let state = initialStates[model] else {
                // if it was added after onStart, remove it
                model.remove()
                return
            }

            model.setState(state)
        }

        // reset clears distance event decorations
        localScene.camera.boundaries.forEach { boundary in
            localScene.camera.addDistanceEventDecoration(for: boundary.object)
        }

        sceneView.scene.physicsWorld.contactDelegate = nil

        localScene.showPlanes = true
        localScene.showFeaturePoints = true

        localScene.state = .preparingToStart
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Object Selection

    @objc
    func didTap(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: sceneView)
        _performTap(at: location)
    }
    
    func _performTap(at location:CGPoint) {
        guard let localScene = scene else { return }
        
        switch localScene.state {
        case .detectingPlanes:
            return

        case .placingModels:
            placeAsset(self)
            return

        case .preparingToStart:
            let focusedElement = UIAccessibility.focusedElement(using: nil) as? ARAccessibilityElement
            let model = detectedModel(at: location)
            
            if model != nil || focusedElement != nil {
                guard let tgtModel = model ?? focusedElement?.backingNode else { return }
                
                localScene.add(tgtModel)
                localScene.state = .placingModels
                
                if UIAccessibility.isVoiceOverRunning {
                    let note = String(format: NSLocalizedString("Repositioning: %@", comment: "AX announcement of user picking up a model and repositioning it. Placeholder will be a titleDescription of the object (e.g., snail, traffic cone)"), tgtModel.titleDescription)

                    postAXNote(note, queued: true)
                }
            }
            return

        case .started:
            // 1. Check if a model was selected
            if let model = detectedModel(at: location)  {
                model.onSelected()
                return
            }

            // 2. Determine if there's a plane in the location
            if let result = detectedPlane(at: location) {

                // a. see if it's a plane we're tracking
                guard let anchor = result.anchor as? ARPlaneAnchor,
                    let plane = Scene.planes[anchor]
                    else { return }

                // b. call the plane's onSelected handler
                plane.onSelected()
                return
            }
        }
    }

    func detectedPlane(at point: CGPoint) -> ARHitTestResult? {
        let results = sceneView.hitTest(point, types: [.existingPlaneUsingGeometry])
        return results.first { result in
            return result.type == .existingPlaneUsingGeometry && result.anchor is ARPlaneAnchor
        }
    }

    func detectedModel(at point: CGPoint) -> ImaginaryNode? {
        let hitTestResults = sceneView.hitTest(point, options: [.boundingBoxOnly: true])

        return hitTestResults.lazy.compactMap { result in
            guard let model = Model.exisitingModelContainingNode(result.node) else { return nil }
            if model is _InternalCameraNode {
                // Can never select the camera node
                return nil
            }
            return model
        }.first
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Asset Placement

    var selectedAsset: QueuedModelDescription? = nil
    var reticulePosition: Point? = nil
    var placementPuck = PlacementDecoration()

    let ModelPlacementOffset = 10.mm

    @IBAction func placeAsset(_ sender: Any) {
        guard let model = selectedAsset?.model else { return }

        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.5

        model.opacity = 1.0
        model.simdPosition.y -= ModelPlacementOffset
        model.physicsBody?.resetTransform()


        // remove billboard constraint
        model.constraints = []
        selectedAsset = nil
        SCNTransaction.commit()

        placementPuck.placeAnimation {
            self.selectNextAsset()
        }
        
        if UIAccessibility.isVoiceOverRunning {
            playSoundEffect(.click)
        }
    }

    private var totalQueuedObjectCount = 0
    func selectNextAsset() {
        guard let asset = scene?.dequeueModel() else {
            // queue empty
            placementPuck.presentation.simdScale = float3(1.0)
            scene?.state = .preparingToStart
            return
        }
        
        if UIAccessibility.isVoiceOverRunning {

            guard let localScene = scene else { return }
            let model = asset.model
            
            var preamble: String? = nil
            if placingModelsFirstTime {
                placingModelsFirstTime = false
                totalQueuedObjectCount = localScene.queuedObjects.count + 1
                var firstTimeAnnoucement = ""
                if totalQueuedObjectCount > 1 {
                    firstTimeAnnoucement = String(format:NSLocalizedString("There are %d items to place.", comment: "AX announcement for first time placement begins"), totalQueuedObjectCount)
                }
                else {
                    firstTimeAnnoucement = NSLocalizedString("There is one item to place.", comment: "AX announcement for first time placement begins")
                }
                let targetDescription = NSLocalizedString("Move the iPad around to move the model in the AR scene. Two-finger double-tap anywhere to place the model. ", comment: "AX description of the placement recticule")
                
                preamble = firstTimeAnnoucement + targetDescription
            }
            
            let objectIndex = totalQueuedObjectCount - localScene.queuedObjects.count
            let objectDesc = String(format: NSLocalizedString("Placing item %d of %d: %@", comment: "AX description of the item being placed and were it is in the queue. e.g. placing item 3 of 5: cool colored lemon"), objectIndex, totalQueuedObjectCount, model.shortDescription)
            
            if let pre = preamble {
                // Separate the announcement of the preamble so if the user silences it, they still get the "placing # of n" information.
                postAXNote(pre, queued: true)
            }
            
            postAXNote(objectDesc, queued: true)
        }

        // make sure it always is facing the camera
        let billboard = SCNBillboardConstraint()
        billboard.freeAxes = .Y
        asset.model.constraints = [billboard]

        placementPuck.pickupAnimation()
        selectedAsset = asset
    }

    var lastDistanceAnnouncedDate:Date? = nil
    var modelMovedOffPlaneTimer:Timer? = nil
    var lastPlacedNode:QueuedModelDescription? = nil
    var announcementThreshold:TimeInterval = 10.0
    func updatePlacementInterface(planeFound: Bool) {
        guard let model = selectedAsset?.model else { return }

        guard planeFound else {
            if UIAccessibility.isVoiceOverRunning, model.opacity > 0.1 {
                playSoundEffect(.rhythm)
                modelMovedOffPlaneTimer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { timer in
                    let desc = NSLocalizedString("Model no longer on plane, not currently placeable.", comment: "AX description when the model is not over a plane and is not placeable.")
                    postAXNote(desc, queued: true)
                    timer.invalidate()
                    self.modelMovedOffPlaneTimer = nil
                }
            }

            // if no plane is found, make it invisible
            model.opacity = 0.0
            placementPuck.opacity = 0.0

            return
        }

        if UIAccessibility.isVoiceOverRunning, model.opacity < 0.1 {
            playSoundEffect(.toeWiggleSingleReverse)
            
            if let timer = modelMovedOffPlaneTimer {
                timer.invalidate()
                modelMovedOffPlaneTimer = nil
            }
        }
        
        model.opacity = 0.8
        placementPuck.opacity = 1.0

        if placementPuck.parent == nil {
            sceneView.scene.rootNode.addChildNode(placementPuck)
        }

        if model.parent == nil {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.0
            scene?.placeInQueue(model, at: reticulePosition!)
            SCNTransaction.commit()
            return
        }

        var modelPosition = reticulePosition!
        modelPosition.y += ModelPlacementOffset
        model.setPosition(modelPosition)

        var puckPosition = reticulePosition!
        puckPosition.y += 3.mm
        placementPuck.simdPosition = puckPosition.simdPoint

        announcePlacementDistance()
    }

}
