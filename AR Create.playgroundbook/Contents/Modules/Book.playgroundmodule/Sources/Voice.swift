//
//  Voice.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//
import Foundation
import AVFoundation



public class Voice: NSObject, AVSpeechSynthesizerDelegate {
    public var onCompletion: (() -> Void)?
    
    let voiceSynthesizer = AVSpeechSynthesizer()
    
    public func speak(_ utterance: AVSpeechUtterance) {
        voiceSynthesizer.speak(utterance)
    }
    
    // TODO: For some reason, this isn't being called when the AVSpeechSynthesizer finishes an utterance
    public func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        DispatchQueue.main.async {
            self.onCompletion?()
        }
    }
    
    public override init() {
        super.init()
        voiceSynthesizer.delegate = self
    }
}


