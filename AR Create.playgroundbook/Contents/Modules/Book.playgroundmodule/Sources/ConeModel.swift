//
//  ConeModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class ConeModel : _InternalModel, ImaginaryModel {
    public static var Identifier = "Cone"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .black, .limePink]

        case .basic:
            return [.pink, .yellow, .lime]

        case .wacky:
            return [.bluePink, .pinkAqua, .lime]
            
        case .cool:
            return [.blue, .whiteAqua, .lime]
            
        case .moody:
            return [.black, .white, .white]
            
        case .hot:
            return [.yellowRed, .yellow, .yellowWhite]

        case .custom:
            return [Palette.Second, Palette.Third, Palette.First]
        }
    }
    
    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public func animate() {
        pop()
    }
    
    public init?() {
        super.init(named: ConeModel.Identifier)
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Traffic cone", comment: "AX title for the traffic cone model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored traffic cone with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored traffic cone.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored traffic cone with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored traffic cone.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored traffic cone with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored traffic cone.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored traffic cone with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored traffic cone.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored traffic cone with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored traffic cone.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored traffic cone with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored traffic cone.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored traffic cone with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored traffic cone.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A pink traffic cone with lime colored stripes around the center.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black traffic cone with white colored stripes around the center.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A yellow traffic cone with black and pink stripes around the center.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A blue traffic cone with aqua blue and lime colored stripes around the center.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange traffic cone with yellow stripes around the center.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink traffic cone with lime colored stripes around the center.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored traffic cone with stripes around the center.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate traffic cone.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of traffic cone.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between traffic cone", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and traffic cone", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
