//
//  CactusModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit
import ARKit

public final class CactusModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Cactus"
    
    public var emitterOffset : (Float, Float) = (0, 0)
    
    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .lime, .black]

        case .basic:
            return [.lime, .pink, .black]

        case .wacky:
            return [.pink, .blue]
            
        case .cool:
            return [.whiteAqua, .blue, .black]
            
        case .moody:
            return [.blackBlack, .white, .black]
            
        case .hot:
            return [.yellow, .yellowRed, .black]

        case .custom:
            return [Palette.First, Palette.Second, Palette.Third]
        }
    }
    
    public enum Animation : String, CustomAnimation {
        case spines = "Spikes"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 1.1
        }
    }
    
    public func animate() {
        pop()
    }

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }

    public init?() {
        super.init(named: CactusModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)

        // <rdar://problem/48716761>
        pivot = SCNMatrix4MakeRotation(Float.pi / 2.0, 0, 1.0, 0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Cactus", comment: "AX title for the cactus model")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored cactus with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored cactus.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored cactus with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored cactus.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored cactus with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored cactus.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored cactus with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored cactus.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored cactus with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored cactus.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored cactus with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored cactus.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored cactus with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored cactus.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A lime colored potted cactus with sharp pink spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black colored potted cactus with sharp white spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A pink colored potted cactus with sharp yellow spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An aqua blue colored potted cactus with sharp blue spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A yellow colored potted cactus with sharp orange spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink colored potted cactus with sharp blue spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored potted cactus with sharp spikes protruding from its surface. Don’t touch them!", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate cactus.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of cactus.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between cactus", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and cactus", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
