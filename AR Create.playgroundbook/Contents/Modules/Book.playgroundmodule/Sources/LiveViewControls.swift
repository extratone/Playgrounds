//
//  LiveViewControls.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import UIKit
import ARKit

struct ControlLayout {
    static let verticalOffset: CGFloat = 18
    static let height: CGFloat = 44
    static let width: CGFloat = 44
    static let edgeOffset: CGFloat = 20
}

// Magic color
public extension UIColor {
    static let AppTintColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}

class OverlayView: UIView {
    init() {
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.layer.cornerRadius = 22
        blurEffectView.clipsToBounds = true
        blurEffectView.translatesAutoresizingMaskIntoConstraints = true
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false

        addSubview(blurEffectView)
        blurEffectView.frame = bounds

        let whiteOverBlurView = UIView()
        whiteOverBlurView.translatesAutoresizingMaskIntoConstraints = true
        whiteOverBlurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(whiteOverBlurView)
        whiteOverBlurView.frame = bounds
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public extension ARLiveViewController {
    func setupSceneControlButtons() {
        let startControl = OverlayView()
        startControl.isHidden = true
        view.addSubview(startControl)

        NSLayoutConstraint.activate([
            startControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: ControlLayout.verticalOffset),
            startControl.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            startControl.heightAnchor.constraint(equalToConstant: ControlLayout.height),
            startControl.widthAnchor.constraint(equalToConstant: 175.0)
        ])

        let startButton = UIButton()
        startButton.translatesAutoresizingMaskIntoConstraints = true
        startButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let startTitle = NSLocalizedString("Start Scene", comment: "Start Scene button title")
        startButton.setTitle(startTitle, for: .normal)
        startButton.setTitleColor(UIColor.AppTintColor, for: .normal)
        startButton.addTarget(self, action: #selector(startPlaying), for: .touchUpInside)
        startButton.accessibilityHint = NSLocalizedString("Begin execution of the scene once setup has been finished.", comment: "AX hint for the 'start scene' button")
        startControl.addSubview(startButton)
        
        sceneStartControl = startControl

        let resetControl = OverlayView()
        resetControl.isHidden = true
        view.addSubview(resetControl)

        NSLayoutConstraint.activate([
            resetControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: ControlLayout.verticalOffset),
            resetControl.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: ControlLayout.edgeOffset),
            resetControl.heightAnchor.constraint(equalToConstant: ControlLayout.height),
            resetControl.widthAnchor.constraint(equalToConstant: ControlLayout.width)
        ])

        let resetButton = UIButton()
        let image = UIImage(named: "grid-icon")
        resetButton.setImage(image, for: .normal)
        resetButton.imageView?.contentMode = .scaleAspectFit

        resetButton.translatesAutoresizingMaskIntoConstraints = true
        resetButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        resetButton.addTarget(self, action: #selector(resetScene), for: .touchUpInside)
        resetButton.accessibilityLabel = NSLocalizedString("Reset scene", comment: "AX label for the 'reset scene' button")
        resetButton.accessibilityHint = NSLocalizedString("Returns all models to their original starting positions.", comment: "AX hint for the 'reset scene' button")
        resetControl.addSubview(resetButton)

        resetSceneControl = resetControl
    }
}

public final class PlacementDecoration : _InternalShape {
    public init() {
        super.init(requiresAdjustmentToSitOnPlane: false)

        let plane = SCNBox(width: CGFloat(6.cm), height: CGFloat(0.001.mm), length: CGFloat(6.cm), chamferRadius: 0.0)
        geometry = plane

        if let material = plane.firstMaterial {
            material.diffuse.contents = UIImage(named: "placement-circle")
            material.lightingModel = .constant
            material.locksAmbientWithDiffuse = true
        }
        castsShadow = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func placeAnimation(completion handler: (()->Void)?) {
        let actions: [BuiltinAction] = [.fadeTo(0, duration: 0.4), .scaleTo(1.5, duration: 0.4), .wait(duration: 0.6)]
        run(group: actions, completion: handler)
    }

    public func pickupAnimation() {
        let actions: [BuiltinAction] = [.fadeTo(1.0, duration: 0.5), .scaleTo(1.0, duration: 0.5)]
        run(group: actions)
    }
}


public extension UIView {
    func controlBuildIn(with delay: TimeInterval = 0.0) {
        alpha = 0.0
        transform = transform.translatedBy(x: 0.0, y: 40.0)
        isHidden = false

        UIView.animate(withDuration: 0.5, delay: delay, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform.identity
            self.alpha = 1.0

        }, completion: nil)
    }

    func controlBuildOut(with delay: TimeInterval = 0.0) {
        UIView.animate(withDuration: 0.4, delay: delay, options: .curveEaseOut, animations: {
            self.transform = self.transform.translatedBy(x: 0.0, y: 40.0)
            self.alpha = 0.0

        }) { _ in
            self.isHidden = true
            self.transform = CGAffineTransform.identity
        }
    }

    func funkyBuildIn(with delay: TimeInterval = 0.0) {
        alpha = 0.0
        isHidden = false

        var scaledRotate = CGAffineTransform(scaleX: 0.0, y: 0.0)
        scaledRotate = scaledRotate.rotated(by: -4.5 * .pi)
        transform = scaledRotate

        UIView.animate(withDuration: 0.8, delay: delay, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform.identity
            self.alpha = 1.0
        }, completion: nil)
    }

    func funkyBuildOut(with delay: TimeInterval = 0.0) {
        UIView.animate(withDuration: 0.6, delay: delay, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            self.alpha = 0.0
        }) { _ in
            self.isHidden = true
            self.transform = CGAffineTransform.identity
        }
    }
}

public final class FeaturePoints : SCNNode {
    public override init() {
        super.init()
        castsShadow = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func update(for featurePoints: ARPointCloud) {
        let stride = MemoryLayout<float3>.stride
        let pointData = Data(bytes: featurePoints.points, count: stride * featurePoints.points.count)

        let source = SCNGeometrySource(data: pointData, semantic: .vertex, vectorCount: featurePoints.points.count, usesFloatComponents: true, componentsPerVector: 3, bytesPerComponent: MemoryLayout<Float>.stride, dataOffset: 0, dataStride: stride)

        let element = SCNGeometryElement(data: nil, primitiveType: .point, primitiveCount: featurePoints.points.count, bytesPerIndex: 0)
        element.pointSize = 0.001
        element.minimumPointScreenSpaceRadius = 8
        element.maximumPointScreenSpaceRadius = 72

        geometry = SCNGeometry(sources: [source], elements: [element])

        let material = SCNMaterial()
        material.diffuse.contents = UIColor.featurePointsColor
        material.lightingModel = .constant
        material.blendMode = .screen
        geometry?.firstMaterial = material
    }
}
