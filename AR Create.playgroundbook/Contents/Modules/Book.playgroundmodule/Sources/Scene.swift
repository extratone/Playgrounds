//
//  Scene.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation

/**
 The AR scene.
 
 - localizationKey: Scene
 */
public final class Scene : _InternalScene, PlaceableContainer, SupportsColoring {

    var anchor = Point(x: 0, y: 0, z: 0)

    // Debug
    var showPlanes: Bool = true {
        didSet(show) {
            Scene.planes.forEach { _, plane in
                plane.makePlanesInivisible(show)
            }
        }
    }

    var showFeaturePoints: Bool = true {
        didSet(show) {
            guard showFeaturePoints else {
                Scene.featurePoints.removeFromParentNode()
                return
            }
            if Scene.featurePoints.parent == nil {
                scnScene?.rootNode.addChildNode(Scene.featurePoints)
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Placement UI

    /**
     Adds a model to the scene.
     
     - Parameter object: The model to add to the scene.
     
     - localizationKey: Scene.add(_ object:)
     */
    public func add(_ object: ImaginaryNode) {
        queue(model: QueuedModelDescription(object, nil))
    }
    
    /**
     Adds multiple models to the scene.
     
     - Parameter objects: The models to add to the scene.
     
     - localizationKey: Scene.add(_ objects:)
     */
    public func add(_ objects: [ImaginaryNode]) {
        for object in objects {
            queue(model: QueuedModelDescription(object, nil))
        }
    }

    /**
     Places a model in the scene at the given location.
     
     - Parameter object: The model to add to the scene.
     - Parameter point: The location the model will be placed.
     
     - localizationKey: Scene.place(_:at:)
     */
    public func place(_ object: ImaginaryNode, at point: Point) {
        if Scene.models.first(where: { $0 === object }) == nil {
            Scene.models.append(object)
        }

        object.setPosition(point)
        object.opacity = 0
        self.scnScene?.rootNode.addChildNode(object)
        object.runAction(.fadeOut(duration: 0), completionHandler: {
            object.appear()
        })
    }
    
    // This should be used when we place models via scene.add
    public func placeInQueue(_ object: ImaginaryNode, at point: Point) {
        if Scene.models.first(where: { $0 === object }) == nil {
            Scene.models.append(object)
        }
        
        object.setPosition(point)
        scnScene?.rootNode.addChildNode(object)
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Plane Detection

    public var onPlaneDetected: ((Plane) -> Void) = { _ in return }

    public func setPlaneDetectionHandler(_ handler: @escaping ((Plane) -> Void)) {
        onPlaneDetected = handler
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Startup

    /**
     The function that gets called when the scene starts.
     
     - localizationKey: Scene.onStart
     */
    public var onStart: (()-> Void) = { return }

    /**
     Sets the function that gets called when the scene starts.
     
     - Parameter handler: The function to call.
     
     - localizationKey: Scene.setOnStartHandler(_:)
     */
    public func setOnStartHandler(_ handler: @escaping (() -> Void)) {
        onStart = handler
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - SupportsColoring

    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        return [.clear]
    }

    public var scheme: ColorScheme = .basic
    
    /**
    Sets a color scheme across all models in the AR scene.
    
    - Parameter scheme: The color scheme to set.
    
    - localizationKey: Scene.applyColor(scheme:)
    */
    public func applyColor(scheme: ColorScheme) {
        for model in Scene.models {
            model.applyColor(scheme: scheme)
        }
    }

}
