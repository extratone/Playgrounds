//
//  InternalScene.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import ARKit

public typealias ModelPlacementCompletion = () -> Void
public typealias QueuedModelDescription = (model: ImaginaryNode, completion: ModelPlacementCompletion?)



public protocol ModelPlacementProtocol {
    func dequeueModel() -> QueuedModelDescription?
    func queue(model: QueuedModelDescription)
}

public enum SceneState {
    case detectingPlanes
    case placingModels
    case preparingToStart
    case started
}

public protocol SceneTransitionDelegate {
    func didTransition(to state: SceneState, from oldState: SceneState)
    func didSet(optionalStartMessage message: String)
}

public class _InternalScene: ModelPlacementProtocol, AccessibleElementTracking {
    var scnScene: SCNScene?

    var queuedObjects: [QueuedModelDescription] = []

    public static var planes: [ARPlaneAnchor : Plane] = [:]
    public static var models: [ImaginaryNode] = []
    public static var particleSystemNodes: [SCNNode] = []
    public static var featurePoints = FeaturePoints()
    public static var activeAXElements = [ARAccessibilityElement]()

    public static func colliderNodesForParticles(from node: ImaginaryNode) -> [SCNNode] {
        var nodes: [SCNNode] = Scene.planes.map { return $0.value.planeNode }

        let models = Scene.models.compactMap {
            return ($0 == node) ? nil : $0._rootNode
        }

        nodes.append(contentsOf: models)
        return nodes
    }


    /**
     The camera for the scene.
     
     - localizationKey: _InternalScene.camera
     */
    public var camera = Camera()

    public init() {
        // Nothing to do
    }

    public func connect(to sceneView: ARSCNView) {
        scnScene = sceneView.scene

        // add camera
        scnScene?.rootNode.addChildNode(camera.node)
    }

    func addParticleSystem(_ system: SCNParticleSystem, at point: Point) {
        let rotation = SCNMatrix4MakeRotation(0, 0, 0, 0)
        let translation = SCNMatrix4MakeTranslation(Float(point.x), Float(point.y), Float(point.z))
        let transform = SCNMatrix4Mult(rotation, translation)
        scnScene?.addParticleSystem(system, transform: transform)
    }

    func play(_ sound: Sound, loops: Bool = false) {
        let soundName = sound.rawValue + ".m4a"
        let audioSource = SCNAudioSource(fileNamed: soundName)!
        audioSource.loops = loops
        audioSource.load()
        audioSource.rate = 1
        let audioPlayer = SCNAudioPlayer(source: audioSource)
        audioPlayer.audioSource?.volume = 70
        scnScene?.rootNode.addAudioPlayer(audioPlayer)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - State Management

    public var state: SceneState = .detectingPlanes {
        didSet {
            transitionDelegate?.didTransition(to: state, from: oldValue)
        }
    }

    public var transitionDelegate: SceneTransitionDelegate?

    public func createStartScreen(with message: String) {
        transitionDelegate?.didSet(optionalStartMessage: message)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - ModelPlacementProtocol

    public func dequeueModel() -> QueuedModelDescription? {
        return queuedObjects.popLast()
    }

    public func queue(model: QueuedModelDescription) {
        queuedObjects.insert(model, at: 0)
    }

    // MARK: - Accessibility
    internal func addAccessibleElement(for node: ImaginaryNode, controller: ARLiveViewController) {
        let element = ARAccessibilityElement(node: node, controller: controller)
        _InternalScene.activeAXElements.append(element)
    }
    
    internal func removeAccessibleElement(for node: ImaginaryNode) {
        if let idx = _InternalScene.activeAXElements.firstIndex(where: { $0.backingNode! == node }) {
            _InternalScene.activeAXElements.remove(at: idx)
        }
    }
    
    internal func accessibleElement(for node: ImaginaryNode) -> ARAccessibilityElement? {
        return _InternalScene.activeAXElements.first { $0.backingNode! == node }
    }
    
    internal func removeAllAccessibleElements() {
        _InternalScene.activeAXElements.removeAll(keepingCapacity: true)
    }
    
    internal func accessibleItemsInScene() -> [ImaginaryNode] {
        let items = _InternalScene.models.filter{ return $0.opacity > 0.1 && $0.parent != nil }
        return items
    }
}
