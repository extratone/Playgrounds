//
//  RainCloudModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit
import UIKit

public final class RainCloudModel : _InternalModel, ImaginaryModel, CustomAnimatable, ParticleProtocol {
    public var particleSize: Float = 0.01
    public var particleColor: ColorDefinition = .clear
    
    public static var Identifier = "RainCloud"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.pinkLime]

        case .basic:
            return [.aquaWhite]

        case .wacky:
            return [.bluePink]
            
        case .cool:
            return [.blueAqua]
            
        case .moody:
            return [.black]
            
        case .hot:
            return [.redYellow]

        case .custom:
            return [Palette.Third]
        }
    }

    public func particleColor(for scheme: ColorScheme) -> ColorDefinition {
        switch scheme {
        case .tacky:
            return .black

        case .basic:
            return .aqua

        case .wacky:
            return .yellow

        case .cool:
            return .white

        case .moody:
            return .yellow

        case .hot:
            return .red

        case .custom:
            return Palette.Second
        }
    }
    
    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: RainCloudModel.Identifier, verticalOffset: .offPlane(CGFloat(13.cm)))
        self.name = RainCloudModel.Identifier
        applyColor(scheme: defaultScheme)
        
        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)
    }

    public enum Animation : CustomAnimation {
        case rain
    }

    /**
     Runs the provided animation.
     
     - Parameter animation: The animation to run.
     - Parameter repeats: The RepeatingType, which determines how many times the animation runs.
     - Parameter completion: The handler to run on completion of the animation running. There is no completion handler by default.
     
     - localizationKey: RainCloudModel.animate(_:repeats:completion:)
     */
    public func animate(_ animation: RainCloudModel.Animation, repeats: RepeatingType = .none, completion: (() -> (Void))? = {}) {

        switch animation {
        // TODO: Need to implement what it means for the rain to run a single time, loop, or repeat.
        case .rain:
            self.play(.rain, loops: true, completion: nil)
            rain()
        }

    }

    fileprivate func rain() {
        if let emitter = SCNParticleSystem(named: "Rain2.scnp", inDirectory: nil), let explode = SCNParticleSystem(named: "ParticleRemove", inDirectory: nil) {
            emitter.particleSize = emitter.particleSize * CGFloat(scale.x)
            emitter.particleDiesOnCollision = true
            emitter.systemSpawnedOnDying = explode
            emitter.colliderNodes = Scene.colliderNodesForParticles(from: self)
            
            emitter.particleColor = particleColor.asColor()
            explode.particleColor = particleColor.asColor()
            
            let cloudNode = SCNNode()
            cloudNode.name = RainCloudModel.Identifier
            self.addChildNode(cloudNode)
            Scene.particleSystemNodes.append(cloudNode)
            cloudNode.addParticleSystem(emitter)
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Rain cloud", comment: "AX title for the rain cloud model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored rain cloud with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored rain cloud.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored rain cloud with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored rain cloud.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored rain cloud with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored rain cloud.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored rain cloud with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored rain cloud.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored rain cloud with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored rain cloud.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored rain cloud with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored rain cloud.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored rain cloud with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored rain cloud.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A fluffy, white cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A fluffy, black cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A fluffy, lime colored cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A fluffy, aqua blue cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A fluffy, yellow cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A fluffy, pink cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A fluffy, uniquely colored rain cloud floating in the sky. Looks like it might rain soon.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate rain cloud.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of rain cloud.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between rain cloud", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and rain cloud", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
