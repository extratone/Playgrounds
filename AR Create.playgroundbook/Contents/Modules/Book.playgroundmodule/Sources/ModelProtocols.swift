//
//  ModelProtocols.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import ARKit
import AVKit
import GameplayKit
import CoreGraphics

public protocol ParticleProtocol {
    var particleSize: Float { get set }
    var particleColor: ColorDefinition { get set }
    
    func scaleParticles(by value: Float)
    
    func scaleParticles(to value: Float)
}


public extension ParticleProtocol where Self : ModelProtocol {
    func scaleParticles(by value: Float) {
        let emitterNodes = Scene.particleSystemNodes.filter { $0.name == Self.Identifier }
        for node in emitterNodes {
            if let particleSystem = node.particleSystems {
                for system in particleSystem {
                    system.particleSize = system.particleSize * CGFloat(value)
                }
            }
        }
    }
    
    func scaleParticles(to value: Float) {
        let emitterNodes = Scene.particleSystemNodes.filter { $0.name == Self.Identifier }
        for node in emitterNodes {
            if let particleSystem = node.particleSystems {
                for system in particleSystem {
                    system.particleSize = CGFloat(particleSize * value)
                }
            }
        }
    }
}

public protocol ModelProtocol {
    static var Identifier: String { get }

    var voice: Voice { get }
    
    var emitterOffset : (Float, Float) { get set }
    
    var location: Point { get }
    
    var hasDistanceEvents: Bool { get }

    func setPosition(_ position: Point)

    func look(at object: ImaginaryNode)

    func play(_ sound: Sound, rate: Float, loops: Bool, completion: (() -> Void)?)
    
    func speak(text: String, withAccent accent: SpeechAccent?, rate: Float?, pitch: Float?, completion: (() -> Void)?)
    
    func disappear()
    
    func appear()
    
    func appearAnimation(xOffset: Float, yOffset: Float)
    
    func disappearAnimation(xOffset: Float, yOffset: Float, completion: (() -> Void)?)

    func animate()
    
    func pop()
    
    func remove()

    func stop()

    func currentState() -> ModelState

    func setState(_ state: ModelState)
}

public typealias ModelState = (transform: float4x4, scale: Float, alpha: CGFloat, other: [String : String]?)

public extension ModelProtocol where Self : ImaginaryNode {
    var placementDescription: String {
        return Self.Identifier
    }

    /**
    Looks at the given model by turning to face it.
     
     - localizationKey: ImaginaryNode.look(at:)
     */
    func look(at object: ImaginaryNode) {
        self.constraints = _internalLookConstraint(for: object)
    }
    
    /**
     Removes the model from the scene.
     
     - localizationKey: ImaginaryNode.remove()
     */
    func remove() {
        if let i = Scene.models.firstIndex(where: { $0 == self }) {
            Scene.models.remove(at: i)
        }
        disappearAnimation(xOffset: 0, yOffset: 0, completion: {
            self.reset(includeActions: false)
            self.removeFromParentNode()
        })
    }

    // TODO: Want to make this come directly from the SCNNode instead of spoken globally
    // TODO: Completion handler is not being called from the AVSpeechSynthesizerDelegate, Voice.
    /**
     Speaks the given text.
     
     - Parameter text: The text to be spoken.
     - Parameter accent: Specifies the accent the text will be spoken with. Defaults to language setting of device.
     - Parameter rate: The rate at which text is spoken. Takes a value between `0` and `1`. Lower values produce slower speech sounds.
     - Parameter pitch: The baseline pitch at which text is spoken. Allowed values are in the range from 0.5 (for lower pitch) to 2.0 (for higher pitch).
     - Parameter completion: The handler to run on completion of the text speaking. This value is `nil` by default.
     
     - localizationKey: ImaginaryNode.speak(text:withAccent:rate:pitch:completion:)
     */
    func speak(text: String, withAccent accent: SpeechAccent? = nil, rate: Float? = nil, pitch: Float? = nil, completion: (() -> Void)? = nil) {
        self.voice.onCompletion = completion
        let utterance = AVSpeechUtterance(string: text)
        utterance.rate = rate ?? 0.4
        utterance.pitchMultiplier = pitch ?? 1.45
        utterance.volume = 0.70
        utterance.voice = AVSpeechSynthesisVoice(language: accent?.rawValue) ?? AVSpeechSynthesisVoice(language: AVSpeechSynthesisVoice.currentLanguageCode())
        self.voice.speak(utterance)
    }

    /**
     Plays the given sound. Optionally, specify whether the sound loops and if there is a handler to run on completion.
     
     - Parameter sound: The sound to be played.
     - Parameter loops: Specifies whether the sound should loop. This value is `false` by default.
     - Parameter rate: The rate at which the sound is played.
     - Parameter completion: The handler to run on completion of the sound playing. This value is `nil` by default.
     
     - localizationKey: ImaginaryNode.play(_:rate:loops:completion:)
     */
    func play(_ sound: Sound, rate: Float = 1, loops: Bool = false, completion: (() -> Void)? = nil) {
        let soundName = sound.rawValue + ".m4a"
        let audioSource = SCNAudioSource(fileNamed: soundName)!
        audioSource.loops = loops
        audioSource.load()
        audioSource.rate = Float(rate)
        let audioPlayer = SCNAudioPlayer(source: audioSource)
        audioPlayer.audioSource?.volume = 100
        self.addAudioPlayer(audioPlayer)
        audioPlayer.didFinishPlayback = completion
    }
    
    func appearAnimation(xOffset: Float, yOffset: Float) {
        // Our models are centered on the floor, so we offset by our model's vertical offset.
        let verticalOffset = yOffset + _InternalModel.VerticalOffset
        let horizontalOffset = xOffset
        
        let exp1 = SCNParticleSystem(named: "Explode", inDirectory: nil)!
        let exp2 = SCNParticleSystem(named: "ExplodeLines", inDirectory: nil)!
        
        let wait = SCNAction.wait(duration: 0.1)
        let scaleDown = SCNAction.scale(by: 0.1, duration: 0)
        let fadeIn = SCNAction.fadeIn(duration: 0.1)
        let scaleUp = SCNAction.scale(by: 10, duration: 0.2)
        let group = SCNAction.group([fadeIn, scaleUp])
        let sequence = SCNAction.sequence([wait, scaleDown, group])
        
        
        func addParticles() {
            self.play(.appear)
            let textCenter = SCNNode()
            self.addChildNode(textCenter)
            textCenter.position = SCNVector3(horizontalOffset, verticalOffset, 0) * self.scale
            
            textCenter.addParticleSystem(exp1)
            textCenter.addParticleSystem(exp2)
        }
        
        guard self.opacity < 0.2 else { return }
        addParticles()
        
        self.runAction(sequence)
    }
    
    func disappearAnimation(xOffset: Float, yOffset: Float, completion: (() -> Void)?) {
        // Our models are centered on the floor, so we offset by 3cm
        let verticalOffset = yOffset + _InternalModel.VerticalOffset
        let horizontalOffset = xOffset
        
        let exp1 = SCNParticleSystem(named: "Explode", inDirectory: nil)!
        let exp2 = SCNParticleSystem(named: "ExplodeLines", inDirectory: nil)!
        
        let wait = SCNAction.wait(duration: 0.1)
        let scaleDown = SCNAction.scale(by: 0.1, duration: 0.2)
        let fadeOut = SCNAction.fadeOut(duration: 0)
        let scaleUp = SCNAction.scale(by: 10, duration: 0)
        let sequence = SCNAction.sequence([wait, scaleDown, fadeOut, scaleUp])
        
        func addParticles() {
            self.play(.disappear)
            let textCenter = SCNNode()
            self.addChildNode(textCenter)
            textCenter.position = SCNVector3(horizontalOffset, verticalOffset, 0) * self.scale
            
            textCenter.addParticleSystem(exp1)
            textCenter.addParticleSystem(exp2)
        }
        
        guard self.opacity > 0.8 else { return }
        addParticles()
        
        self.runAction(sequence, completionHandler: completion)
    }
    
    /**
     The location of the model in 3D coordinate space.
     
     - localizationKey: ImaginaryNode.location
     */
    var location: Point {
        return Point(x: position.x, y: position.y, z: position.z)
    }
    
    /**
     Makes the model disappear.
     
     - localizationKey: ImaginaryNode.disappear()
     */
    func disappear() {
        disappearAnimation(xOffset: 0, yOffset: 0, completion: {
            self.reset(includeActions: false)
        })
    }

    func reset(includeActions: Bool) {
        constraints = []
        removeAllAudioPlayers()
        removeAllAnimations()
        if includeActions {
            removeAllActions()
        }
        simdScale = float3(1.0)

        voice.voiceSynthesizer.stopSpeaking(at: .immediate)

        childNodes.forEach { node in
            node.constraints = []
            node.removeAllParticleSystems()
            node.removeAllAnimations()
            if includeActions {
                node.removeAllActions()
            }
        }
    }

    /**
     Stops any animations, actions or sounds that are associated with the model and currently running.

     - localizationKey: ImaginaryNode.stop()
     */
    func stop() {
        reset(includeActions: true)
    }

    
    /**
     Makes the model appear. The model must have already disappeared to run `appear()` or no effect will occur.
     
     - localizationKey: ImaginaryNode.appear()
     */
    func appear() {
        appearAnimation(xOffset: 0, yOffset: 0)
    }

    /**
     Runs the default animation for the model.
     
     - localizationKey: ImaginaryNode.animate()
     */
    func animate() {
        let moveUp = SCNAction.move(by: SCNVector3(0, 2.cm, 0) * self.scale, duration: 1.75)
        moveUp.timingMode = .easeInEaseOut
        let moveDown = SCNAction.move(by: SCNVector3(0, -2.cm, 0) * self.scale, duration: 1.75)
        moveDown.timingMode = .easeInEaseOut
        let sequence = SCNAction.sequence([moveUp, moveDown])
        let floatAnimation = SCNAction.repeatForever(sequence)
        self.play(.float, loops: true, completion: nil)
        
        self.runAction(floatAnimation)
    }
    
    func pop() {
        let scaleDown = SCNAction.scale(by: 0.8, duration: 0.05)
        let scaleUp = SCNAction.scale(by: 2, duration: 0.2)
        scaleUp.timingMode = .easeInEaseOut
        let scaleDown2 = SCNAction.scale(by: 0.625, duration: 0.02)
        let rotate = SCNAction.rotateBy(x: 0, y: 10 * .pi, z: 0, duration: 1.3)
        rotate.timingMode = .easeInEaseOut
        let sequence = SCNAction.sequence([scaleDown, scaleUp, scaleDown2, rotate])
        self.play(.appear, loops: false, completion: {
            self.play(.toeWiggleSingleReverse)
        })
        self.runAction(sequence)
    }

    /**
     Adjusts the vertical position of the node, so that it sits atop a plane.

     - localizationKey: ImaginaryNode.adjust(onto:using)
     */
    func adjust(onto planeAnchor: ARPlaneAnchor, using offset: Float) {
        // Add 10% tolerance to the corners of the plane.
        let tolerance: Float = 0.1

        let minX: Float = planeAnchor.center.x - planeAnchor.extent.x / 2 - planeAnchor.extent.x * tolerance
        let maxX: Float = planeAnchor.center.x + planeAnchor.extent.x / 2 + planeAnchor.extent.x * tolerance
        let minZ: Float = planeAnchor.center.z - planeAnchor.extent.z / 2 - planeAnchor.extent.z * tolerance
        let maxZ: Float = planeAnchor.center.z + planeAnchor.extent.z / 2 + planeAnchor.extent.z * tolerance

        guard (minX...maxX).contains(self.worldPosition.x) && (minZ...maxZ).contains(self.worldPosition.z) else {
            return
        }


        guard let c = constraints else {
            self.simdPosition.y -= offset
            return
        }

        if !c.contains(where: { return $0.identifier == SCNDistanceConstraintIdentifier }) {
            // rdar://problem/49830921
            // only apply the adjustment if the model is not
            // following anything, otherwise the adjustment will
            // fight the constraint, and things get weird.
            self.simdPosition.y -= offset
        }
    }
}

public extension SCNNode {
    func look(at point: Point) {
        let node = SCNNode()
        node.position = point.vector3
        self.constraints = _internalLookConstraint(for: node)
    }

    func _internalLookConstraint(for node: SCNNode, away: Bool = false) -> [SCNConstraint] {
        let look = SCNLookAtConstraint(target: node)
        look.isGimbalLockEnabled = true
        look.isIncremental = true
        look.influenceFactor = 0.1
        look.localFront = away ? SCNVector3(0, 0, -1) : SCNVector3(0, 0, 1)

        // rdar://problem/48789238
        if let _ = self as? SnailModel {
            look.localFront = away ? SCNVector3(-1, 0, 0) : SCNVector3(1, 0, 0)
        }

        let orientation = SCNTransformConstraint.orientationConstraint(inWorldSpace: false) {
                (node, orient) -> SCNQuaternion in

            // disallow rotations on the x and z axis,
            // as a result of the lookAt constraint
            let theta = atan2f(orient.y, orient.w)
            let yRotation = simd_quatf(ix: 0, iy: sinf(theta), iz: 0, r: cosf(theta))

            return SCNQuaternion(yRotation.vector)
        }

        orientation.isIncremental = false

        return [look, orientation]
    }
}


/// An enumeration of all the different sounds that can be played. Some of the sounds you can make include: `ring`, `buzz`, and `chord`.
///
/// - localizationKey: Sound
public enum Sound: String, Codable, CaseIterable {
    case ring = "alarmclock_ring_loop_pingpong"
    case ringSingle = "alarmclock_ring_reverse_single"
    case tick = "alarmclock_tick_loop_pingpong_reverse"
    case tickSingle = "alarmclock_tick_single"
    case bounceAndPoint = "arrow_bounceandpoint_loop_pingpong_reverse_single"
    case spines = "cactus_spines_pingpong"
    case spinesLoop = "cactus_spines_single_loop"
    case spinesReverse = "cactus_spines_reverse"
    case tear = "eye_tear_loop"
    case eyeLook = "eye_look_pingpong"
    case eyeLookLoopSingle = "eye_look_loop_single"
    case eyeLookReverse = "eye_look_reverse"
    case float = "floating"
    case flap = "flyingbug_flap_loop_pingpong_reverse_single"
    case point = "hand_point_pingpong"
    case pointReverse = "hand_point_reverse"
    case pointSingleLoop = "hand_point_single_loop"
    case juice = "lemon_juice_loop"
    case buzz = "lightbulb_buzz"
    case click = "lighbulb_click"
    case frown = "mouth_frown_pingpong"
    case frownReverse = "mouth_frown_reverse"
    case frownSingleLoop = "mouth_frown_single_loop"
    case smile = "mouth_smile_pingpong"
    case smileReverse = "mouth_smile_reverse"
    case smileSingle = "mouth_smile_single_loop"
    case drip = "nose_drip"
    case appear = "poof_appear"
    case disappear = "poof_disappear"
    case rain = "raincloud_rain"
    case waggle = "snail_waggle_loop_pingpong"
    case waggleReverseSingle = "snail_waggle_reverse_single"
    case fingerWiggle = "hand_fingerwiggle_loop_pingpong"
    case fingerWiggleReverse = "hand_fingerwiggle_reverse"
    case fingerWiggleSingle = "hand_fingerwiggle_single"
    case toeWiggle = "foot_toewiggle_loop"
    case toeWiggleSingleReverse = "foot_toewiggle_single_reverse"
    case toeWigglePingPong = "foot_toewiggle_pingpong"
    case soundscape = "soundscape_single"
    case soundscapeLoop = "soundscape_loop"
    case rhythm = "rhythm_single"
    case rhythmLoop = "rhythm_loop"
    case arpeggioLoop = "arpeggio_loop"
    case arpeggio = "arpeggio_single"
    case chord = "chord_single"
    case chordLoop = "chord_loop"
    case melody = "melody_single"
    case melodyLoop = "melody_loop"
    case music = "startScreenMusicMono"
    case buttonClick = "startButton"
}


/// An enumeration of the available Speech accents, including: `arabic`, `englishUS`, `japanese`, and `german`.
///
/// - localizationKey: SpeechAccent
public enum SpeechAccent: String {
    case arabic = "ar-SA"
    case chinese = "zh-CN"
    case chineseHongKong = "zh-HK"
    case chineseTaiwan = "zh-TW"
    case czech = "cs-CZ"
    case danish = "da-DK"
    case dutch = "nl-BE"
    case englishAustralia = "en-AU"
    case englishIreland = "en-IE"
    case englishSouthAfrica = "en-ZA"
    case englishUK = "en-GB"
    case englishUS = "en-US"
    case finnish = "fi-FI"
    case frenchCanda = "fr-CA"
    case frenchFrance = "fr-FR"
    case german = "de-DE"
    case greek = "el-GR"
    case hebrew = "he-IL"
    case hindi = "hi-IN"
    case hungarian = "hu-HU"
    case indonesian = "id-ID"
    case italian = "it-IT"
    case japanese = "ja-JP"
    case korean = "ko-KR"
    case norwegian = "no-NO"
    case polish = "pl-PL"
    case portugueseBrazil = "pt-BR"
    case portuguesePortugal = "pt-PT"
    case romanian = "ro-RO"
    case russian = "ru-RU"
    case slovak = "sk-SK"
    case spanishMexico = "es-MX"
    case spanishSpain = "es-ES"
    case swedish = "sv-SE"
    case thai = "th-TH"
    case turkish = "tr-TR"
}

public typealias DistanceTriggerFragments = (between:String, and:String)
public protocol Accessibility {
    // One-to-two word description.
    var titleDescription: String { get }
    
    // The most basic information needed to indicate what this object is. Keep it short, it'll be used in bulk add/remove announcements as well as during the object placement phase, which is already very information dense.
    var shortDescription: String { get }
    
    // A more detailed description, triggered on-demand by the user.
    var detailedDescription: String { get }
    
    // Titles for actions each model will provide.
    var locateModelActionTitle: String { get }
    var describeModelActionTitle: String { get }
    
    // Distance triggers describe the two related actors. We can compose these strings when each model provides a localized "between" and "and" component for itself. This is a Localization sanctioned method.
    var distanceTriggerFragments: DistanceTriggerFragments { get }
}
