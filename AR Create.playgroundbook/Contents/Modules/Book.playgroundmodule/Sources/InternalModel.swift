//
//  InternalModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit
import ARKit


public class _InternalModel : SCNReferenceNode {
    public static var Ambience: Float = 0.0
    public static var Frequency: Float = 0.0
    public static let ModelSize: Float = 9.cm
    public static let VerticalOffset: Float = 3.cm

    public let voice = Voice()

    var isRigged: Bool

    public enum _ModelOffset {
        case none
        case bounds
        case offPlane(CGFloat)
    }

    var simdOffset: float3 = float3(0.0, 0.0, 0.0) 

    public init?(named: String, rigged: Bool = false, verticalOffset: _ModelOffset = .none, scale: Float = 1.0) {
        let prefix = rigged ? "SK_Labs_" : "SM_Labs_"
        let path = "SCNAssets.scnassets/Models/\(prefix)\(named)"
        guard let url = Bundle.main.url(forResource: path, withExtension: "dae") else {
            fatalError("Failed to find node at: '\(path).dae'.")
        }
        isRigged = rigged

        super.init(url: url)
        
        simdScale = float3(scale, scale, scale)

        switch verticalOffset {
        case .none:
            simdOffset = float3(0.0, 0.0, 0.0)

        case .bounds:
            let offsetY = boundingSphere.radius * scale
            simdOffset = float3(0.0, offsetY, 0.0)

        case .offPlane(let additional):
            let offsetY = boundingSphere.radius * scale
            simdOffset = float3(0.0, offsetY + Float(additional), 0.0)
        }

        load()
        _loadTextures(named: named)
        castsShadow = false

        let shape = SCNPhysicsShape(geometry: _rootGeometry, options: nil)
        physicsBody = SCNPhysicsBody(type: .kinematic, shape: shape)
        physicsBody?.contactTestBitMask = Int(SCNPhysicsCollisionCategory.default.rawValue)

        movabilityHint = .movable

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Internal API
    public override var _rootNode: SCNNode {
        return isRigged ? childNodes[0].childNodes[1] : childNodes[0]
    }

    func _loadTextures(named: String) {
        guard let material = _rootGeometry.materials.first else { return }

        let prefix = "SCNAssets.scnassets/Textures/T_Labs_\(named)_"
        let diffuse = UIImage(named: prefix + "RGBmask.png")

        material.diffuse.contents = diffuse
        material.lightingModel = .lambert
        material.isLitPerPixel = false

        let shaderPrefix = "SCNAssets.scnassets/Shaders/"
        do {
            let surface = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "surface", withExtension: "metal")!)
            let fragment = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "fragment", withExtension: "metal")!)
            let lighting = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "lighting", withExtension: "metal")!)

            material.shaderModifiers = [ .surface : surface, .fragment : fragment, .lightingModel : lighting ]
        }

        catch {
            // Bad things
            fatalError("*** Couldn't find shader: \(error.localizedDescription)")
        }
    }

    func _load(animation: String, for model: String) -> SCNAnimation {
        var anim: SCNAnimation?
        let animScene = SCNScene(named: "AN_Labs_\(model)@\(animation)", inDirectory: "SCNAssets.scnassets/Animations/")
        animScene?.rootNode.enumerateChildNodes { (child, stop) in
            if !child.animationKeys.isEmpty {
                guard let player = child.animationPlayer(forKey: child.animationKeys[0]) else { return }
                anim = player.animation
                stop.initialize(to: true)
            }
        }
        return anim!
    }


    public func setPosition(_ position: Point) {
        simdPosition = position.simdPoint + simdOffset
    }

    func setTransform(_ newTransform: float4x4, relativeToCamera cameraTransform: float4x4) {
        let cameraWorldPosition = cameraTransform.translation
        var positionOffsetFromCamera = newTransform.translation - cameraWorldPosition

        // Allow maximum distance of 10 meters from the camera
        if simd_length(positionOffsetFromCamera) > 10 {
            positionOffsetFromCamera = simd_normalize(positionOffsetFromCamera) * 10
        }

        simdPosition = cameraWorldPosition + positionOffsetFromCamera + simdOffset
    }

    public func currentState() -> ModelState {
        return (transform: presentation.simdTransform, scale: presentation.scale.x, alpha: presentation.opacity, nil)
    }

    public func setState(_ state: ModelState) {
        simdTransform = presentation.simdTransform

        SCNTransaction.begin()
        simdTransform = state.transform
        opacity = state.alpha
        simdScale = float3(state.scale, state.scale, state.scale)

        if !SCNMatrix4EqualToMatrix4(SCNMatrix4Identity, pivot) {
            // rdar://problem/48789238
            // Both the cactus and the foot models have a pivot value
            // applied so their "front" is properly rendered. This causes
            // a 90 degree rotation, which must be undone during a reset.
            simdLocalRotate(by: simd_quatf(angle: Float.pi/2.0, axis: float3(0, 1.0, 0)))
        }

        SCNTransaction.commit()
    }
    
    public var boundaryOwners: [ImaginaryNode] = []

    public var hasDistanceEvents: Bool {
        return boundaryOwners.count > 0
    }
    
    @available(*, unavailable, message: "The camera property is not accessible on models.") public override var camera: SCNCamera? {
        get {
            return nil
        }

        set {
            // Do nothing
        }
    }
}

extension SupportsColoring where Self : _InternalModel {
    /**
    Sets a color scheme, such as `.wacky` or `.cool`, on the model.
    
    - Parameter scheme: The color scheme to set.
    
    - localizationKey: _InternalModel.applyColor(scheme:)
    */
    public func applyColor(scheme: ColorScheme) {
        guard let material = _rootGeometry.materials.first else { return }
        let colors = self.colorArray(for: scheme)
        for index in Array(0..<4) {
            guard index < colors.count else {
                material.setValue(UIColor.black, forKey: "color\(index)")
                material.setValue(UIColor.black, forKey: "ColorLit\(index)")
                continue
            }

            let color = colors[index]
            material.setValue(color.asColor(), forKey: "color\(index)")
            material.setValue(litColor(for: color), forKey: "ColorLit\(index)")
        }

        if var localSelf = self as? ParticleProtocol {
            localSelf.particleColor = self.particleColor(for: scheme)
            self.childNodes.forEach { node in
                node.particleSystems?.forEach { emitter in
                    emitter.particleColor = localSelf.particleColor.asColor()
                    if let explode = emitter.systemSpawnedOnDying {
                        explode.particleColor = localSelf.particleColor.asColor()
                    }
                }
            }
        }
    }
}


extension _InternalModel : Actionable {
    public typealias ActionType = BuiltinAction
}
