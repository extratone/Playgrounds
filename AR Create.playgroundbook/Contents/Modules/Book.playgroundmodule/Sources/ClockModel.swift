//
//  ClockModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class ClockModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Clock"
    
    public var emitterOffset : (Float, Float) = (0, 0)
    
    public enum Animation : String, CustomAnimation {
        case ring = "AlarmRing"
        case tick = "HandMove"
    }

    public func animate() {
        pop()
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .lime, .limePink, .black]

        case .basic:
            return [.red, .lime, .pink, .black]

        case .wacky:
            return [.aquaLime, .pink, .blue, .black]
            
        case .cool:
            return [.blue, .blueAqua, .whiteAqua, .black]
            
        case .moody:
            return [.black, .white, .white, .black]
            
        case .hot:
            return [.yellowRed, .redYellow, .yellowWhite, .black]

        case .custom:
            return [Palette.Fourth, Palette.Second, Palette.First, Palette.Third]
        }
    }
    
    

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: ClockModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Alarm clock", comment: "AX title for the alarm clock model")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored alarm clock with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored alarm clock.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored alarm clock with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored alarm clock.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored alarm clock with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored alarm clock.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored alarm clock with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored alarm clock.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored alarm clock with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored alarm clock.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored alarm clock with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored alarm clock.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored alarm clock with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored alarm clock.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A classic style pink and red colored alarm clock. It has yellow alarm bells on the top and black hands on the face.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A classic style black and white colored alarm clock. It has white alarm bells on the top and black hands on the face.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A classic style pink and lime colored alarm clock. It has yellow alarm bells on the top and black hands on the face.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A classic style blue and aqua blue colored alarm clock. It has aqua blue colored alarm bells on the top and black hands on the face.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A classic style yellow and orange colored alarm clock. It has yellow alarm bells on the top and black hands on the face.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A classic style blue and lime colored alarm clock. It has pink alarm bells on the top and black hands on the face.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A classic style uniquely colored alarm clock. It has alarm bells on the top.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate alarm clock.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of alarm clock.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between alarm clock", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and alarm clock", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
