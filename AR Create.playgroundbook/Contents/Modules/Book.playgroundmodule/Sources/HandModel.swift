//
//  HandModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

public final class HandModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Hand"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime, .pink, .black]
            
        case .basic:
            return [.aqua, .red, .black]

        case .wacky:
            return [.pink, .aqua, .black]
            
        case .cool:
            return [.blue, .aquaWhite, .black]
            
        case .moody:
            return [.white, .black, .black]
            
        case .hot:
            return [.yellowRed, .black, .black]

        case .custom:
            return [Palette.Third, Palette.First, Palette.Second]
        }
    }
    
    public enum Animation : String, CustomAnimation {
        case point = "Point"
        case fingerWiggle = "FingerWiggle"

        public func shouldClipAnimation() -> Bool {
            switch self {
            case .point:
                return true

            default:
                return false
            }
        }

        public func customDuration() -> TimeInterval {
            switch self {
            case .point:
                return 3.0

            default:
                return 0.0
            }
        }
    }
    
    public func animate() {
        pop()
    }

    public var boundaries: [ModelBoundaryDescription] = []

    public var onSelected: (() -> Void) = { }

    public init?() {
        super.init(named: HandModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)
        
        physicsBody?.physicsShape = SCNPhysicsShape(geometry: SCNSphere(radius: CGFloat(_InternalModel.ModelSize)), options: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Hand", comment: "AX title for the hand model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored hand with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored hand.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored hand with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored hand.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored hand with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored hand.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored hand with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored hand.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored hand with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored hand.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored hand with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored hand.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored hand with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored hand.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A blue hand wearing black nail polish on each finger.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A white colored hand wearing black nail polish on each finger.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A yellow hand wearing black nail polish on each finger.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A blue hand wearing black nail polish on each finger.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange hand wearing black nail polish on each finger.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink hand wearing black nail polish on each finger.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored hand wearing black nail polish on each finger.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate hand.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of hand.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between hand", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and hand", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
