//
//  TextModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class TextModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "Text"
    
    public var emitterOffset : (Float, Float) = (7.m, 10.m)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    override public func setPosition(_ position: Point) {
        // Text geometries have their origins at the lower left corner
        // It's more intuitive to beginners for the text to be centered
        let extent = boundingBox.max - boundingBox.min

        let offsetLeft = (extent.x / 2.0) * scale.x
        let offsetPlane = Float(planeOffset) - extent.y * 0.3
        simdPosition = position.simdPoint + float3(-offsetLeft, offsetPlane, 0.0)
    }

    override public func currentState() -> ModelState {
        return (transform: presentation.simdTransform, scale: presentation.scale.x, alpha: presentation.opacity, other: ["text" : text])
    }

    override public func setState(_ state: ModelState) {
        guard let value = state.other?["text"] else { return }
        text = value
        super.setState(state)
    }

    // MARK: - Learner API

    public var text: String {
        didSet {
            guard let geo = geometry as? SCNText else { return }
            geo.string = text
        }
    }

    public var planeOffset: CGFloat

    public var fontSize: Float = 40.cm {
        didSet {
            guard let geo = geometry as? SCNText else { return }
            geo.font = UIFont.systemFont(ofSize: CGFloat(fontSize), weight: .medium)
        }
    }

    public var extrusionDepth: Float = _InternalModel.ModelSize {
        didSet {
            guard let geo = geometry as? SCNText else { return }
            geo.extrusionDepth = CGFloat(extrusionDepth)
        }
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }


    public init(string: String, planeOffset: Float) {
        text = string
        self.planeOffset = CGFloat(planeOffset)

        super.init()

        // set scale from 1m to 9cm
        scale = SCNVector3(_InternalModel.ModelSize, _InternalModel.ModelSize, _InternalModel.ModelSize)

        let geo = SCNText(string: text, extrusionDepth: CGFloat(extrusionDepth))
        geo.font = UIFont.systemFont(ofSize: CGFloat(fontSize), weight: .medium)
        geo.flatness = CGFloat(1.mm * scale.x)

        geometry = geo
        _createPhysicsBody(for: geo)
        applyColor(scheme: defaultScheme)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink, .limePink, .limePink]

        case .basic:
            return [.pink, .pink, .aqua]

        case .wacky:
            return [.aqua, .aqua, .pink]
            
        case .cool:
            return [.blue, .blue, .lime]
            
        case .moody:
            return [.black, .black, .white]
            
        case .hot:
            return [.yellowRed, .yellow, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second, Palette.Fourth]
        }
    }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Text", comment: "AX title for text rendered as a model in the scene.")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                String(format: NSLocalizedString("Basic colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Basic colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .moody:
            return hasTrigger ? 
                String(format: NSLocalizedString("Moody colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Moody colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .tacky:
            return hasTrigger ? 
                String(format: NSLocalizedString("Tacky colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Tacky colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .cool:
            return hasTrigger ? 
                String(format: NSLocalizedString("Cool colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Cool colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .hot:
            return hasTrigger ? 
                String(format: NSLocalizedString("Hot colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Hot colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .wacky:
            return hasTrigger ? 
                String(format: NSLocalizedString("Wacky colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Wacky colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .custom:
            return hasTrigger ? 
                String(format: NSLocalizedString("Uniquely colored text with a distance event that reads: %@", comment: "AX description of basic colored text with an associated distance event trigger. Placeholder is the text the user typed and could be anything, in any language."), text) : 
                String(format: NSLocalizedString("Uniquely colored text that reads: %@.", comment: "AX description of basic colored text. Placeholder is the text the user typed and could be anything, in any language."), text)
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = String(format:NSLocalizedString("Blue and pink text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a basic colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .moody:
            description = String(format:NSLocalizedString("Black and white text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a moody colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .tacky:
            description = String(format:NSLocalizedString("Pink and lime text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a tacky colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .cool:
            description = String(format:NSLocalizedString("Blue and lime text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a cool colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .hot:
            description = String(format:NSLocalizedString("Orange and red text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a hot colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .wacky:
            description = String(format:NSLocalizedString("Aqua blue and pink text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a wacky colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        case .custom:
            description = String(format:NSLocalizedString("Uniquely colored text that looks like it’s made from kitchen magnets. It says: %@.", comment: "AX detailed description of a uniquely colored model. Placeholder is the text the user typed and could be anything, in any language."), text)
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate text.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of text.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between text", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and text", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
