//
//  SnailModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit
import UIKit

public final class SnailModel : _InternalModel, ImaginaryModel, CustomAnimatable {
    public static var Identifier = "Snail"
    
    public var emitterOffset : (Float, Float) = (0, -1.cm)
    
    public enum Animation : String, CustomAnimation {
        case waggle = "BlinkWiggle"

        public func shouldClipAnimation() -> Bool {
            return true
        }

        public func customDuration() -> TimeInterval {
            return 5.8
        }
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.black, .limePink, .lime, .lime]

        case .basic:
            return [.lime, .blackBlack, .pink, .white]

        case .wacky:
            return [.bluePink, .black, .aqua, .lime]
            
        case .cool:
            return [.whiteAqua, .black, .blue, .blue]
            
        case .moody:
            return [.white, .black, .black, .white]
            
        case .hot:
            return [.yellowWhite, .black, .yellowRed, .yellowRed]

        case .custom:
            return [Palette.First, Palette.Third, Palette.Second, Palette.Fourth]
        }
    }
    
    

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: SnailModel.Identifier, rigged: true)
        applyColor(scheme: defaultScheme)
    }
    
    public func animate() {
        pop()
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }
    
    public func flee(from object: ImaginaryNode, safeDistance: Float) {
        _internalFlee(from: object, safeDistance: CGFloat(safeDistance), block: {
            self.animate(.waggle, repeats: .loop, completion: nil)
        })
    }
    
    public func follow(_ object: ImaginaryNode, at distance: Float) {
        _internalFollow(object, at: CGFloat(distance)) {
            self.animate(.waggle, repeats: .loop, completion: nil)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Snail", comment: "AX title for the snail node")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored snail with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored snail.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored snail with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored snail.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored snail with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored snail.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored snail with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored snail.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored snail with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored snail.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored snail with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored snail.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored snail with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored snail.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A lime colored snail wearing a pink shell covered in black swirls. It has white eyes and two wiggly black antennae on its head.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A white snail wearing a black shell covered in black swirls. It has white eyes and two wiggly black antennae on its head.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A black snail wearing a lime colored shell covered in pink swirls. It has lime colored eyes and two wiggly pink antennae on its head.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An aqua blue snail wearing a blue shell covered in black swirls. It has blue eyes and two wiggly black antennae on its head.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A yellow snail wearing an orange shell covered in black swirls. It has red eyes and two wiggly black antennae on its head.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink snail wearing a aqua blue shell covered in black swirls. It has lime colored eyes and two wiggly black antennae on its head.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored snail wearing a shell covered in swirls. It has two wiggly antennae on its head.", comment: "AX detailed description of a uniquely colored model")
        }

        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate snail.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of snail.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between snail", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and snail", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
