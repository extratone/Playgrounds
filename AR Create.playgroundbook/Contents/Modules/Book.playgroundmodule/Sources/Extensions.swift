//
//  Extensions.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import ARKit

extension float4x4 {
    /**
     Treats matrix as a (right-hand column-major convention) transform matrix
     and factors out the translation component of the transform.
     */
    var translation: float3 {
        let translation = columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}


extension UIColor {
    var scnVector: SCNVector4 {
        var red: CGFloat    = 1.0
        var green: CGFloat  = 1.0
        var blue: CGFloat   = 1.0
        var alpha: CGFloat  = 0.0

        if getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return SCNVector4(red, green, blue, alpha)
        }

        // If conversion fails, return Clear
        return SCNVector4(1.0, 1.0, 1.0, 0.0)
    }
}


public extension SCNVector3 {
    static func - (lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z)
    }

    static func * (lhs: SCNVector3, rhs:SCNVector3) -> SCNVector3 {
        return SCNVector3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z)
    }

    static func distanceBetween(_ vector1: SCNVector3, to vector2: SCNVector3) -> Float {
        let x0 = vector1.x
        let x1 = vector2.x
        let y0 = vector1.y
        let y1 = vector2.y
        let z0 = vector1.z
        let z1 = vector2.z
        
        return sqrtf(powf(x1-x0, 2) + powf(y1-y0, 2) + powf(z1-z0, 2))
    }
}


public extension SCNNode {
    @objc
    var _rootGeometry: SCNGeometry! {
        return _rootNode.geometry
    }

    @objc
    var _rootNode: SCNNode {
        return self
    }
}


public let SCNConstraintIdentifier = "SCNConstraintIdentifier"
public let SCNDistanceConstraintIdentifier = "SCNDistanceConstraintIdentifier"

public extension SCNConstraint {
    @objc var identifier: String { return SCNConstraintIdentifier }
}

public extension SCNDistanceConstraint {
    override var identifier: String { return SCNDistanceConstraintIdentifier }
}


extension SCNVector3 {
}
