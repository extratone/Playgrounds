//
//  LemonModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import SceneKit
import UIKit
import CoreGraphics

public final class LemonModel : _InternalModel, ImaginaryModel, CustomAnimatable, ParticleProtocol {
    public var particleSize: Float = 0.01
    public var particleColor: ColorDefinition = .clear
    
    public static var Identifier = "Lemon"
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.pinkLime, .lime, .black]
            
        case .basic:
            return [.yellow, .lime, .black]

        case .wacky:
            return [.aqua, .pink, .lime]
            
        case .cool:
            return [.aquaLime, .aquaLime, .blue]
            
        case .moody:
            return [.blackBlack, .white, .black]
            
        case .hot:
            return [.yellowRed, .yellowRed, .yellow]

        case .custom:
            return [Palette.First, Palette.Second, Palette.Third]
        }
    }

    public func particleColor(for scheme: ColorScheme) -> ColorDefinition {
        switch scheme {
        case .tacky:
            return .pink

        case .basic:
            return .lime

        case .wacky:
            return .lime

        case .cool:
            return .aqua

        case .moody:
            return .black

        case .hot:
            return .yellow

        case .custom:
            return Palette.Fourth
        }
    }

    public var boundaries: [ModelBoundaryDescription] = []
    
    public var onSelected: (() -> Void) = { }
    
    public init?() {
        super.init(named: LemonModel.Identifier, verticalOffset: .offPlane(CGFloat(8.cm)))
        self.name = LemonModel.Identifier
        applyColor(scheme: defaultScheme)
    }


    public enum Animation : CustomAnimation {
        case juice
    }

    /**
     Runs the provided animation.
     
     - Parameter animation: The animation to run.
     - Parameter repeats: The RepeatingType, which determines how many times the animation runs.
     - Parameter completion: The handler to run on completion of the animation running. There is no completion handler by default.
     
     - localizationKey: LemonModel.animate(_:repeats:completion:)
     */
    public func animate(_ animation: LemonModel.Animation, repeats: RepeatingType = .none, completion: (() -> (Void))? = {}) {
        switch animation {
        case .juice:
            self.play(.juice, loops: true, completion: nil)
            juice()

        }
    }
    
    public func animate() {
        pop()
    }

    fileprivate func juice() {
        if let emitter = SCNParticleSystem(named: "LemonRain", inDirectory: nil),  let explode = SCNParticleSystem(named: "ParticleRemove", inDirectory: nil) {
            emitter.particleSize = emitter.particleSize * CGFloat(scale.x)
            emitter.particleDiesOnCollision = true
            emitter.systemSpawnedOnDying = explode
            emitter.colliderNodes = Scene.colliderNodesForParticles(from: self)
            
            let verticalOffset = 0.8.cm

            emitter.particleColor = particleColor.asColor()
            explode.particleColor = particleColor.asColor()
            
            let lemonNode = SCNNode()
            lemonNode.name = LemonModel.Identifier
            self.addChildNode(lemonNode)
            lemonNode.position = SCNVector3(0, verticalOffset, -0.2.cm)
            lemonNode.addParticleSystem(emitter)
            Scene.particleSystemNodes.append(lemonNode)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Lemon", comment: "AX title for the lemon model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored lemon with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored lemon.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored lemon with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored lemon.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored lemon with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored lemon.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored lemon with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored lemon.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored lemon with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored lemon.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored lemon with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored lemon.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored lemon with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored lemon.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("A yellow lemon cut in half, showing black seeds inside.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A white colored lemon cut in half, showing black seeds inside.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("A yellow lemon cut in half, showing black seeds inside.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A lime colored lemon cut in half, showing blue seeds inside.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An orange colored lemon cut in half, showing yellow seeds inside.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("A pink lemon cut in half, showing yellow seeds inside.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored lemon cut in half, showing seeds inside.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate lemon.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of lemon.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between lemon", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and lemon", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
