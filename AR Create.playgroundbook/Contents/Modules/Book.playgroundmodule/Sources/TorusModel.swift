//
//  TorusModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public final class TorusModel : _InternalShape, ImaginaryModel {
    public static var Identifier = "Torus"
    
    public var emitterOffset : (Float, Float) = (0, -_InternalModel.VerticalOffset)

    public var onSelected: (() -> Void) = { }

    public var boundaries: [ModelBoundaryDescription] = []

    init(diameter: CGFloat, thickness: CGFloat) {
        super.init(requiresAdjustmentToSitOnPlane: true)

        let torus = SCNTorus(ringRadius: diameter / 2.0, pipeRadius: (diameter * thickness) / 2.0)
        geometry = torus
        _createPhysicsBody(for: torus)
        applyColor(scheme: defaultScheme)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func appear() {
        appearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1)
    }
    
    public func disappear() {
        disappearAnimation(xOffset: emitterOffset.0, yOffset: emitterOffset.1, completion: {
            self.reset(includeActions: false)
        })
    }

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.lime]

        case .basic:
            return [.aqua]

        case .wacky:
            return [.pinkAqua]
            
        case .cool:
            return [.blue]
            
        case .moody:
            return [.black]
            
        case .hot:
            return [.yellowRed]

        case .custom:
            return [Palette.First]
        }
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Torus", comment: "AX title for the torus model.")
    }

    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored torus with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored torus.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored torus with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored torus.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored torus with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored torus.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored torus with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored torus.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored torus with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored torus.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored torus with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored torus.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored torus with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored torus.", comment: "AX description of a custom colored model")
        }
    }
    
    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("An aqua blue donut-shaped ring.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("A black donut-shaped ring.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("An lime colored donut-shaped ring.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("A blue donut-shaped ring.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("A yellow and red donut-shaped ring.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("An aqua blue and pink donut-shaped ring.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("A uniquely colored donut-shaped ring.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate torus.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of torus.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between torus", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and torus", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
