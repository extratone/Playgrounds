//
//  InternalShape.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import SceneKit

public class _InternalShape : SCNNode {
    public let voice = Voice()

    public func setPosition(_ position: Point) {
        var simdOffset = float3(0.0, 0.0, 0.0)

        if requiresAdjustmentToSitOnPlane {
            let volume = boundingBox
            simdOffset.y = abs(volume.max.y - volume.min.y) / 2.0
        }
        simdPosition = position.simdPoint + simdOffset
    }

    var requiresAdjustmentToSitOnPlane: Bool

    init(requiresAdjustmentToSitOnPlane: Bool = false) {
        self.requiresAdjustmentToSitOnPlane =  requiresAdjustmentToSitOnPlane
        super.init()

        castsShadow = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func _shaderModifiers() -> [SCNShaderModifierEntryPoint : String]? {
        let shaderPrefix = "SCNAssets.scnassets/Shaders/"
        do {
            let surface = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "surface", withExtension: "metal")!)
            let fragment = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "fragment", withExtension: "metal")!)
            let lighting = try String(contentsOf: Bundle.main.url(forResource: shaderPrefix + "lighting", withExtension: "metal")!)

            return [ .surface : surface, .fragment : fragment, .lightingModel : lighting ]
        }

        catch {
            // Bad things
            fatalError("*** Couldn't find shader: \(error.localizedDescription)")
        }
    }

    func _createPhysicsBody(for geometry: SCNGeometry) {
        let shape = SCNPhysicsShape(geometry: geometry, options: nil)
        physicsBody = SCNPhysicsBody(type: .kinematic, shape: shape)

        movabilityHint = .movable
    }

    public func currentState() -> ModelState {
        return (transform: presentation.simdTransform, scale: presentation.scale.x, alpha: presentation.opacity, nil)
    }

    public func setState(_ state: ModelState) {
        simdTransform = presentation.simdTransform

        SCNTransaction.begin()
        simdTransform = state.transform
        opacity = state.alpha
        simdScale = float3(state.scale, state.scale, state.scale)

        SCNTransaction.commit()
    }
    
    public var boundaryOwners: [ImaginaryNode] = []

    public var hasDistanceEvents: Bool {
        return boundaryOwners.count > 0
    }
    
    @available(*, unavailable, message: "The camera property is not accessible on models.") public override var camera: SCNCamera? {
        get {
            return nil
        }
        
        set {
            // Do nothing
        }
    }
}

extension _InternalShape : Actionable {
    public typealias ActionType = BuiltinAction
}

extension SupportsColoring where Self : _InternalShape {
    /**
    Sets a color scheme on the shape.
    
    - Parameter scheme: The color scheme to set.
    
    - localizationKey: _InternalShape.applyColor(scheme:)
    */
    public func applyColor(scheme: ColorScheme) {
        let colors = self.colorArray(for: scheme)

        geometry?.materials = (0..<colors.count).map { index in
            let material = SCNMaterial()
            material.setValue(colors[index].asColor(), forKey: "color0")
            material.setValue(litColor(for: colors[index]), forKey: "ColorLit0")

            for placeHolder in Array(1..<4) {
                material.setValue(UIColor.black, forKey: "color\(placeHolder)")
                material.setValue(UIColor.black, forKey: "ColorLit\(placeHolder)")
            }

            material.shaderModifiers = self._shaderModifiers()
            return material
        }
    }
}
