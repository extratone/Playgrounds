//
//  StarModel.swift
//  
//  Copyright © 2016-2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

public final class StarModel : _InternalModel, ImaginaryModel {
    public var boundaries: [ModelBoundaryDescription] = []
    
    public var emitterOffset : (Float, Float) = (0, 0)

    public var scheme: ColorScheme = .basic
    
    public func colorArray(for scheme: ColorScheme) -> [ColorDefinition] {
        switch scheme {
        case .tacky:
            return [.limePink]

        case .basic:
            return [.yellow]

        case .wacky:
            return [.bluePink]
            
        case .cool:
            return [.blue]
            
        case .moody:
            return [.white]
            
        case .hot:
            return [.yellowRed]

        case .custom:
            return [Palette.Second]
        }
    }
    

    public static var Identifier = "Star"

    public var onSelected: (() -> Void) = { }

    public init?() {
        super.init(named: StarModel.Identifier, verticalOffset: .offPlane(CGFloat(8.cm)))
        applyColor(scheme: defaultScheme)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Accessibility
    public var titleDescription: String {
        return NSLocalizedString("Star", comment: "AX title for the star model")
    }
    
    public var shortDescription: String {
        let hasTrigger = hasDistanceEvents
        switch scheme {
        case .basic:
            return hasTrigger ? 
                NSLocalizedString("A basic colored star with a distance event.", comment: "AX description of a basic colored model with an associated distance event trigger.") : 
                NSLocalizedString("A basic colored star.", comment: "AX description of a basic colored model")
        case .moody:
            return hasTrigger ? 
                NSLocalizedString("A moody colored star with a distance event.", comment: "AX description of a moody colored model with an associated distance event trigger.") : 
                NSLocalizedString("A moody colored star.", comment: "AX description of a moody colored model")
        case .tacky:
            return hasTrigger ? 
                NSLocalizedString("A tacky colored star with a distance event.", comment: "AX description of a tacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A tacky colored star.", comment: "AX description of a tacky colored model")
        case .cool:
            return hasTrigger ? 
                NSLocalizedString("A cool colored star with a distance event.", comment: "AX description of a cool colored model with an associated distance event trigger.") : 
                NSLocalizedString("A cool colored star.", comment: "AX description of a cool colored model")
        case .hot:
            return hasTrigger ? 
                NSLocalizedString("A hot colored star with a distance event.", comment: "AX description of a hot colored model with an associated distance event trigger.") : 
                NSLocalizedString("A hot colored star.", comment: "AX description of a hot colored model")
        case .wacky:
            return hasTrigger ? 
                NSLocalizedString("A wacky colored star with a distance event.", comment: "AX description of a wacky colored model with an associated distance event trigger.") : 
                NSLocalizedString("A wacky colored star.", comment: "AX description of a wacky colored model")
        case .custom:
            return hasTrigger ? 
                NSLocalizedString("A uniquely colored star with a distance event.", comment: "AX description of a custom colored model with an associated distance event trigger.") : 
                NSLocalizedString("A uniquely colored star.", comment: "AX description of a custom colored model")
        }
    }

    public var detailedDescription: String {
        var description:String = ""
        switch scheme {
        case .basic:
            description = NSLocalizedString("An asymmetric, cartoonish, yellow star.", comment: "AX detailed description of a basic colored model")
        case .moody:
            description = NSLocalizedString("An asymmetric, cartoonish, white colored star.", comment: "AX detailed description of a moody colored model")
        case .tacky:
            description = NSLocalizedString("An asymmetric, cartoonish, pink and lime colored star.", comment: "AX detailed description of a tacky colored model")
        case .cool:
            description = NSLocalizedString("An asymmetric, cartoonish, blue star.", comment: "AX detailed description of a cool colored model")
        case .hot:
            description = NSLocalizedString("An asymmetric, cartoonish, orange and yellow star.", comment: "AX detailed description of a hot colored model")
        case .wacky:
            description = NSLocalizedString("An asymmetric, cartoonish, pink star.", comment: "AX detailed description of a wacky colored model")
        case .custom:
            description = NSLocalizedString("An asymmetric, cartoonish, uniquely colored star.", comment: "AX detailed description of a uniquely colored model")
        }
        
        return description
    }

    public var locateModelActionTitle: String {
        return NSLocalizedString("Locate star.", comment: "AX title for action")
    }
    
    public var describeModelActionTitle: String {
        return NSLocalizedString("Detailed description of star.", comment: "AX title for action")
    }

    public var distanceTriggerFragments: DistanceTriggerFragments {
        return (between: NSLocalizedString("between star", comment: "AX preposition string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."),
                and: NSLocalizedString("and star", comment: "AX conjunction string fragment, used to compose distance trigger descriptions (e.g. 'Trigger distance event <between camera> <and moon>."))
    }

}
