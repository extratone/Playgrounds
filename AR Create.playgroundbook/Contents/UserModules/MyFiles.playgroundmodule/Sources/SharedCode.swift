//#-localizable-zone(StartingPointSharedCode01)
// Shared Code
// Code written in this file is available on all pages in this Playground Book.
//#-end-localizable-zone
import SceneKit

public var scene = Scene()

public var allModels: [ImaginaryNode] = [Model.alarmClock, Model.arrow, Model.cactus, Model.ear, Model.eye, Model.flyingBug, Model.foot, Model.hand, Model.lemon, Model.lightbulb, Model.lightning, Model.moon, Model.nose, Model.mouth, Model.raincloud, Model.snail, Model.star, Model.sun]
public var origin = Point(x: 0, y: 0, z: 0)
var xOffset = -48.cm
var modelsPlacedCount = 0

//#-localizable-zone(StartingPointSharedCode02)
// Take all of the models in the allModels array, and place them onto the scene around the origin.
//#-end-localizable-zone
public func placeAllModels() {
    let model = allModels[modelsPlacedCount]
    scene.place(model, at: Point(x: origin.x - xOffset, y: origin.y, z: origin.z - 30.cm))
    model.animate()
    xOffset += 8.cm
    if modelsPlacedCount + 1 < allModels.count {
        modelsPlacedCount += 1
        model.play(.ring, loops: false, completion: placeAllModels)
    }
}

