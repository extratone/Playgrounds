import CoreGraphics

//#-localizable-zone(Feet01)
// Follow the feet until something special happens.
//#-end-localizable-zone
public func followTheFeet() {
    var foot = Model.foot
    var label = Model.text("/*#-localizable-zone(FeetText01)*/Follow the feet!/*#-end-localizable-zone*/", elevation: 5.cm)
    
    scene.add([foot, label])
    
    scene.setOnStartHandler {
        foot.run(action: .moveBy(x: 0, y: 5.cm, z: 0, duration: 0.5))
        foot.speak(text: "/*#-localizable-zone(FeetText02)*/Follow the feet. Follow the toes. Come on!/*#-end-localizable-zone*/")
        foot.play(.arpeggio, loops: true)
        foot.repeat(.loop, sequence: [.moveBy(x: 0, y: 3.cm, z: 0, duration: 0.1), .rotateBy(x: 0.7, y: 0, z: 0, duration: 0.2), .moveBy(x: 0, y: -3.cm, z: 0, duration: 0.1), .rotateBy(x: -0.7, y: 0, z: 0, duration: 0.2)], completion: nil)
    }
    
    var count = 5
    scene.camera.when(foot, isWithin: 3.cm, do: {
        if count > 0 {
            foot.run(action: .fadeTo(0, duration: 0.2))
            foot.play(.ring, completion: {
                scene.place(foot, at: Point(x: foot.location.x + Float.random(in: -20.cm...20.cm), y: 0, z: foot.location.z + Float.random(in: -20.cm...20.cm)))
                foot.run(action: .fadeTo(1, duration: 0.2))
                count -= 1
            })
        } else {
            foot.remove()
            let eye = Model.eye
            scene.place(eye, at: foot.location)
            eye.animate(.lookRight, completion: {
                eye.animate(.lookLeft, completion: {
                    eye.play(.arpeggioLoop)
                    eye.animate(.tear)
                    eye.speak(text: "/*#-localizable-zone(FeetText03)*/Thank you for freeing me from that invisible prison! These are happy tears, not sad tears./*#-end-localizable-zone*/")
                })
            })
        }
        
    })
}


