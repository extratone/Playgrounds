//#-localizable-zone(Friend01)
// Do something friendly when you get close to the eye.
//#-end-localizable-zone
public func doSomethingFriendly() {
    let leftEye = Model.eye
    let rightEye = Model.eye
    let label = Model.text("/*#-localizable-zone(FriendText01)*/LOOK INTO MY EYES!/*#-end-localizable-zone*/", elevation: 5.cm)
    
    scene.add([leftEye, rightEye, label])
    
    scene.setOnStartHandler {
        leftEye.play(.chord, loops: true, completion: nil)
        leftEye.run(action: .moveBy(x: 0, y: 12.cm, z: 0, duration: 1))
        rightEye.run(action: .moveBy(x: 0, y: 12.cm, z: 0, duration: 1))
    }
    
    scene.camera.when(leftEye, isWithin: 5.cm) {
        leftEye.animate(.lookLeft, repeats: .pingpong)
        rightEye.animate(.lookLeft, repeats: .pingpong)
        
        let centerDistance = (rightEye.location.x - leftEye.location.x) / 2
        
        let mouth = Model.mouth
        mouth.run(action: .scaleTo(3, duration: 0))
        
        let nose = Model.nose
        scene.place(nose, at: Point(x: leftEye.location.x + centerDistance, y: leftEye.location.y - 5.cm, z: leftEye.location.z))
        scene.place(mouth, at: Point(x: leftEye.location.x + centerDistance, y: leftEye.location.y - 9.cm, z: leftEye.location.z))
        
        mouth.animate(.smile, repeats: .loop)
        nose.animate(.drip)
        mouth.speak(text: "/*#-localizable-zone(FriendText02)*/Why hello there, my dear friend. You look even better up close!/*#-end-localizable-zone*/")
    }
}
