//#-localizable-zone(Roses01)
// Find out what happens when you stop to smell the roses.
//#-end-localizable-zone
public func smellTheRoses() {
    var arrow = Model.arrow
    var label = Model.text("/*#-localizable-zone(RosesText01)*/COME HERE!/*#-end-localizable-zone*/", elevation: 5.cm)
    let cactus = Model.cactus
    
    scene.add([arrow, label])
    
    scene.camera.when(arrow, isWithin: 2.cm, do: {
        arrow.remove()
        scene.place(cactus, at: arrow.location)
        label.text = "/*#-localizable-zone(RosesText02)*/Smell the roses!/*#-end-localizable-zone*/"
        label.speak(text: "/*#-localizable-zone(RosesText03)*/Smell the roses!/*#-end-localizable-zone*/", rate: 0.2, pitch: 1.9)
        cactus.run(action: .scaleBy(3, duration: 10))
    })
    
    scene.camera.when(cactus, isWithin: 2.cm, do: {
        cactus.animate()
        cactus.speak(text: "/*#-localizable-zone(RosesText04)*/I smell pretty incredible, don’t I?/*#-end-localizable-zone*/")
    })
}
