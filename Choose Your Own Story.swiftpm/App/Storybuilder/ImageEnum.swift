import SwiftUI

enum Images: CaseIterable {
    case wizardLibrary
    case spaceWhale
    case astronautOopsy
    case elfFairyLand
    case magicalDoors
    case pirateShip
    case foxTeaParty
    case treasure
}
