import SwiftUI

struct MyStoryBook: StoryBook {
    /*#-code-walkthrough(0.titlePage)*/
    var title = "Blimp's Burden"
    /*#-code-walkthrough(0.titlePage)*/
    /*#-code-walkthrough(1.titlePage)*/
    var author = "David Blue"
    /*#-code-walkthrough(1.titlePage)*/
    /*#-code-walkthrough(2.titlePage)*/
    var coverImage : CoverImage = .sword
    /*#-code-walkthrough(2.titlePage)*/
    
    var stories: some Stories {
        TitleCover()
        CopyrightPage()
        DedicationPage()
        FirstChapter()
        SecondChapter()
        ThirdChapter()
    }
}

