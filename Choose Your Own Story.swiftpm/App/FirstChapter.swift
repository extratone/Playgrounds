import SwiftUI

struct FirstChapter: Story {
    var myStory: some Prose {
        TitlePage {
            Picture(.spaceWhale)
            Chapter(number: 1)
            /*#-code-walkthrough(2a.writeTheStory)*/
            Title("Space Whale")
            /*#-code-walkthrough(2a.writeTheStory)*/
        }
        "Gruel never yielded anything to a single soul, but most of the apartment’s occasional guests – when they’d pay her any mind at all – mistook her resistance for the indifference of morbid obesity, or the pervading detachment from life and reality resulting from prolonged suffering from the same. Barney had loved her in precisely the same cheesily-affectionate manner with which he did in their kittenhood, uninterrupted for nearly a decade, and thought he knew for certain how she felt about his touch – he assumed that when she fled in a sudden, fluid, ground-covering saunter to her corner’s solitude, it was only an unfortunate, but inevitable involuntary reflex to his stocky presence, when in fact his volume failed one hundred percent of his hundreds of attempts to command any whatsoever. In the midst of female and/or unfamiliar company, he’d make a point to bend his bulbous puckering down to search for her eyes with his own, rubbing lightly together the pads of his thumb and index finger as he very deliberately extended the whole of the grubby device toward her snout, as if to point at her with the foul racket. For her ears, it was sautéed nails in a chamber pot; for her tastes, it was the sound of death."
        PageBreak()
        "In actuality, she had never once experienced even a wee grain of fear of the imbecile. As it happens, no living being – thinking and not – ever had or ever would, for the extent of his existence. If Gruel had been bestowed with the most boundless, ever-giving gift of complex linguistics, she wouldn’t have batted an eye before loudly protesting her sentiments about his life, his noise, and his rank-ass hands as they approached her, sitting static in her green bowelled-out faux leather recliner, attempting to murder him with her eyes. Unfortunately, gray Tabbies are quite unlucky – or perhaps just out of all favor in the eyes of Chance, because they are the least equipped mammals by far when it comes to domestic dialog, though Gruel had learned to tolerate her companion’s pervasively meek nature. She was allowed to languish about for the last few years of her life, eating just a bit more than she should every day, warming herself in the windowsills bathing among welcome patches of crisp mountain sun – on especially frisky occasions, even explicitly expressing her disgust by knocking one or two of Barney’s collectible anime figurines off their wall shelf and onto the creaky, disused air hockey table in the corner. It’s fairly widely-accepted in the scientific community that she, being feline, 1) could not possibly understand her own mortality in any substantial way, 2) could not maintain any sort of concept of the future, and 3) was extremely metacognitively limited, generally – without the ability to self-reflect. And yet, Gruel’s day-to-day demeanor – were you able to translate the thoughts occupying the space between her two (especially soft) ears into an English internal dialogue – was full of easily recognizable human utterances."
        /*#-code-walkthrough(2b.writeTheStory)*/
        /*#-code-walkthrough(4.writeTheStory)*/
        StoryPage {
            Picture(.pirateShip)
        }
        
        /*#-code-walkthrough(4.writeTheStory)*/

    }
}



struct FirstChapterView_Previews : PreviewProvider {
    static var previews: some View {
        StoryNodePresenter(node: FirstChapter().myStory.storyNode, book: MyStoryBook())
            .storyNodeBackgroundStyle()
    }
}



