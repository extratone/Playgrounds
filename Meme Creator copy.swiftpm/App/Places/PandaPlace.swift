import CoreLocation
import MapKit

/*#-code-walkthrough(pandaPlace.PandaPlace)*/
struct PandaPlace: Identifiable, Codable {
    /*#-code-walkthrough(pandaPlace.PandaPlace)*/
    /*#-code-walkthrough(pandaPlace.id)*/
    var id: Int
    /*#-code-walkthrough(pandaPlace.id)*/
    //#-learning-code-snippet(pandaPlace.properties)
    
    //#-learning-code-snippet(pandaPlace.location)
    
    //#-learning-code-snippet(pandaPlace.imageURL)
    
}
