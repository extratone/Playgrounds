//#-learning-task(MotionDetector)
/*#-code-walkthrough(MotionDetector.import)*/
import CoreMotion
/*#-code-walkthrough(MotionDetector.import)*/
import UIKit

class MotionDetector: ObservableObject {
    /*#-code-walkthrough(MotionDetector.motionManager)*/
    private let motionManager = CMMotionManager()
    /*#-code-walkthrough(MotionDetector.motionManager)*/

    /*#-code-walkthrough(MotionDetector.timerProperties)*/
    private var timer = Timer()
    private var updateInterval: TimeInterval
    /*#-code-walkthrough(MotionDetector.timerProperties)*/

    /*#-code-walkthrough(MotionDetector.publishedProperties)*/
    @Published var pitch: Double = 0
    @Published var roll: Double = 0
    @Published var yaw: Double = 0
    @Published var xAcceleration: Double = 0
    @Published var yAcceleration: Double = 0
    @Published var zAcceleration: Double = 0
    /*#-code-walkthrough(MotionDetector.publishedProperties)*/

    /*#-code-walkthrough(MotionDetector.onUpdateProperty)*/
    var onUpdate: (() -> Void) = {}
    /*#-code-walkthrough(MotionDetector.onUpdateProperty)*/

    init(updateInterval: TimeInterval) {
        self.updateInterval = updateInterval
    }
    
    /*#-code-walkthrough(MotionDetector.startMethod)*/
    func start() {
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.startDeviceMotionUpdates()
            
            /*#-code-walkthrough(MotionDetector.Timer)*/
            timer = Timer.scheduledTimer(withTimeInterval: updateInterval, repeats: true) { [weak self] _ in
                self?.updateMotionData()
            }
            /*#-code-walkthrough(MotionDetector.Timer)*/
        } else {
            print("Device motion not available")
        }
    }
    /*#-code-walkthrough(MotionDetector.startMethod)*/
    
    /*#-code-walkthrough(MotionDetector.updateMotionDataMethod.updateValues)*/
    func updateDetector(with data: CMDeviceMotion) {
        roll = data.attitude.roll
        pitch = data.attitude.pitch
        yaw = data.attitude.yaw
        xAcceleration = data.userAcceleration.x
        yAcceleration = data.userAcceleration.y
        zAcceleration = data.userAcceleration.z
    }
    /*#-code-walkthrough(MotionDetector.updateMotionDataMethod.updateValues)*/
    
    /*#-code-walkthrough(MotionDetector.updateMotionDataMethod)*/
    func updateMotionData() {
        /*#-code-walkthrough(MotionDetector.updateMotionDataMethod.verifyData)*/
        if let data = motionManager.deviceMotion {
            updateDetector(with: data)
            /*#-code-walkthrough(MotionDetector.updateMotionDataMethod.verifyData)*/
            onUpdate()
        }
    }
    /*#-code-walkthrough(MotionDetector.updateMotionDataMethod)*/

    /*#-code-walkthrough(MotionDetector.stopMethod)*/
    func stop() {
        motionManager.stopDeviceMotionUpdates()
        timer.invalidate()
    }

    deinit {
        stop()
    }
    /*#-code-walkthrough(MotionDetector.stopMethod)*/
}

extension MotionDetector {
    func started() -> MotionDetector {
        start()
        return self
    }
}
