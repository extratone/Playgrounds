import SwiftUI

struct RawData: View {
    @EnvironmentObject var detector: MotionDetector
    
    var body: some View {
        VStack {
            Group {
                Text("Motion data!")
                    .foregroundColor(.accentColor)
                /*#-code-walkthrough(rawData.addText)*/
                Text("Yaw: \(detector.yaw)")
                /*#-code-walkthrough(rawData.gyroscope)*/
                Text("Pitch: \(detector.pitch)")
                Text("Roll: \(detector.roll)")
                /*#-code-walkthrough(rawData.zacceleration)*/
                Text("z-Acceleration: \(detector.zAcceleration)")
                /*#-code-walkthrough(rawData.xyacceleration)*/
                Text("x-Acceleration: \(detector.xAcceleration)")
                Text("y-Acceleration: \(detector.yAcceleration)")
                /*#-code-walkthrough(rawData.xyacceleration)*/
                /*#-code-walkthrough(rawData.zacceleration)*/
                /*#-code-walkthrough(rawData.gyroscope)*/
                /*#-code-walkthrough(rawData.addText)*/
                
            }
            .font(.system(.body, design: .monospaced))
            .padding(5)
        }
    }
}

struct RawData_Preview: PreviewProvider {
    static var previews: some View {
        RawData()
            .environmentObject(MotionDetector(updateInterval: 0.5).started())
    }
}
