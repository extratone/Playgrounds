import SwiftUI

struct DataHistory: View {
    @EnvironmentObject var detector: MotionDetector
    
    /*#-code-walkthrough(dataHistory.array)*/
    //#-learning-code-snippet(dataHistory.array)
    /*#-code-walkthrough(dataHistory.array)*/
    
    var body: some View {
        VStack {
            
            Text("Pitch: \(detector.pitch)")
                .foregroundColor(.accentColor)
                .font(.system(.body, design: .monospaced))
                .padding()
            
            /*#-code-walkthrough(dataHistory.forEach)*/
            HStack {
                //#-learning-code-snippet(dataHistory.forEach)
            }
            /*#-code-walkthrough(dataHistory.forEach)*/
        
        /*#-code-walkthrough(dataHistory.onAppear)*/
        }
        //#-learning-code-snippet(dataHistory.onAppear)
        /*#-code-walkthrough(dataHistory.onAppear)*/
        
    }
}

struct DataHistory_Preview: PreviewProvider {
    static var previews: some View {
        DataHistory()
            .environmentObject(MotionDetector(updateInterval: 0.15).started())
    }
}

