import SwiftUI

struct NeedleSeismometer: View {
    @EnvironmentObject var detector: MotionDetector

    private let needleAnchor = UnitPoint(x: 0.5, y: 1)
    private let amplification = 2.0
    /*#-code-walkthrough(NeedleSeismometer.data)*/
    private var rotationAngle: Angle {
        Angle(radians: detector.zAcceleration * amplification)
    }
    /*#-code-walkthrough(NeedleSeismometer.data)*/

    var body: some View {
        VStack {
            Spacer()
            
            ZStack(alignment: .bottom) {
                GaugeBackground(width: 250)
                /*#-code-walkthrough(NeedleSeismometer.Rectangle)*/
                Rectangle()
                    /*#-code-walkthrough(NeedleSeismometer.conditional)*/
                    .foregroundColor(.init(hue: 0.6, saturation: 0.8, brightness: 1.0))
                /*#-code-walkthrough(NeedleSeismometer.Rectangle)*/
                /*#-code-walkthrough(NeedleSeismometer.conditional)*/
                    .frame(width: 5, height: 190)
                    .rotationEffect(rotationAngle, anchor: needleAnchor)
                    .overlay(alignment: .bottom) {
                        Circle()
                            .stroke(lineWidth: 3)
                            .frame(width: 10, height: 10)
                        /*#-code-walkthrough(NeedleSeismometer.overlay)*/
                            .foregroundColor(.accentColor)
                        /*#-code-walkthrough(NeedleSeismometer.overlay)*/
                            .background(.white)
                            .offset(x: 0, y: 5)
                    }
            }

            Spacer()
            
            Text("Set your device on a flat surface to detect vibrations.")
                .padding()

            Spacer()
        }
    }
}

struct NeedleSeismometer_Previews: PreviewProvider {
    @StateObject static private var detector = MotionDetector(updateInterval: 0.01).started()
    
    static var previews: some View {
        NeedleSeismometer()
            .environmentObject(detector)
    }
}
