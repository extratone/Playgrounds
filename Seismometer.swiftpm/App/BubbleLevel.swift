import SwiftUI

/*#-code-walkthrough(BubbleLevel.viewOverview)*/
struct BubbleLevel: View {
    /*#-code-walkthrough(BubbleLevel.viewOverview)*/
    @EnvironmentObject private var detector: MotionDetector

    let range = Double.pi
    let levelSize: CGFloat = 300

    /*#-code-walkthrough(BubbleLevel.bubblePositionProperties)*/
    var rollAsFraction: Double {
        (detector.roll + range/2) / range
    }
    
    var pitchAsFraction: Double {
        (detector.pitch + range/2) / range
    }

    var bubbleXPosition: CGFloat {
        return OrientationDetector.shared.adjustedRoll(forRoll: rollAsFraction, pitch: pitchAsFraction) * levelSize
    }

    var bubbleYPosition: CGFloat {
        return OrientationDetector.shared.adjustedPitch(forRoll: rollAsFraction, pitch: pitchAsFraction) * levelSize
    }
    /*#-code-walkthrough(BubbleLevel.bubblePositionProperties)*/

    var verticalLine: some View {
        Rectangle()
            .frame(width: 0.5, height: 40)
    }

    var horizontalLine: some View {
        Rectangle()
            .frame(width: 40, height: 0.5)
    }

    var body: some View {
        
        VStack {
            Spacer()
            
        Circle()
            .foregroundStyle(Color.secondary.opacity(0.25))
            .frame(width: levelSize, height: levelSize)
            .overlay(
                ZStack {
                    
                    /*#-code-walkthrough(BubbleLevel.bubble)*/
                    Circle()
                        .foregroundColor(.accentColor)
                    /*#-code-walkthrough(BubbleLevel.bubbleSize)*/
                        .frame(width: 50, height: 50)
                    /*#-code-walkthrough(BubbleLevel.bubbleSize)*/
                        .position(x: bubbleXPosition,
                                  y: bubbleYPosition)
                    /*#-code-walkthrough(BubbleLevel.bubble)*/
                    
                    Circle()
                        .stroke(lineWidth: 0.5)
                        .frame(width: 20, height: 20)
                    verticalLine
                    horizontalLine

                    verticalLine
                        .position(x: levelSize / 2, y: 0)
                    verticalLine
                        .position(x: levelSize / 2, y: levelSize)
                    horizontalLine
                        .position(x: 0, y: levelSize / 2)
                    horizontalLine
                        .position(x: levelSize, y: levelSize / 2)
                    
                    /*#-code-walkthrough(BubbleLevel.conditional)*/
                    //#-learning-code-snippet(BubbleLevel.conditional)
                    /*#-code-walkthrough(BubbleLevel.conditional)*/
                }
            )
        
            Spacer()
            
            Text("Move your device around to detect when it is level.")
                .padding()
            /*#-code-walkthrough(BubbleLevel.positionTracking)*/
            //#-learning-code-snippet(BubbleLevel.positionTracking)
            /*#-code-walkthrough(BubbleLevel.positionTracking)*/
            
            Spacer()
        
        }
    }
}

struct BubbleLevel_Previews: PreviewProvider {
    @StateObject static private var detector = MotionDetector(updateInterval: 0.01).started()
    
    static var previews: some View {
        BubbleLevel()
            .environmentObject(detector)
    }
}

