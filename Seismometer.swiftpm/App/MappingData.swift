import SwiftUI

struct MappingData: View {
    @EnvironmentObject var detector: MotionDetector
    
    var body: some View {
        VStack {
            Text("Roll: \(detector.roll)")
                .foregroundColor(.blue)
                .font(.system(.body, design: .monospaced))
                .padding()
            /*#-code-walkthrough(mapData.rectangle)*/
            Rectangle()
            /*#-code-walkthrough(mapData.size)*/
            /*#-code-walkthrough(mapData.foregroundColor.rgb)*/
            /*#-code-walkthrough(mapData.otherColors)*/
                .foregroundColor(Color.init(red: (detector.roll + Double.pi) / (Double.pi * 2), green: 0.0, blue: 1 - ((detector.roll + Double.pi) / (Double.pi * 2))
))
            /*#-code-walkthrough(mapData.foregroundColor.rgb)*/
                .frame(width: 100, height: (detector.roll + Double.pi) / (Double.pi * 2) * 300)
                .animation(.default, value: detector.roll)
            /*#-code-walkthrough(mapData.rotation)*/
            Rectangle()
                .frame(width: 100, height: 200)
            /*#-code-walkthrough(mapData.rotation3D)*/
                .rotation3DEffect(Angle(radians: detector.roll), axis: (x: 0.5, y: 0.0, z: 0.0))
            /*#-code-walkthrough(mapData.rotation3D)*/
                .animation(.default, value: detector.roll)
            
            /*#-code-walkthrough(mapData.rotation)*/
            /*#-code-walkthrough(mapData.size)*/
            /*#-code-walkthrough(mapData.rectangle)*/
            
        }
    }
    
}

struct MappingData_Preview: PreviewProvider {
    static var previews: some View {
        MappingData()
            .environmentObject(MotionDetector(updateInterval: 0.1).started())
    }
}
