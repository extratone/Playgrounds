import SwiftUI
import Charts

/*#-code-walkthrough(GraphSeismometer.declaration)*/
struct GraphSeismometer: View {
    /*#-code-walkthrough(GraphSeismometer.environmentObject)*/
    /*#-code-walkthrough(GraphSeismometer.declaration)*/
    @EnvironmentObject private var detector: MotionDetector
    /*#-code-walkthrough(GraphSeismometer.environmentObject)*/
    /*#-code-walkthrough(GraphSeismometer.dataProperty)*/
    @State private var data = [Double]()
    let maxData = 500
    /*#-code-walkthrough(GraphSeismometer.dataProperty)*/
    
    @State var buttonText = "Stop"
    @State var recordData = true
    
    var body: some View {
        VStack {
            Spacer()
            
            /*#-code-walkthrough(GraphSeismometer.LineGraph)*/
            Chart(data.indices, id: \.self) { index in
                /*#-code-walkthrough(GraphSeismometer.LineMark)*/
                LineMark(
                    x: .value("Data point", index),
                    y: .value("Acceleration", data[index])
                )
                /*#-code-walkthrough(GraphSeismometer.LineMark)*/
            }
            /*#-code-walkthrough(GraphSeismometer.LineGraph)*/
            /*#-code-walkthrough(GraphSeismometer.modifiers)*/
            .frame(height: 300)
            .foregroundColor(.accentColor)
            .chartYScale(domain: -0.5...0.5)
            .chartXScale(domain: 0.0...Double(maxData))
            /*#-code-walkthrough(GraphSeismometer.modifiers)*/
            
            
            Spacer()
            
            Text("Set your device on a flat surface to begin detecting vibrations using the motion sensors.")
                .padding()
            
            Button(buttonText) {
                if recordData {
                    buttonText = "Start"
                    recordData = false
                } else {
                    buttonText = "Stop"
                    recordData = true
                }
            }
            
            Spacer()
        }
        .onAppear {
            /*#-code-walkthrough(GraphSeismometer.detectorOnUpdate)*/
            detector.onUpdate = {
                if recordData {
                    data.append(detector.zAcceleration)
                    if data.count > maxData {
                        data = Array(data.dropFirst())
                    }
                }
            }
            /*#-code-walkthrough(GraphSeismometer.detectorOnUpdate)*/
        }
    }
}
