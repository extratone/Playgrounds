import SwiftUI

struct SensorDataDisplay: View {
    @EnvironmentObject private var detector: MotionDetector
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Circle()
                    .fill(Color(uiColor: .tertiarySystemFill))
                    .frame(width: 30, height: 30)
                    .overlay(
                        Image(systemName: "xmark")
                            .imageScale(.small)
                            .font(.callout.weight(.bold))
                            .foregroundColor(.secondary)
                    )
                    .padding(10)
                    .onTapGesture {
                        dismiss()
                    }
                
            }
            
            Text("Rotate and shake your device to explore the data from its sensors.")
                .padding()
            
            HStack {
                Spacer()
                VStack {
                    Text("Gyroscope")
                        .fontWeight(.bold)
                        .padding(2)
                    DeviceViewRotate(sensorData: "Pitch", gyroData: detector.pitch, rotationAxes: (x: 0.0, y: 0.5, z: 0.0), axisName: .x)
                        .padding()
                    DeviceViewRotate(sensorData: "Roll", gyroData: detector.roll, rotationAxes: (x: 0.5, y: 0.0, z: 0.0), axisName: .y)
                        .padding()
                    DeviceViewRotate(sensorData: "Yaw", gyroData: detector.yaw, rotationAxes: (x: 0.0, y: 0.0, z: 0.5), axisName: .z)
                        .padding()
                }
                
                Spacer()
                
                VStack {
                    Text("Accelerometer")
                        .fontWeight(.bold)
                        .padding(2)
                    DeviceViewRotate(sensorData: "x-acceleration", isGyro: false, accData: detector.xAcceleration, scaleFactor: 100, axisName: .x)
                        .padding()
                    DeviceViewRotate(sensorData: "y-acceleration", isGyro: false, accData: detector.yAcceleration, scaleFactor: 100, axisName: .y)
                        .padding()
                    DeviceViewRotate(sensorData: "z-acceleration", isGyro: false, accData: detector.zAcceleration, axisName: .z)
                        .padding()
                }
                Spacer()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SensorDataDisplay_Previews: PreviewProvider {
    static var previews: some View {
        SensorDataDisplay()
            .environmentObject(MotionDetector(updateInterval: 0.1).started())
    }
}


struct DeviceViewRotate: View {
    let sensorData: String
    var isGyro = true
    var gyroData = 0.0
    var accData = 0.0
    var rotationAxes: (x: CGFloat, y: CGFloat, z: CGFloat) = (x: 0.0, y: 0.0, z: 0.0)
    var scaleFactor = 0.0
    
    enum Axis: String {
        case x, y, z
    }
    let axisName: Axis
    
    var accDataX: Double {
        axisName == .x ? accData : 0.0
    }
    
    var accDataY: Double {
        axisName == .y ? accData : 0.0
    }
    
    var scaleValue: Double {
        axisName == .z && !isGyro ? 1.0 + accData/2 : 1.0
    }
    
    var body: some View {
        VStack {
            Text(sensorData)
            RoundedRectangle(cornerRadius: 5.0)
                .aspectRatio(CGSize(width: 4, height: 3), contentMode: .fit)
                .frame(minHeight: 60, maxHeight: 120)
                .scaleEffect(scaleValue)
                .rotation3DEffect(Angle(radians: gyroData), axis: rotationAxes)
                .offset(x: accDataY * scaleFactor, y: accDataX * scaleFactor)
                .foregroundColor(.gray)
                .overlay (
                    GeometryReader { geometry in
                        switch axisName {
                            case .x:
                                VStack {
                                    Rectangle()
                                        .frame(width: 2.0, height: geometry.size.height * 1.1)
                                        .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
                                        .opacity(0.2)
                                    Text(axisName.rawValue)
                                        .opacity(0.4)
                                        .offset(y: 25)
                                }
                            case .y:
                                HStack {
                                    Rectangle()
                                        .frame(width: geometry.size.width * 1.1, height: 2.0)
                                        .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
                                        .opacity(0.2)
                                    Text(axisName.rawValue)
                                        .opacity(0.4)
                                        .offset(x: 25)
                                }
                            case .z:
                                Rectangle()
                                    .frame(width: 4.0, height: 4.0)
                                    .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
                                    .foregroundColor(.white)
                                    .opacity(0.7)
                                Text(axisName.rawValue)
                                    .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
                                    .offset(x: 10)
                                    .foregroundColor(.white)
                                    .opacity(0.7)
                        }
                    }
                )
                .animation(.default, value: isGyro ? gyroData : accData)
            Spacer()
        }
    }
    
}
