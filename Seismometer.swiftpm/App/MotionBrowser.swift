//#-learning-task(MotionBrowser)
import SwiftUI

/*#-code-walkthrough(MotionBrowser.declaration)*/
struct MotionBrowser: View {
/*#-code-walkthrough(MotionBrowser.declaration)*/
    /*#-code-walkthrough(MotionBrowser.detectorProperty)*/
    @StateObject private var detector = MotionDetector(updateInterval: 0.01)
    /*#-code-walkthrough(MotionBrowser.detectorProperty)*/

    @State private var viewSelector : SeismometerDisplay = .needle
    @State private var isShowingSheet = false
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Button {
                    isShowingSheet.toggle()
                } label: {
                    Image(systemName: "info.circle")
                }
                .sheet(isPresented: $isShowingSheet) {
                    SensorDataDisplay()
                }
            }
            .padding(10)
            
            Picker("Tool selector", selection: $viewSelector) {
                Text("Needle").tag(SeismometerDisplay.needle)
                Text("Graph").tag(SeismometerDisplay.graph)
                Text("Bubble").tag(SeismometerDisplay.bubble)
            }
            .frame(maxWidth: 500)
            
            switch viewSelector {
                case .needle:
                NeedleSeismometer()
                case .graph:
                GraphSeismometer()
                case .bubble:
                BubbleLevel()
            }
        }
        .pickerStyle(.segmented)
        .padding()
        /*#-code-walkthrough(MotionBrowser.environmentObjectModifier)*/
        .environmentObject(detector)
        /*#-code-walkthrough(MotionBrowser.environmentObjectModifier)*/
        /*#-code-walkthrough(MotionBrowser.appearDisappearModifiers)*/
        .onAppear() {
            detector.start()
        }
        .onDisappear {
            detector.stop()
        }
        /*#-code-walkthrough(MotionBrowser.appearDisappearModifiers)*/
    }
    
    enum SeismometerDisplay {
        case needle, bubble, graph
    }
    
}

struct MotionBrowser_Previews: PreviewProvider {
    static var previews: some View {
        MotionBrowser()
    }
}


private func linkView(image: String, title: String, subtitle: String) -> some View {
    HStack() {
        Image(systemName: image)
            .foregroundColor(.accentColor)
            .padding()
            .font(.title2)
        
        VStack(alignment: .leading, spacing: 8) {
            Text(title)
                .font(.headline)
            Text(subtitle)
                .font(.caption)
        }
        .padding(.trailing)
    }
}
