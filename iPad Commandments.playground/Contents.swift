//: A SwiftUI-based playground made by DetailsPro for presenting great user interface designs.

import SwiftUI
import UIKit
import PlaygroundSupport

struct ContentView: View {
    var body: some View {
		ZStack {
			Image(uiImage: UIImage(named: "Image 1.png") ?? .init())
				.renderingMode(.original)
				.resizable()
				.aspectRatio(contentMode: .fit)
				.offset(x: 730, y: 700)
				.opacity(0.34)
				.shadow(color: Color(.tertiaryLabel).opacity(0.5), radius: 28, x: 4, y: 4)
				.scaleEffect(0.7, anchor: .center)
			RoundedRectangle(cornerRadius: 30, style: .continuous)
				.stroke(Color(.systemGray5), lineWidth: 60)
				.background(RoundedRectangle(cornerRadius: 30, style: .continuous).fill(Color(.systemGray5).opacity(0.39)))
			ZStack {
				Text("Doing email.")
					.font(.system(size: 200, weight: .heavy, design: .serif))
					.foregroundColor(.red)
				Text("Reading ebooks.")
					.font(.system(size: 90, weight: .ultraLight, design: .serif))
					.foregroundColor(Color(.systemBrown))
					.offset(x: 0, y: 340)
				Text("Enjoying and sharing photographs.")
					.offset(x: 120, y: 150)
					.font(.system(size: 40, weight: .medium, design: .serif))
					.foregroundColor(.purple)
				Text("Watching videos.")
					.offset(x: 350, y: 220)
					.font(.system(size: 40, weight: .medium, design: .serif))
					.foregroundColor(.cyan)
				Text("Enjoying your music collection.")
					.offset(x: -120, y: -120)
					.font(.system(size: 40, weight: .medium, design: .serif))
					.foregroundColor(.yellow)
				Text("Playing games.")
					.offset(x: -350, y: -200)
					.font(.system(size: 40, weight: .medium, design: .serif))
					.foregroundColor(.green)
			}
			.offset(x: 0, y: -50)
			.shadow(color: .pink.opacity(0.28), radius: 38, x: 0, y: 4)
		}
		.frame()
		.frame(width: 1366, height: 1024)
		.clipped()
		.background(Color(.systemBackground))
		.mask { RoundedRectangle(cornerRadius: 16, style: .continuous) }
    }
}



PlaygroundPage.current.setLiveView(ContentView())